---
type: project
title: Unlicense
date: 2010-01-01T00:00:00Z
draft: false
tags:
  - Unlicense
  - unlicensing
  - public domain
  - copyright
  - Creative Commons
  - CC0
summary: A template for disclaiming copyright monopoly interest in software you've written.
---

https://unlicense.org

The Unlicense is a template for disclaiming copyright monopoly interest in
software you've written; in other words, it is a template for dedicating
your software to the public domain. It combines a copyright waiver patterned
after the very successful public domain SQLite project with the no-warranty
statement from the widely-used MIT/X11 license.
