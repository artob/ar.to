---
type: review
url: 2014/02/the-martian.html
title: "The Martian"
subtitle: "★★★★★ My book review of [_\"The Martian\"_](https://www.goodreads.com/book/show/18007564) by Andy Weir."
date: 2014-02-05T00:00:00Z
lastmod:
draft: false
authors:
  - arto
categories:
  - Fiction
tags:
  - fiction
  - science fiction
  - Mars
summary: ⭑⭑⭑⭑⭑ Near-future hard science fiction at its finest, this is the ultimate marooned-on-an-island yarn.
---

<em><small>Originally published as a <a href="https://www.goodreads.com/review/show/846331504" target="_blank">book review in my Goodreads account</a>.</small></em>

I read this in one go. Near-future hard science fiction at its finest, this is the ultimate marooned-on-an-island story---where the island is a barren desert world twenty light-minutes past the horizon, and you're stuck at the bottom of its gravity well with a dwindling food and oxygen supply, no means of egress, and no possible hope of rescue.

It's a fantastic yarn, and it's a credit to the author that the least plausible aspect of the story is its quaint and romantic premise that NASA, and the insolvent former superpower backing it, could afford to and would be able to land a manned mission on Mars---and not just one, but five of them in a row. I'll put my chips on Elon Musk, thank you all the same.

Be that as it may, if you appreciate man's battle for survival in the face of merciless nature and impossible odds, be sure to follow up _The Martian_ with the bona fide account of dogged, preposterous survival recounted in [_Endurance: Shackleton's Incredible Voyage_](https://www.goodreads.com/book/show/139069) by [Alfred Lansing](https://www.goodreads.com/author/show/29034). If we can survive what Shackleton and his men did, we'll survive Mars, too. Though it'd surely help to have Mark Watney along for the ride.
