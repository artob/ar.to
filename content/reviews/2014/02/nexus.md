---
type: review
url: 2014/02/nexus.html
title: "Nexus"
subtitle: "★★★★★ My book review of [_\"Nexus\"_](https://www.goodreads.com/book/show/24968342) by Ramez Naam."
date: 2014-02-13T00:00:00Z
lastmod:
draft: false
authors:
  - arto
categories:
  - Fiction
tags:
  - fiction
  - science fiction
  - cyberpunk
summary: ⭑⭑⭑⭑⭑ A roller-coaster of a cyberpunk thriller, and a compelling vision of hackable wetware.
---

<em><small>Originally published as a <a href="https://www.goodreads.com/review/show/854078078" target="_blank">book review in my Goodreads account</a>.</small></em>

I ended up reading this roller-coaster of a cyberpunk thriller in one sitting. It's a fantastic debut and an impressive depiction of a plausible enough near future on the fast track to a technological singularity. To introduce a noob to transhumanism, I'd prescribe _Nexus_ followed by [_Accelerando_](https://www.goodreads.com/book/show/17863) and [_Diaspora_](https://www.goodreads.com/book/show/156785), which should be plenty to blow anyone's mind wide open into a minor existential crisis.

Having won the Prometheus Award this previous year, it's no surprise that the themes presented in this story will resonate particularly strongly with libertarians. With the powers that be waging a doomed-to-fail War on Science to contain disruptive technologies, with agents of the state extorting, kidnapping, torturing, and murdering people for victimless crimes, and with a general populace turning a blind eye as usual to the atrocities committed ostensibly on their behalf, there's plenty here to keep a cynic happy. (The storyline in [_Influx_](https://www.goodreads.com/book/show/18114057) unfolds on rather parallel themes.)

For us techies, this compelling vision of hackable wetware---hackable in both senses---presents hypotheticals and responsibilities of paramount import. As we've learned the hard way over the past couple of years, our current approaches to computer security are broken on such a fundamental level that in reality the Emerging Risks Directorate would hardly need to break a sweat in their pursuit of the factory backdoors to Nexus OS; rather, they'd just task the NSA with finding them a security exploit or two that would amount to the same thing. To prevent endless variations of digital dystopia facilitated by coercion technologies, we had better well and truly figure out provably secure computing in the next decade or so.
