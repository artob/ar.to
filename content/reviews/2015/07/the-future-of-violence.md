---
type: review
url: 2015/07/the-future-of-violence.html
title: "The Future of Violence: Robots and Germs, Hackers and Drones"
subtitle: "★★☆☆☆ My book review of [_\"The Future of Violence: Robots and Germs, Hackers and Drones---Confronting A New Age of Threat\"_](https://www.goodreads.com/book/show/25110269) by Wittes & Blum."
date: 2015-07-31T00:00:00Z
lastmod:
draft: false
authors:
  - arto
categories:
  - Technology
tags:
  - technology
  - politics
  - superempowerment
  - 4GW
summary: ⭑⭑⭒⭒⭒ More a product of its environment than a root cause analysis that will stand the test of time.
---

<em><small>Originally published as a <a href="https://www.goodreads.com/review/show/1351021186" target="_blank">book review in my Goodreads account</a>.</small></em>

Disappointing overall, and at times shallow and boring. One emerges with the assessment that the two authors are easily impressed by surface-level phenomena. They maintain that narratives taught in civics class suffice to illuminate the origins and practice of politics. They perceive a simple world, where stated intentions equal actual objectives, costs can be judged relative to ostensible objectives without minding actual effects, correlation generally implies causation, and the direction of the arrow of causality can be promptly determined. These are characteristics more of a Haidtian moral matrix than the dispassionate analysis one might have wished for in a book of this kind.

The authors speak confidently about their ability to understand and predict this neatly modeled world of theirs, not particularly endeavoring to uncover their own implicit premises---brief mentions of Western liberalism aside---and rarely hedging or qualifying their rather confident assertions, many which could readily be challenged. In the gulf between reality and academia, this work is more a product of its environment than a root cause analysis that will stand the test of time.

On the bright side, in largely restricting themselves to disruptive technologies with ETAs defined with pretty firm confidence margins, we were at least spared yet another alarmist narrative of shoddily-premised and amateurishly-informed more speculative futures, which in this instance might have been titled something like "the state v. gray goo".

Indeed, the most readable part of the book is the initial bit that discusses some of the developing technologies of mass empowerment that are set to make the first half of this century an "interesting times" to live through per that purported old Chinese curse. There is nothing new here, but it is at least a useful overview of biotech terrors to come as well as the developing arms race in autonomous weaponry, soon to be wielded by state and non-state actors both.

This is followed, alas, by the middle portion of the book, where I suspect the authors will tend to lose readers who aren't Beltway policy wonks. These chapters are, chiefly, tedious apologia for the ostensible origins and continued existence of the state. The authors glimpse the coming sea change, but prove ultimately unable to transcend or escape the shore.

Almost pathetically obsessed with the continuation of the authority and legitimacy of the status quo in political organization today (perhaps their jobs depend on it?), the authors even go on to disregard their own contention---grounded in the myth of the social contract---that the state is only a means, not an end unto itself. Their own authority bias here leads them down a garden path of motivated reasoning that is sure to not only find itself on the wrong side of history, but soon enough swept into its dust bin.

The authors rehash elementary early modern political theory at length---beginning with the Hobbesian war of all against all---taking it all at face value as would any freshman. To say that this is a historically uninformed and philosophically unsophisticated analysis would only scratch the surface here, and the authors seem impossibly yet genuinely unaware of troves and generations of thinkers and scholars who advanced in leaps and bounds on everything they discuss here.

One might have hoped, ultimately, for a deeper nonideological and amoral ("check your morality at the door, please") analysis premised, say, on an evolutionary and game-theoretic angle instead of this longwinded trite elaboration of the dominant political mythology du jour, consisting of little more than post-hoc rationalizations that already read akin to medieval notions of the divine right of kings. What's a little surprising is that the authors even acknowledge these being mere justifications for the Westphalian order, prompting the question of whether this ought to not be read as an extended bit of demagoguery in the Menckenian sense of _"preaching doctrines one knows to be untrue to men one knows to be idiots."_

The discussion, as it moves on to the prescriptive last third of the book, is further muddled by the authors' disregard for the precise use of language to aid in clarity of thought. Never, for instance, do they differentiate between positive liberty and negative liberty---a distinction both elementary and essential---which, unsurprisingly, helps lead them absurd conclusions such as that _"mass surveillance makes us freer."_ Edward Snowden's "crime" accordingly merits a whole section in a similar vein.

At root, though, unexamined and uncontested, lies the belief that, given sufficient resources, the state can, in fact, actually achieve its ostensible ends---whether those ostensible ends be the provision of security as such, or the maintenance of a Tofflerian surplus order. This is a perspective uninformed by economic law and untempered by a developed intuition about spontaneous order; both being sides of the same coin, as per Hayek's dictum that _"the curious task of economics is to demonstrate to men how little they really know about what they imagine they can design."_

Scarcely mentioned, for instance, are the more than trillion dollars and countless lives wasted fighting a futile War on Drugs. Other authors from [Bill Lind](https://www.goodreads.com/author/show/325376) to [T. X. Hammes](https://www.goodreads.com/author/show/21901) have made the case that the inability of the state, with every conceivable home court advantage that it has, to significantly hamper let alone eradicate these 4GW adversaries---in this case, the drug cartels and their international distribution network---perfectly showcases its impotence in credibly countering more dangerous internal and external adversaries going forward.

Instead of the epistemic humility to limit themselves to the merely descriptive and predictive, the authors here choose to be prescriptive; too bad, only, that it is on such an axiomatically deficient and analytically shallow platform. As such, this amounts to rearranging deck chairs on the Titanic---or make that SS Leviathan. The book best serves to underline the incompetence of the extended modern state apparatus to comprehend and cope with the rapidly multiplying and escalating challenges to its formerly unilateral hegemony. As Boyd would cackle, _"you're not even in the game!"_

A case example thereof is that much of the discussion in the section on so-called "intermediate regulation" has already been obsoleted by subsequent technological developments, and that's all in the span of less than a decade past the authority they cite as the basis for their narrative here. You'd never imagine the authors to have heard of these newer developments, even as events were in the process of overtaking and obsoleting their manuscript.

In their conclusion, the authors tacitly acknowledge that the future is ungovernable, but go on to say that they're _"not ready to give up on the state"_ yet. Good for them. For a deeper, more realistic, and better informed picture of the unfolding twenty-first century, pass on this and read the likes of [John Robb](https://www.goodreads.com/author/show/8642675) and old curmudgeon [Bill Lind](https://www.goodreads.com/author/show/325376) instead.
