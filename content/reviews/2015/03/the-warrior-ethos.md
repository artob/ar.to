---
type: review
url: 2015/03/the-warrior-ethos.html
title: "The Warrior Ethos"
subtitle: "★★☆☆☆ My book review of [_\"The Warrior Ethos\"_](https://www.goodreads.com/book/show/11468255) by Steven Pressfield."
date: 2015-03-07T00:00:00Z
lastmod:
draft: false
authors:
  - arto
categories:
  - History
tags:
  - history
  - military history
  - self-improvement
summary: ⭑⭑⭒⭒⭒ An encomium to empire, deliberately and carefully selective with its facts.
---

<em><small>Originally published as a <a href="https://www.goodreads.com/review/show/1221653552" target="_blank">book review in my Goodreads account</a>.</small></em>

As a fan of Pressfield's historical fiction and as a student of military history, I was predisposed to like this short book of historical anecdotes and commentary on the warrior ethos; more's the pity that it turned out to be significantly problematic.

You see, a naive reading would miss that this is ultimately an encomium to empire, deliberately and carefully selective with its facts. It can only be meant to inspire, not to inform---and indeed, a clue to this end can be found in Pressfield's statement that he wrote this for _"men and women in uniform"_. He evidently does not have an unduly high regard for the critical faculties of his audience.

An illustrative example right off the bat is the pathological case of sample bias that concludes the first chapter, narrated with a straight face and nary a historiographical concern, devoid any condemnation of eugenics, and demonstrating no comprehension of established human universals:

> In Sparta, every newborn boy was brought before the magistrates to be examined for physical hardiness. If a child was judged unfit, he was taken to a wild gorge on Mount Taygetos, the mountain overlooking the city, and left for the wolves. We have no reports of a mother weeping or protesting.

As another point of note, when speaking of pride and honor, and judging the warrior ethos of Americans to be superior to that of their foes, Pressfield makes an obligatory mention of "enhanced interrogation" (torture) but omits any consideration of "collateral damage" (regrettable murder), the millions of victims of many long decades of empire-building. Similar bland disregard is given to the peoples vanquished by history's great conquerors and commanders---this is very much a hagiography of the victors.

By the chapter on "The Civilian World", one begins to suspect the author of outright demagoguery. Coming from an author who certainly knows better, how are we to understand a nakedly false assertion such as the following:

> In ancient Sparta and in the other cultures cited, a warrior culture (the army) existed within a warrior society (the community itself). No conflict existed between the two.

The helots of ancient Sparta, who under pain of death toiled to feed and clothe these brave and honorable warriors, might have taken umbrage at the statement---had they dared to look their masters in the eye. As it is, the word "helot" does not appear even once in this book that on the whole reads like an ode to the totalitarian glory that was Sparta.

Even if it be the case that wolves don't concern themselves with the opinions of sheep---a warrior ethos for the ages if there ever was one---such strategic omissions are all the more puzzling given the author's considerably more evenhanded treatment of these matters in his bestselling novel [_Gates of Fire_](https://www.goodreads.com/book/show/19047634).

I might have had a positive thing or two to say about portions of this work, but cannot be motivated to further comment given the overarching vexations and shallow pretense of history here. A true disappointment.
