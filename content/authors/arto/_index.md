---
name: Arto Bendiken
email: arto@bendiken.net
authors:
  - arto
superuser: true
user_groups: []

role: Master Toolsmith, Cypherpunk Division
organizations:
  - name: Crypto Economics Consulting Group
    url: https://cecg.biz
bio: Digital nomad. Freelance coder. Polyglot hacker. Wannabe maker.

interests:
  - Robotics
  - Machine Learning
  - Military History & Theory

social:
  - icon: envelope
    icon_pack: fas
    link: '#contact'
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/bendiken
  - icon: github
    icon_pack: fab
    link: https://github.com/artob
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.com/citations?user=tC8f4UMAAAAJ
  - icon: cv
    icon_pack: ai
    link: https://www.linkedin.com/in/arto/
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed interdum arcu. Nam vitae consequat neque. Maecenas a pretium enim, sit amet eleifend enim. In id magna sit amet quam mattis cursus. Suspendisse potenti. Ut nunc quam, auctor id lorem nec, maximus pharetra turpis. Mauris vel risus sem. Cras porta pulvinar magna sit amet vulputate. Aenean id nisl volutpat, tristique lacus consectetur, faucibus metus.

Fusce hendrerit rutrum rhoncus. Vestibulum felis magna, fermentum a porta imperdiet, ultrices ut ex. Donec nibh urna, cursus sed efficitur vel, blandit ac mi. Nam neque enim, commodo vitae vestibulum a, congue porta quam. Nam fringilla euismod lectus, a dapibus quam lobortis quis. Nullam blandit leo eu quam cursus, sit amet aliquet purus tristique. Suspendisse pretium dictum rhoncus. Fusce gravida ligula vitae nunc ornare, quis ullamcorper nibh suscipit. Donec facilisis convallis faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas placerat lacus quis dui vestibulum mollis. Pellentesque fermentum varius malesuada.
