---
type: talk
url: 2018/04/talk-at-pivorak-33.html
title: In Search of Silver Bullets for Polyglots
subtitle: My talk at the Pivorak 33 meetup in Lviv on April 20, 2018.
date: 2018-04-20T18:00:00Z
date_end: 2018-04-20T22:00:00Z
all_day: false
publishDate: 2018-04-20Z
featured: false
event: Pivorak 33
event_url: https://pivorak.com/talks/in-search-of-silver-bullets-for-polyglots
location: Lviv, Ukraine
url_video: https://www.youtube.com/watch?v=VXg6qYKxIUw
url_slides: https://speakerdeck.com/arto/in-search-of-silver-bullets-for-polyglots-at-pivorak-33
url_pdf: https://speakerd.s3.amazonaws.com/presentations/a210c9304f8b4e9ca85620c67b7bea4c/Silver_Bullets_for_Polyglots_at_Pivorak_33.pdf
url_code:
links:
  - icon: twitter
    icon_pack: fab
    name: Follow
    url: https://twitter.com/bendiken
authors:
  - arto
tags:
  - polyglot programming
  - programming
projects:
  - drylib
summary: An examination of some force multipliers to reduce the cognitive
  load of polyglot programming.
abstract: Frontends in JavaScript, backends in Ruby, Elixir, Go, or Java.
  Apps in Dart, Kotlin, and Swift. The right tools for the job at hand, or
  fashions and fads? Few of us are coding in only a single programming
  language any longer. Monoculture is dead and buried. Like it or not, we are
  all polyglot programmers now. However, this trend has come at a huge
  cognitive cost. We will examine some semi-crazy force multipliers to reduce
  that cognitive load, enable network effects to cross languages, and perhaps
  manage to preserve investments in code for years to come in our rapidly
  shifting industry.
---

{{< youtube VXg6qYKxIUw >}}
