---
type: talk
url: 2019/03/talk-at-ifrb-5.html
title: Make Your Own Neural Network with Ruby
subtitle: "My talk at the If.rb #5 meetup in Ivano-Frankivsk on March 6, 2019."
date: 2019-03-06T19:00:00Z
date_end: 2019-03-06T21:00:00Z
all_day: false
publishDate: 2019-03-06Z
featured: false
event: If.rb Meetup #5
event_url: https://www.facebook.com/events/290820645150764/
location: Ivano-Frankivsk, Ukraine
url_video:
url_slides: https://speakerdeck.com/arto/make-your-own-neural-network-with-ruby-at-if-dot-rb-number-5
url_pdf: https://speakerd.s3.amazonaws.com/presentations/f44e0735d77e457aa683503ac167b94f/Make_Your_Own_Neural_Network_with_Ruby_at_If.rb__5.pdf
url_code: https://github.com/artob/myonn.rb
links:
  - icon: twitter
    icon_pack: fab
    name: Follow
    url: https://twitter.com/bendiken
authors:
  - arto
tags:
  - Ruby
  - programming
  - neural network
  - deep learning
  - machine learning
projects: []
summary: How to build a neural network from scratch to recognize handwritten
  numbers with excellent accuracy.
abstract: Since the breakthroughs five years ago that unleashed deep
  learning on the world, it has been described as being able to automate any
  mental task that would take an average human less than one second of
  thought.
  I'll give a gentle introduction to the mathematics and principles
  underlying neural networks--the basis for deep learning--and we will use
  Ruby to build our own neural network, from scratch, to recognize
  handwritten numbers with near state-of-the-art (circa 95%) accuracy.
---
