---
type: talk
url: 2019/02/talk-at-pivorak-conf-2.html
title: Building a Home Security System with Elixir and Nerves
subtitle: My talk at the Pivorak Conference 2.0 in Lviv on February 1, 2019.
date: 2019-02-01T18:00:00Z
date_end: 2019-02-01T23:00:00Z
all_day: false
publishDate: 2019-02-01Z
featured: false
event: Pivorak Conference 2.0
event_url: https://pivorak.com/talks/coming-soon-8ae2ecca-c016-4cad-b420-e7236ac5c302
location: Lviv, Ukraine
url_video: https://www.youtube.com/watch?v=oyNSmhkS7Dw
url_slides: https://speakerdeck.com/arto/building-a-home-security-system-with-elixir-and-nerves
url_pdf: https://speakerd.s3.amazonaws.com/presentations/dc080d5247e3424a9d474bad43cfe5a5/Building_a_Home_Security_System_with_Elixir_and_Nerves.pdf
url_code:
links:
  - icon: twitter
    icon_pack: fab
    name: Follow
    url: https://twitter.com/bendiken
authors:
  - arto
tags:
  - Nerves
  - Elixir
  - TensorFlow
  - programming
  - face recognition
  - machine learning
projects: []
summary: How to get started with Nerves by building a home security system
  running on a Raspberry Pi.
abstract: Nerves is a framework for crafting bulletproof embedded software
  written in Elixir and deployed onto the rock-solid, nine-nines Erlang/OTP
  platform running directly on top of the Linux kernel.
  I will show how to get started with Nerves with the motivating example of
  a home security system running on a Raspberry Pi. To disarm the security
  system, you need to step in front of the camera and give a big smile.
  In addition to Nerves and Elixir, we will use deep learning with
  TensorFlow for face recognition.
---

{{< youtube oyNSmhkS7Dw >}}
