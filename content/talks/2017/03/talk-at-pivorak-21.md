---
type: talk
url: 2017/03/talk-at-pivorak-21.html
title: Interactive Robotics with Elixir and Nerves
subtitle: My talk at the Pivorak 21 meetup in Lviv on March 24, 2017.
date: 2017-03-24T18:30:00Z
date_end: 2017-03-24T22:00:00Z
all_day: false
publishDate: 2017-03-24Z
featured: false
event: Pivorak 21
event_url: https://pivorak.com/talks/interactive-robotics-with-elixir-and-nerves
location: Lviv, Ukraine
url_video: https://www.youtube.com/watch?v=zBzKoV2iFdQ
url_slides: https://speakerdeck.com/conreality/conreality-at-pivorak-21
url_pdf: https://speakerd.s3.amazonaws.com/presentations/bee5f73e9b0e4b938d1e21e0b492e9ca/Interactive_Robotics_with_Elixir_and_Nerves.pdf
url_code:
links:
  - icon: twitter
    icon_pack: fab
    name: Follow
    url: https://twitter.com/bendiken
authors:
  - arto
tags:
  - Nerves
  - Elixir
  - programming
  - robotics
projects:
  - conreality
summary: An introduction to the Nerves embedded framework based on a
  live-action wargame involving drones.
abstract: The killer app for Ruby was Rails. The killer app for Elixir
  isn't (only) the Phoenix web framework, however, but rather the Nerves
  embedded framework that enables the creation of smart, networked, and
  reliable hardware using high-level abstractions. We'll introduce Nerves
  through the motivating example of the development of a live-action wargame
  involving quadcopter drones.
---

{{< youtube zBzKoV2iFdQ >}}
