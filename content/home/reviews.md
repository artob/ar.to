---
widget: pages
headless: true
active: true
weight: 40
title: Reviews
subtitle: My [book reviews](https://www.goodreads.com/review/list/22170557-arto-bendiken?shelf=read).
content:
  page_type: review
  count: 5
  offset: 0
  order: desc
design:
  view: 2
---
