---
widget: portfolio
headless: true
active: true
weight: 30
title: Essays
subtitle: My scribblings on various topics.
content:
  page_type: essay
  count: 0
  offset: 0
  order: desc
  filter_default: 0
  filter_button:
    - name: All
      tag: '*'
    - name: History
      tag: history
    - name: Technology
      tag: technology
design:
  view: 2
---
