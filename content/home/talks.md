---
widget: pages
headless: true
active: true
weight: 20
title: Talks
subtitle: My upcoming & recent presentations.
content:
  page_type: talk
  count: 5
  offset: 0
  order: desc
design:
  view: 2
---
