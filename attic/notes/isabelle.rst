****************************************************************************************
Arto's Notes re: `Isabelle <https://en.wikipedia.org/wiki/Isabelle_(proof_assistant)>`__
****************************************************************************************

* https://isabelle.in.tum.de/
* https://isabelle.in.tum.de/community/Main_Page
* https://github.com/seL4/isabelle

Frontends
=========

* `Isabelle/jEdit
  <https://www.cl.cam.ac.uk/research/hvg/Isabelle/dist/doc/jedit.pdf>`__
* `Proof General <https://proofgeneral.github.io/>`__
  (`GitHub <https://github.com/ProofGeneral/PG>`__)

See Also
========

* `Coq <coq>`__
* `HOL4 <hol4>`__
