**************************************************************************
Arto's Notes re: `statistics <https://en.wikipedia.org/wiki/Statistics>`__
**************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Guides
======

- `An Intuitive (and Short) Explanation of Bayes' Theorem
  <https://betterexplained.com/articles/an-intuitive-and-short-explanation-of-bayes-theorem/>`__
  at Better Explained

----

Notation
========

- `Notation in probability and statistics
  <https://en.wikipedia.org/wiki/Notation_in_probability_and_statistics>`__

----

See Also
========

- Arto's Notes re: `mathematics <math>`__ (`probability <prob>`__)

- `Statistics questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/statistics>`__

- `Statistics questions at MathOverflow
  <https://mathoverflow.net/questions/tagged/st.statistics>`__

- `Cross Validated <https://stats.stackexchange.com/>`__
