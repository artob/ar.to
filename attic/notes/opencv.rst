******************************************************************
Arto's Notes re: `OpenCV <https://en.wikipedia.org/wiki/OpenCV>`__
******************************************************************

http://opencv.org/

::

   # Ubuntu
   $ sudo aptitude install python-opencv

   # Mac OS X
   $ sudo port install opencv +python27
