*********************************************************************************************
Arto's Notes re: `augmented reality (AR) <https://en.wikipedia.org/wiki/Augmented_reality>`__
*********************************************************************************************

* `Software <#software>`__
* `Hardware <#hardware>`__
* `See Also <#see-also>`__

----

Software
========

Platforms
---------

* `Apple ARKit <https://developer.apple.com/arkit/>`__ (Swift, Objective-C)

* `Google ARCore <https://developers.google.com/ar/>`__ (Kotlin, Java)

  * https://github.com/google-ar/arcore-android-sdk
  * https://android-developers.googleblog.com/2017/08/arcore-augmented-reality-at-android.html

* `Vuforia AR SDK <https://en.wikipedia.org/wiki/Vuforia_Augmented_Reality_SDK>`__

Hardware
========

Headsets
--------

See Also
========

`virtual reality (VR) <vr>`__, `3D graphics <3d>`__, `GPUs <gpu>`__
