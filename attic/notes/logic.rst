****************************************************************
Arto's Notes re: `logic <https://en.wikipedia.org/wiki/Logic>`__
****************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Symbols
=======

- ¬
  "not"
  (`negation <https://en.wikipedia.org/wiki/Negation>`__)

- `∧ <https://en.wikipedia.org/wiki/Wedge_(symbol)>`__
  "and"
  (`conjunction
  <https://en.wikipedia.org/wiki/Logical_conjunction>`__)

- `∨ <https://en.wikipedia.org/wiki/Vel_(symbol)>`__
  "or"
  (`disjunction
  <https://en.wikipedia.org/wiki/Logical_disjunction>`__)

- `∀ <https://en.wikipedia.org/wiki/Turned_A>`__
  "for all"
  (`universal quantifier
  <https://en.wikipedia.org/wiki/Universal_quantification>`__)

- `∃ <https://en.wikipedia.org/wiki/Turned_E>`__
  "for some", or "there exists"
  (`existential quantifier
  <https://en.wikipedia.org/wiki/Existential_quantification>`__)

Reference
---------

- `Logical constants
  <https://en.wikipedia.org/wiki/Logical_constant>`__

- `Logical connectives
  <https://en.wikipedia.org/wiki/Logical_connective>`__

- `Logical quantifiers
  <https://en.wikipedia.org/wiki/Quantifier_(logic)>`__

- `Logic symbols
  <https://en.wikipedia.org/wiki/List_of_logic_symbols>`__

Tutorials
=========

- `The Very Short Teach Yourself Logic Guide
  <http://www.logicmatters.net/tyl/shorter-tyl/>`__

- `Formal Logic Undressed: The Director's Cut
  <https://www.youtube.com/watch?v=1KWcuhX-QTg>`__
  by Paul Snively

- `Formal Logic Undressed
  <https://www.youtube.com/watch?v=saMtzIaDCJM>`__
  by Paul Snively

Reference
=========

- `List of logic systems
  <https://en.wikipedia.org/wiki/List_of_logic_systems>`__

- `Minimal logic
  <https://en.wikipedia.org/wiki/Minimal_logic>`__

- `Classical logic
  <https://en.wikipedia.org/wiki/Classical_logic>`__

- `Intuitionistic logic
  <https://en.wikipedia.org/wiki/Intuitionistic_logic>`__

History
=======

- `The Laws of Thought
  <https://en.wikipedia.org/wiki/The_Laws_of_Thought>`__
  (1854) by George Boole

- `Begriffsschrift
  <https://en.wikipedia.org/wiki/Begriffsschrift>`__
  (1879) by Gottlob Frege

- `The Foundations of Arithmetic
  <https://en.wikipedia.org/wiki/The_Foundations_of_Arithmetic>`__
  (1884) by Gottlob Frege

- (1910) by Whitehead and Russell

- (1941) by Church

- (1958) Curry and Feys

- (1980) Howard

- (1972) Girard

- (1973) Martin-Lof


See Also
========

- `first-order logic (FOL) <fol>`__
- `higher-order logic (HOL) <hol>`__
- `type theory <types>`__
- `Zermelo–Fraenkel set theory (ZFC) <zfc>`__
