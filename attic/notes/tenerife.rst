**********************************************************************
Arto's Notes re: `Tenerife <https://en.wikipedia.org/wiki/Tenerife>`__
**********************************************************************

Services
========

Taxi
----

* `Official Taxi Tenerife
  <http://www.officialtaxitenerife.com/en/home/>`__,
  +34-922-397074 and +34-653-916278
* `Tele Taxi Santa Cruz
  <http://www.taxitenerife.es/>`__,
  +34-922-311012 and +34-922-621313
* `Taxi Santa Cruz de Tenerife <#>`__
  +34-651-437579
* `Quiero un Taxi!
  <https://play.google.com/store/apps/details?id=QT.Droid>`__

Gyms
----

* `Intergym Sport Center
  <https://www.facebook.com/intergym.sport.center/>`__,
  `Av. Melchor Luz, 2, 38400 Puerto de la Cruz
  <https://goo.gl/maps/2APQHq3G8tS2>`__.
  €9/visit. Mon-Fri 07:00-22:00, Sat 09:00-14:00.

Comms
-----

Visible networks: Movistar, Vodafone, Orange, Yoigo.

Logistics
=========

* `Bus Timetables
  <http://titsa.com/index.php/en/>`__ by
  `TITSA <https://en.wikipedia.org/wiki/TITSA>`__
* `Tourist Routes in Tenerife
  <http://www.titsa.com/index.php/en/tenerife-by-bus/tourist-routes-in-tenerife>`__
* `Tourist Routes in Santa Cruz
  <http://www.titsa.com/index.php/en/tenerife-by-bus/tourist-routes-in-santa-cruz>`__
* `Getting to the South Airport
  <http://www.titsa.com/index.php/en/tenerife-by-bus/getting-to-the-airport/tenerife-south-airport>`__
  (buses 111, **343**, 415, 450, 711)
* `Bus Line 343
  <http://www.titsa.com/index.php/en/tenerife-by-bus/getting-to-the-airport/tenerife-south-airport/linea-343>`__

From Puerto de la Cruz
----------------------

* `To Los Cristianos
  <https://www.rome2rio.com/s/Puerto-de-la-Cruz/Los-Cristianos>`__
* `To Playa de las Américas
  <https://www.rome2rio.com/s/Puerto-de-la-Cruz/Playa-de-las-Am%C3%A9ricas>`__
* `To Santa Cruz
  <https://www.rome2rio.com/s/Puerto-de-la-Cruz/Santa-Cruz-de-Tenerife>`__
* `To Tenerife Sur Airport
  <https://www.rome2rio.com/s/Puerto-de-la-Cruz/Tenerife-Sur-Apt-Airport-TFS>`__

Reference
=========

* `Tenerife on Wikitravel
  <http://wikitravel.org/en/Tenerife>`__

Airports
--------

* `Tenerife Sur Airport (aka Reina Sofia)
  <https://en.wikipedia.org/wiki/Tenerife%E2%80%93South_Airport>`__
* `Tenerife North Airport (aka Los Rodeos)
  <https://en.wikipedia.org/wiki/Tenerife-North_Airport>`__

Towns
-----

Towns in the North
^^^^^^^^^^^^^^^^^^

* `Puerto de la Cruz
  <https://en.wikipedia.org/wiki/Puerto_de_la_Cruz>`__
* `San Cristóbal de La Laguna
  <https://en.wikipedia.org/wiki/San_Crist%C3%B3bal_de_La_Laguna>`__ (at airport)
* `Santa Cruz de Tenerife
  <https://en.wikipedia.org/wiki/Santa_Cruz_de_Tenerife>`__ (capital)

Towns in the South
^^^^^^^^^^^^^^^^^^

* `Granadilla de Abona
  <https://en.wikipedia.org/wiki/Granadilla_de_Abona>`__ (at airport)
* `Playa de las Américas
  <https://en.wikipedia.org/wiki/Playa_de_las_Am%C3%A9ricas>`__ (nightlife)
* `Los Cristianos
  <https://en.wikipedia.org/wiki/Los_Cristianos>`__ (nightlife)

Unsorted
========

* https://en.wikipedia.org/wiki/Public_holidays_in_Spain
