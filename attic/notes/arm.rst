*************************************************************************
Arto's Notes re: `ARM <https://en.wikipedia.org/wiki/ARM_architecture>`__
*************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Profiles
========

- **Application profile**:
  `ARM Cortex-A
  <https://en.wikipedia.org/wiki/ARM_Cortex-A>`__

- **Microcontroller profile**:
  `ARM Cortex-M
  <https://en.wikipedia.org/wiki/ARM_Cortex-M>`__

- **Real-time profile**:
  `ARM Cortex-R
  <https://en.wikipedia.org/wiki/ARM_Cortex-R>`__

----

See Also
========

- Arto's Notes re: `electronics <electronics>`__
