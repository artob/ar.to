***********************
Arto's Notes re: cities
***********************

Europe
======

* `Athens <athens>`__
* `Amsterdam <amsterdam>`__
* `Barcelona <barcelona>`__
* `Berlin <berlin>`__
* `Bratislava <bratislava>`__
* `Brussels <brussels>`__
* `Budapest <budapest>`__
* `Cambridge <cambridge>`__
* `Cologne <cologne>`__
* `Copenhagen <copenhagen>`__
* `Edinburgh <edinburgh>`__
* `Eindhoven <eindhoven>`__
* `Frankfurt <frankfurt>`__
* `Gibraltar <gibraltar>`__
* `Hamburg <hamburg>`__
* `Helsinki <helsinki>`__
* `Kharkiv <kharkiv>`__
* `Kiev <kiev>`__
* `Kraków <krakow>`__
* `Madrid <madrid>`__
* `Málaga <malaga>`__
* `Marbella <marbella>`__
* `Milan <milan>`__
* `Leipzig <leipzig>`__
* `Lisbon <lisbon>`__
* `London <london>`__
* `Longyearbyen <longyearbyen>`__
* `Lviv <lviv>`__
* `Munich <munich>`__
* `Odessa <odessa>`__
* `Paris <paris>`__
* `Santa Cruz de Tenerife <tenerife>`__
* `Sliema <malta>`__
* `Sofia <sofia>`__
* `Stockholm <stockholm>`__
* `Tallinn <tallinn>`__
* `Turin <turin>`__
* `Valetta <malta>`__
* `Vienna <vienna>`__
* `Warsaw <warsaw>`__
* `Wroclaw <wroclaw>`__
* `Zagreb <zagreb>`__
* `Zurich <zurich>`__

`Japan <japan>`__
=================

* `Nagasaki <nagasaki>`__
* `Tokyo <tokyo>`__

See Also
========

* `List of urban areas in the European Union
  <https://en.wikipedia.org/wiki/List_of_urban_areas_in_the_European_Union>`__
