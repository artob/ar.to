*************************************************************************************************************
Arto's Notes re: `garbage collection <https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)>`__
*************************************************************************************************************

Algorithms
==========

Immix
-----

* https://www.infoq.com/presentations/gc-conservative-immix
* http://lambda-the-ultimate.org/node/4825
* https://speakerdeck.com/brixen/papers-we-love-immix-mark-region-garbage-collector

Implementations
^^^^^^^^^^^^^^^

* https://ghc.haskell.org/trac/ghc/wiki/Commentary/Rts/Storage/GC/Immix
* https://gitlab.anu.edu.au/mu/immix-rust
* https://github.com/xoofx/gcix
