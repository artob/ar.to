********************************************************************
Arto's Notes re: `Ukraine <https://en.wikipedia.org/wiki/Ukraine>`__
********************************************************************

* `Cities <#cities>`__
* `Travel <#travel>`__
* `Languages <#languages>`__
* `Goods <#goods>`__
* `Services <#services>`__
* `Business <#business>`__
* `See Also <#see-also>`__

----

Cities
======

* `Kharkiv <kharkiv>`__ (1.4M+)
* `Kiev <kiev>`__ (officially 2.8M+, unofficially perhaps 4M+)
* `Lviv <lviv>`__ (officially 700K+)
* `Odessa <odessa>`__ (1.0M+)

----

Travel
======

Visa
----

* US and EU citizens don't need to apply for a visa to enter Ukraine.
  **Just make sure to pack your passport.**

----

Languages
=========

`Ukrainian <ukrainian>`__ and
`Russian <russian>`__

----

Goods
=====

* `Rozetka <https://rozetka.com.ua/ua/>`__ (UA/RU)

* `Bigl <https://bigl.ua/>`__ (B2C, best vendors from Prom)
  (`background <https://evo.company/kak-bigl-ua-vyishel-v-lideryi-ukrainskogo-e-commerce/>`__)

* `Prom <https://prom.ua/>`__ (B2B/B2C)

* `27.ua <https://27.ua/>`__

Electronics
-----------

* `Brain <https://brain.com.ua/ukr/Komplektuyuchi_do_PK-c204/>`__ (Kiev, Lviv, etc; UA/RU)

* `Citrus <https://www.citrus.ua/>`__ ()

* `Comfy <https://comfy.ua/ua/shops.html>`__ (Kiev, Lviv, etc)

* `Defis <http://defis.lviv.ua/?option=com_contact>`__ (Lviv)

* `Foxtrot (Фокстрот) <http://www.foxtrot.com.ua/uk/>`__
  (`Lviv <https://lviv.foxtrot.com.ua/uk/>`__; UA/RU)

* `Moyo <https://www.moyo.ua/>`__ ()

* `PCshop <https://pcshop.ua/komplektuyuschie>`__ (Kharkiv; RU)

* `Rozetka <https://hard.rozetka.com.ua/ua/>`__ (UA/RU)

* `Stylus <https://stylus.ua/>`__ ()

DIY
---

* `Epicentr (Епіцентр)
  <https://epicentrk.ua/market/epicentr-lvov-hmelnickogo>`__

Hobbyist Electronics
--------------------

* `DiyLab <https://diylab.com.ua/>`__

* `Arduino-UA.com <https://arduino-ua.com/>`__
  Retailer of iArduino kits.
  Quick shipping.

* `Arduino-Kit <http://arduino-kit.com.ua/>`__

* `Pro Tools <https://rozetka.com.ua/ua/seller/pro-tools/>`__
  good selection of soldering stations, multimeters, and tool kits.

* `BeGreen <http://beegreen.com.ua/>`__
  (also via `Prom.ua <https://bg24.shop/>`__)

Second-Hand
-----------

* `OLX <https://www.olx.ua/uk/>`__

Wholesalers
-----------

TODO

----

Services
========

* `Kabanchik (Кабанчик) <https://kabanchik.ua/>`__

Logistics
---------

* http://uacargo.com.ua/kurerskie-sluzhby-ukrainy

* `Nova Poshta <https://novaposhta.ua/en>`__ (Нова пошта)

* `Nova Poshta Shopping <https://npshopping.com/>`__ (NP Shopping)

* `Meest Express <https://meest-express.com.ua/ua/>`__ (Міст Експрес)

* Ukrposhta

----

Business
========

* `Товари́ство з обме́женою відповіда́льністю (ТОВ/ТзОВ) <https://uk.wikipedia.org/wiki/%D0%A2%D0%BE%D0%B2%D0%B0%D1%80%D0%B8%D1%81%D1%82%D0%B2%D0%BE_%D0%B7_%D0%BE%D0%B1%D0%BC%D0%B5%D0%B6%D0%B5%D0%BD%D0%BE%D1%8E_%D0%B2%D1%96%D0%B4%D0%BF%D0%BE%D0%B2%D1%96%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D1%96%D1%81%D1%82%D1%8E>`__

* https://www.contactukraine.com/company-law/business-structures-in-ukraine

* https://www.contactukraine.com/company-law/register-company-in-ukraine

* https://www.contactukraine.com/company-law/llc-compliance-guide

* https://www.contactukraine.com/company-law/ukraine-llc-model-company-charter

* https://www.contactukraine.com/company-law/private-entrepreneur-registration-guide

* https://www.contactukraine.com/taxation/corporate-tax

* https://www.contactukraine.com/incorporation-form

----

See Also
========
