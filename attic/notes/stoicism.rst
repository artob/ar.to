**********************************************************************
Arto's Notes re: `stoicism <https://en.wikipedia.org/wiki/Stoicism>`__
**********************************************************************

Disciplines
===========

The three Stoic disciplines are:

* Desire: what one ought to desire/want
* Assent: what one ought to think/judge
* Action: how one ought to behave/act/react

`Virtues <virtues>`__
=====================

The four cardinal Stoic virtues are:

* Courage (*andreia*)
* Temperance (*sophrosyne*)
* Justice (*dikaiosyne*)
* Wisdom (*sophia*)

Books
=====

* `A Guide to the Good Life: The Ancient Art of Stoic Joy
  <https://www.goodreads.com/book/show/19669566-a-guide-to-the-good-life>`__
  by William Irvine
* `How to Be a Stoic: Using Ancient Philosophy to Live a Modern Life
  <https://www.goodreads.com/book/show/35102306-how-to-be-a-stoic>`__
  by Massimo Pigliucci

Works
=====

* `De Brevitate Vitae
  <https://en.wikipedia.org/wiki/De_Brevitate_Vitae_(Seneca)>`__
  by Seneca
* `Discourses
  <https://en.wikipedia.org/wiki/Discourses_of_Epictetus>`__
  by Epictetus
* `Enchiridion
  <https://en.wikipedia.org/wiki/Enchiridion_of_Epictetus>`__
  by Epictetus
* `Epistles
  <https://en.wikipedia.org/wiki/Epistulae_Morales_ad_Lucilium>`__
  by Seneca
* `Meditations
  <https://en.wikipedia.org/wiki/Meditations>`__
  by Marcus Aurelius
* `The Republic
  <https://en.wikipedia.org/wiki/The_Republic_(Zeno)>`__
  by Zeno

`Philosophers <https://en.wikipedia.org/wiki/List_of_Stoic_philosophers>`__
===========================================================================

* `Seneca <https://en.wikipedia.org/wiki/Seneca_the_Younger>`__
* `Musonius Rufus <https://en.wikipedia.org/wiki/Gaius_Musonius_Rufus>`__
* `Epictetus <https://en.wikipedia.org/wiki/Epictetus>`__
* `Marcus Aurelius <https://en.wikipedia.org/wiki/Marcus_Aurelius>`__

Links
=====

* `My bookshelf on stoicism
  <https://www.goodreads.com/review/list/22170557?shelf=stoicism>`__
* `The /r/Stoicism subreddit
  <https://www.reddit.com/r/Stoicism/>`__
* `Stoicism in the Stanford Encyclopedia of Philosophy
  <https://plato.stanford.edu/entries/stoicism/>`__
