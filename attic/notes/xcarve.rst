*******************************************************************************
Arto's Notes re: `X-Carve <https://www.inventables.com/technologies/x-carve>`__
*******************************************************************************

* `Books <#books>`__
* `News <#news>`__
* `Forums <#forums>`__
* `Reviews <#reviews>`__
* `Videos <#videos>`__
* `Reference <#reference>`__
* `Purchases <#purchases>`__
* `See Also <#see-also>`__

----

Books
=====

* `Getting Started with 3D Carving
  <https://www.goodreads.com/book/show/35963007>`__
  (2017) by Zach Kaplan

----

News
====

* https://twitter.com/Inventables

* https://twitter.com/zkaplan

Forums
======

* https://discuss.inventables.com

* https://www.reddit.com/r/XCarve/

----

Reviews
=======

* `X-Carve's Features and Community Offer a Great Entry Point for CNC Routing
  <https://makezine.com/2017/04/14/x-carve-review/>`__
  by Make Magazine (2017)

* `X-Carve Upgrade Honest Review
  <https://www.thegeekpub.com/9390/x-carve-upgrade-honest-review/>`__
  (2017)

* `Making Props with the Inventables X-Carve CNC Router
  <http://www.tested.com/art/makers/561314-making-props-inventables-x-carve-cnc-router/>`__
  (2016)

----

Videos
======

* `Winston Moy
  <https://www.youtube.com/channel/UCxdCeHBUOlcCWr6RM8acEog>`__

----

Reference
=========

2016 Model
----------

* http://x-carve-instructions.inventables.com/500mm/

----

Models
======

`500mm <http://x-carve-instructions.inventables.com/500mm/>`__
--------------------------------------------------------------

* https://www.inventables.com/technologies/x-carve/customize#500mm
* https://robosavvy.com/store/x-carve-500.html
* Build Volume: 500×500×67mm

`750mm <http://x-carve-instructions.inventables.com/750mm/>`__
--------------------------------------------------------------

* https://www.inventables.com/technologies/x-carve/customize#750mm
* https://robosavvy.com/store/x-carve-750mm.html

`1000mm <http://x-carve-instructions.inventables.com/1000mm/>`__
----------------------------------------------------------------

* https://www.inventables.com/technologies/x-carve/customize#1000mm
* https://robosavvy.com/store/x-carve-1000mm.html

`2015 <http://x-carve-instructions.inventables.com/xcarve2015/>`__
------------------------------------------------------------------

* http://x-carve-instructions.inventables.com/upgrade/
* https://www.inventables.com/technologies/gshield

----

Purchases
=========

Americas
--------

* https://www.inventables.com/technologies/x-carve/customize

Europe
------


* https://robosavvy.com/store/x-carve-purchase-guide

* https://robosavvy.com/store/cnc?limit=all&manufacturer=127

----

See Also
========

* https://www.cnccookbook.com/interview-zach-kaplan-new-x-carve-cnc-router/

* http://chilipeppr2.blogspot.com/2015/06/grbl-vs-tinyg.html

`CAD <cad>`__, `CNC <cnc>`__
