**********************************************************************************************
Arto's Notes re: `neural networks <https://en.wikipedia.org/wiki/Artificial_neural_network>`__
**********************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Books
=====

- `Make Your Own Neural Network
  <https://www.goodreads.com/book/show/29936790>`__
  (2016) by Tariq Rashid
  (`@Amazon <https://www.amazon.com/dp/B01EER4Z4G>`__,
  `@Twitter <https://twitter.com/myoneuralnet>`__,
  `blog <http://makeyourownneuralnetwork.blogspot.com/>`__,
  `talk <https://www.youtube.com/watch?v=2sevic5Vy4E>`__)

- `Neural Networks: A Visual Introduction For Beginners
  <https://www.goodreads.com/book/show/36153846>`__
  (2017) by Michael Taylor

See Also
--------

- `Arto's Deep Learning bookshelf
  <https://www.goodreads.com/review/list/22170557?shelf=deep-learning>`__

- `Artificial Neural Networks and Deep Learning
  <https://www.goodreads.com/list/show/89481>`__ community bookshelf.

----

See Also
========

`artificial intelligence (AI) <ai>`__,
`deep learning <dl>`__,
`machine learning (ML) <ml>`__,
`fast.ai <fastai>`__,
`OpenAI <openai>`__,
`TensorFlow <tensorflow>`__
