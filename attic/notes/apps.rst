*********************
Arto's Notes re: apps
*********************

`Android <android>`__
=====================

Google
------

* `Open GApps (mini)
  <https://github.com/opengapps/opengapps/wiki/Mini-Package>`__
  (`FAQ <https://github.com/opengapps/opengapps/wiki/FAQ>`__)
* `Gmail
  <https://play.google.com/store/apps/details?id=com.google.android.gm>`__
* `Google Chrome
  <https://play.google.com/store/apps/details?id=com.android.chrome>`__
* `Google Contacts
  <https://play.google.com/store/apps/details?id=com.google.android.contacts>`__
* `Google Fit
  <https://play.google.com/store/apps/details?id=com.google.android.apps.fitness>`__
* `Google Maps
  <https://play.google.com/store/apps/details?id=com.google.android.apps.maps>`__
* `Google Translate
  <https://play.google.com/store/apps/details?id=com.google.android.apps.translate>`__

Shortlist
---------

* `1Password
  <https://play.google.com/store/apps/details?id=com.agilebits.onepassword>`__
* `Amazon Kindle
  <https://play.google.com/store/apps/details?id=com.amazon.kindle>`__
* `Dropbox
  <https://play.google.com/store/apps/details?id=com.dropbox.android>`__
* `Evernote
  <https://play.google.com/store/apps/details?id=com.evernote>`__
* `StrongLifts 5x5 Workout
  <https://play.google.com/store/apps/details?id=com.stronglifts.app>`__
* `VLC
  <https://play.google.com/store/apps/details?id=org.videolan.vlc>`__

Comms
-----

* `Facebook Messenger
  <https://play.google.com/store/apps/details?id=com.facebook.orca>`__
* `Signal Private Messenger
  <https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms>`__
* `Skype
  <https://play.google.com/store/apps/details?id=com.skype.raider>`__
* `Telegram
  <https://play.google.com/store/apps/details?id=org.telegram.messenger>`__
* `Zoiper IAX SIP VoIP Softphone
  <https://play.google.com/store/apps/details?id=com.zoiper.android.app>`__
* `WhatsApp Messenger
  <https://play.google.com/store/apps/details?id=com.whatsapp>`__

Study
-----

* `AnkiDroid Flashcards
  <https://play.google.com/store/apps/details?id=com.ichi2.anki>`__
* `Babbel
  <https://play.google.com/store/apps/details?id=com.babbel.mobile.android.en>`__
* `Coursera
  <https://play.google.com/store/apps/details?id=org.coursera.android>`__
* `edX
  <https://play.google.com/store/apps/details?id=org.edx.mobile>`__
* `OU Anywhere
  <https://play.google.com/store/apps/details?id=uk.ac.open.ouanywhere>`__

Work
----

* `Basecamp 3
  <https://play.google.com/store/apps/details?id=com.basecamp.bc3>`__
* `FastMail
  <https://play.google.com/store/apps/details?id=com.fastmail.app>`__
* `Slack
  <https://play.google.com/store/apps/details?id=com.Slack>`__

Operators
---------

* `My Vodafone
  <https://play.google.com/store/apps/details?id=ua.vodafone.myvodafone>`__

Travel
------

* `Booking.com
  <https://play.google.com/store/apps/details?id=com.booking>`__
* `Ryanair
  <https://play.google.com/store/apps/details?id=com.ryanair.cheapflights>`__
* `Uber
  <https://play.google.com/store/apps/details?id=com.ubercab>`__

Miscellaneous
-------------

* `ES File Explorer
  <https://play.google.com/store/apps/details?id=com.estrongs.android.pop>`__
* `JuiceSSH
  <https://play.google.com/store/apps/details?id=com.sonelli.juicessh>`__
* `RunKeeper
  <https://play.google.com/store/apps/details?id=com.fitnesskeeper.runkeeper.pro>`__

`macOS <mac>`__
===============

Shortlist
---------

* `1Password 6 <https://agilebits.com/downloads>`__
* `Android File Transfer <https://www.android.com/filetransfer/>`__
* `Dropbox <https://www.dropbox.com/install?os=mac>`__
* `Evernote <https://evernote.com/download/>`__
* `Google Chrome <https://www.google.com/chrome/browser/desktop/>`__
* `Little Snitch <https://www.obdev.at/products/littlesnitch/download.html>`__
* `MacPorts <https://www.macports.org/install.php>`__
* `Private Internet Access <https://www.privateinternetaccess.com/pages/client-support/>`__
* `Signal Desktop <https://chrome.google.com/webstore/detail/signal-private-messenger/bikioccmkafdpakkkcpdbppfkghcmihk>`__
* `Skype <https://www.skype.com/en/download-skype/skype-for-computer/>`__
* `Sublime Text <https://www.sublimetext.com/>`__
* `Telegram <https://telegram.org/dl/osx>`__
* `TextMate 2 <https://macromates.com/download>`__
* `Textual 5 <https://www.codeux.com/textual/>`__
* `VLC <https://www.videolan.org/vlc/download-macosx.html>`__
* `VMware Fusion 8 <https://my.vmware.com/web/vmware/info?slug=desktop_end_user_computing/vmware_fusion/7_0>`__
* `VoodooPad <https://plausible.coop/voodoopad/>`__
* `Xcode Command-Line Tools <http://guide.macports.org/#installing.xcode>`__

Everything
----------

* 1Password
* Adobe Reader
* `Airmail <http://airmailapp.com/>`__
* Android File Transfer
* `AppCleaner <https://freemacsoft.net/appcleaner/>`__
* Bonjour Browser
* Calibre
* Dropbox
* `Eclipse <#>`__
* Evernote
* `Firefox <#>`__
* Gephi
* Google Chrome
* Graphviz
* `Harvest <#>`__
* `Heroku Toolbelt <https://toolbelt.heroku.com>`__
* Keynote
* `Kindle for Mac <#>`__
* `Little Snitch <https://www.obdev.at/products/littlesnitch/download.html>`__
* `MacPorts <https://www.macports.org/install.php>`__
* `MacTeX <#>`__
* `MATLAB <#>`__
* Numbers
* `OmniGraffle <#>`__
* Pages
* Private Internet Access
* `Sequel Pro <http://www.sequelpro.com/download>`__
* `Skitch <#>`__
* Skype
* `Slack <#>`__
* Sublime Text
* Telegram
* TextMate
* Textual 5
* Wire
* VLC
* VMware Fusion
* VNC Viewer
* VoodooPad
* Xcode (Command-Line Tools)

Discontinued
------------

* Adium
* uTorrent
