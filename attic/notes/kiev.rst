**************************************************************
Arto's Notes re: `Kiev <https://en.wikipedia.org/wiki/Kiev>`__
**************************************************************

* `Travel <#travel>`__
* `Goods <#goods>`__
* `Services <#services>`__
* `Events <#events>`__
* `Activities <#activities>`__
* `Languages <#languages>`__
* `See Also <#see-also>`__

----

Travel
======

Visa
----

* US and EU citizens don't need to apply for a visa to enter Ukraine.
  **Just make sure to pack your passport.**

----

Goods
=====

TODO

----

Services
========

Gyms
----

* `SkyFitness <http://eng.skyfitness.com.ua/>`__
  ($200+/month)

Taxi
----

* Uber

* `Uklon <http://www.uklon.com.ua/>`__

Opticians
---------

TODO

----

Events
======

Technology
----------

* `Elixir Club Kiev
  <https://www.facebook.com/elixirkyiv>`__

* `R0boCamp <http://kyiv.robocamp.com.ua/>`__

Miscellaneous
-------------

* https://www.meetup.com/cities/ua/kyiv/

----

Activities
==========

TODO

----

Languages
=========

Russian
-------

Tutors
^^^^^^

* `Russian language tutors in Kiev via Preply
  <https://preply.com/en/kiev/russian-tutors>`__

Courses
^^^^^^^

* `AMBergh Education
  <http://www.ambergh.com/learn-russian/kiev>`__

* `ECHO Eastern Europe
  <https://echoee.com/kyiv-language-school/>`__

* `Eurolingua
  <http://www.eurolingua.com/russian/learn-russian-courses>`__

* `NovaMova
  <http://novamova.net/russian-schools/kiev>`__

Ukrainian
---------

Tutors
^^^^^^

* `Ukrainian language tutors in Kiev via Preply
  <https://preply.com/en/kiev/ukrainian-tutors>`__

Courses
^^^^^^^

* `ECHO Eastern Europe
  <https://echoee.com/kyiv-language-school/>`__

* `Eurolingua
  <http://www.eurolingua.com/russian/learn-ukrainian>`__

----

See Also
========

`Ukraine <ukraine>`__: `Kharkiv <kharkiv>`__, `Lviv <lviv>`__, and `Odessa <odessa>`__
