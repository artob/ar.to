**************************************************************************
Arto's Notes re: `censorship <https://en.wikipedia.org/wiki/Censorship>`__
**************************************************************************

Essays
======

Essays on Political Correctness
-------------------------------

* `What You Can't Say
  <http://www.paulgraham.com/say.html>`__
  &
  `Re: What You Can't Say
  <http://www.paulgraham.com/resay.html>`__
  by `Paul Graham <https://en.wikipedia.org/wiki/Paul_Graham_(computer_programmer)>`__

* `The Sound of Silence
  <http://foundersatwork.posthaven.com/the-sound-of-silence>`__
  by `Jessica Livingston <https://en.wikipedia.org/wiki/Jessica_Livingston>`__

* `E Pur Si Muove
  <http://blog.samaltman.com/e-pur-si-muove>`__
  by `Sam Altman <https://en.wikipedia.org/wiki/Sam_Altman>`__

* `Kolmogorov Complicity and the Parable of Lightning
  <http://slatestarcodex.com/2017/10/23/kolmogorov-complicity-and-the-parable-of-lightning/>`__
  by `Scott Alexander <https://twitter.com/slatestarcodex>`__

* `Silicon Valley would be wise to follow China's lead
  <https://www.ft.com/content/42daca9e-facc-11e7-9bfc-052cbba03425>`__
  by Michael Moritz

See Also
========

* `Horseshoe theory
  <https://en.wikipedia.org/wiki/Horseshoe_theory>`__

* `Overton window
  <https://en.wikipedia.org/wiki/Overton_window>`__
