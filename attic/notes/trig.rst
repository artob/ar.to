******************************************************************************
Arto's Notes re: `trigonometry <https://en.wikipedia.org/wiki/Trigonometry>`__
******************************************************************************

The `mathematical <math>`__ study of relationships involving lengths and
angles of triangles.

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Guides
======

- `How To Learn Trigonometry Intuitively
  <https://betterexplained.com/articles/intuitive-trigonometry/>`__
  at Better Explained

----

See Also
========

- Arto's Notes re: `mathematics <math>`__ (`geometry <geometry>`__)

- `Trigonometry questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/trigonometry>`__
