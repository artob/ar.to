*******************************************************************************
Arto's Notes re: `PCBs <https://en.wikipedia.org/wiki/Printed_circuit_board>`__
*******************************************************************************

* https://en.wikipedia.org/wiki/Printed_circuit_board_milling

Software
========

* `Autodesk EAGLE <https://en.wikipedia.org/wiki/EAGLE_(program)>`__

See Also
========

`CAD <cad>`__
