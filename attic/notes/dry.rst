*************************************************
Arto's Notes re: `DRY <https://dryproject.org>`__
*************************************************

* https://dryproject.org
* https://github.com/dryproject

Repositories
============

* `Umbrella project <https://github.com/dryproject>`__

Languages
---------

* `C11 <https://github.com/dryc>`__
* `C++ <https://github.com/drycpp>`__
* `OCaml <https://github.com/drycaml>`__
* `Lisp <https://github.com/drylisp>`__
* `Lua <https://github.com/drylua>`__
* `PHP <https://github.com/dryphp>`__
* `Python <https://github.com/drypy>`__
* `Ruby <https://github.com/dryruby>`__

Tracking
========

* https://github.com/topics/dryproject

See Also
========

`DRYlang <drylang>`__, `DRYlib <drylib>`__
