*******************************************************************************
Arto's Notes re: `C <https://en.wikipedia.org/wiki/C_(programming_language)>`__
*******************************************************************************

Snippets
========

Version Requirement
-------------------

::

   #if __STDC_VERSION__ < 201112L
   #error "This project requires a C11 compiler (CFLAGS='-std=c11')"
   #endif

Links
=====

* https://en.wikichip.org/wiki/c/c11

* `The Curious Case of the Longevity of C
  <https://www.ahl.com/ahl-tech-the-curious-case-of-the-longevity-of-c>`__

* `Comments on the MISRA C coding guidelines
  <http://www.knosof.co.uk/misracom.html>`__

* `C∀ (C-for-all)
  <https://plg.uwaterloo.ca/~cforall/>`__
