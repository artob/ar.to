******************************************************************************
Arto's Notes re: `Lapland <https://en.wikipedia.org/wiki/Lapland_(Finland)>`__
******************************************************************************

`Enontekiö <https://en.wikipedia.org/wiki/Enonteki%C3%B6>`__
============================================================

* `Enontekiö Airport
  <https://en.wikipedia.org/wiki/Enonteki%C3%B6_Airport>`__
* `Flights to Enontekiö with Finnair
  <https://www.finnair.com/fi/gb/destinations/finland/lapland/enontekio>`__
* https://en.wikivoyage.org/wiki/Enonteki%C3%B6
* http://www.enontekio.fi/en/
* http://www.tosilappi.fi/en/home/

`Kilpisjärvi <https://en.wikipedia.org/wiki/Kilpisj%C3%A4rvi>`__
----------------------------------------------------------------

* http://www.kilpisjarvi.org/en/
* https://en.wikivoyage.org/wiki/Kilpisj%C3%A4rvi
* http://kilpissafarit.fi/en/kaamos-lahenee/

`Ivalo <https://en.wikipedia.org/wiki/Ivalo>`__
===============================================

* `Flights to Ivalo with Finnair
  <https://www.finnair.com/fi/gb/destinations/finland/ivalo>`__
* `Flights to Ivalo with Norwegian
  <http://www.norwegian.com/en/destinations/ivalo>`__

`Kemijärvi <https://en.wikipedia.org/wiki/Kemij%C3%A4rvi>`__
============================================================

* https://en.wikivoyage.org/wiki/Kemij%C3%A4rvi

`Pyhätunturi <https://fi.wikipedia.org/wiki/Pyh%C3%A4tunturi_(Pelkosenniemi)>`__
--------------------------------------------------------------------------------

* https://pyha.fi/en/
* https://pyha.fi/en/about/getting-to-pyha

`Suomu <https://en.wikipedia.org/wiki/Suomu>`__
-----------------------------------------------

* https://en.wikivoyage.org/wiki/Suomu
* http://www.suomutunturi.fi/english/

`Kittilä <https://en.wikipedia.org/wiki/Kittil%C3%A4>`__
========================================================

* `Flights to Kittilä with Finnair
  <https://www.finnair.com/fi/gb/destinations/finland/kittila>`__
* `Flights to Kittilä with Norwegian
  <http://www.norwegian.com/en/destinations/kittila>`__
* https://en.wikivoyage.org/wiki/Kittil%C3%A4

`Levi <https://en.wikipedia.org/wiki/Levi,_Finland>`__
------------------------------------------------------

* http://www.levi.fi/en/
* https://en.wikivoyage.org/wiki/Levi

`Kolari <https://en.wikipedia.org/wiki/Kolari>`__
=================================================

* `Kolari railway station
  <https://en.wikipedia.org/wiki/Kolari_railway_station>`__

`Ylläs <https://en.wikipedia.org/wiki/Yll%C3%A4s>`__
----------------------------------------------------

* http://www.yllas.fi/en/
* http://www.yllas.fi/en/area-info/transportation-and-connections.html
* http://www.yllas.fi/en/area-info/maps.html

`Rovaniemi <https://en.wikipedia.org/wiki/Rovaniemi>`__
=======================================================

* `Flights to Rovaniemi with Finnair
  <https://www.finnair.com/fi/gb/destinations/finland/rovaniemi>`__
* `Flights to Rovaniemi with Norwegian
  <http://www.norwegian.com/en/destinations/rovaniemi>`__
* https://en.wikivoyage.org/wiki/Rovaniemi

`Ounasvaara <https://fi.wikipedia.org/wiki/Ounasvaara>`__
---------------------------------------------------------

* http://ounasvaara.fi/en/

See Also
========

* https://en.wikivoyage.org/wiki/Finnish_Lapland
* `Arctic Circle <https://en.wikipedia.org/wiki/Arctic_Circle>`__
* `Midnight sun <https://en.wikipedia.org/wiki/Midnight_sun>`__
* `Polar night <https://en.wikipedia.org/wiki/Polar_night>`__
* `Polar lights <https://en.wikipedia.org/wiki/Aurora>`__
* `Finnish long-distance railway map <https://www.vr.fi/cs/vr/en/long-distance_service_network>`__
* `Finnish long-distance buses <https://matkahuolto.fi/en/>`__
