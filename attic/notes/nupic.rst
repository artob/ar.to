******************************************************************
Arto's Notes re: `NuPIC <https://en.wikipedia.org/wiki/Numenta>`__
******************************************************************

* https://numenta.org/
* https://twitter.com/numenta
* https://github.com/numenta

See Also
========

`artificial intelligence (AI) <ai>`__,
`machine learning (ML) <ml>`__
