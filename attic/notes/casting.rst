********************************************************************************
Arto's Notes re: `resin casting <https://en.wikipedia.org/wiki/Resin_casting>`__
********************************************************************************

* https://www.reddit.com/r/ResinCasting/

* http://lcamtuf.coredump.cx/gcnc/

* https://makezine.com/2014/03/21/resin-casting-going-from-cad-to-engineering-grade-plastic-parts/

* https://makezine.com/2011/01/12/simple-molding-and-casting-for-toy/

* http://ericweinhoffer.com/resin-casting/

* http://ericweinhoffer.com/blog/2016/8/7/resin-casting-part-1

* https://hackaday.com/2016/07/14/learn-resin-casting-techniques-cold-casting/

* https://hackaday.com/2016/03/09/materials-to-know-tooling-and-modeling-board/

* https://hackaday.com/2012/04/03/who-needs-mecanum-wheels/

* http://www.instructables.com/id/Two-Part-Silicone-Casting/

* https://en.wikipedia.org/wiki/Molding_(process)

* https://en.wikipedia.org/wiki/Casting

* https://en.wikipedia.org/wiki/Curing_(chemistry)

* https://www.freemansupply.com/products/machinable-media/renshape-modeling-and-styling-boards/renshape-460-medium-high-density-modeling-board

* https://en.wikipedia.org/wiki/Medium-density_fibreboard
