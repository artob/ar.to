*************************************************************************************
Arto's Notes re: `Rust <https://en.wikipedia.org/wiki/Rust_(programming_language)>`__
*************************************************************************************

Installation
============

macOS
-----

::

   $ brew install rust

   $ rustc --version

   $ cargo --version

Reference
=========

* https://doc.rust-lang.org/std/

Tutorials
=========

* https://learning-rust.github.io

Books
=====

* `The Rust Programming Language
  <https://doc.rust-lang.org/book/second-edition/>`__

* `miscellaneous <https://github.com/sger/RustBooks>`__

Talks
=====

* `The Rust Programming Language
  <https://www.youtube.com/watch?v=d1uraoHM8Gg>`__
  (2015)

Comments
========

* https://blog.rust-lang.org/2015/04/10/Fearless-Concurrency.html
* http://venge.net/graydon/talks/intro-talk-2.pdf

By Graydon Hoare
----------------

* `Rust is mostly safety
  <http://graydon2.dreamwidth.org/247406.html>`__
  (2016)
* `Things Rust shipped without
  <http://graydon2.dreamwidth.org/218040.html>`__
  (2015)
* `Five lists of six things about Rust
  <http://graydon2.dreamwidth.org/214016.html>`__
  (2015)
* `More cores, more fun
  <http://graydon2.dreamwidth.org/201806.html>`__
  (2015)
