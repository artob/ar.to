*************************************************************************
Arto's Notes re: `gunsmithing <https://en.wikipedia.org/wiki/Gunsmith>`__
*************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Classes
=======


----

Books
=====

- `Arto's bookshelf <>`__

----

See Also
========

- Arto's Notes re: `rifles <rifles>`__, `pistols <pistols>`__,
  `shooting sports <shooting>`__
