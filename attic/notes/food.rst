**********************************
Arto's Notes re: shelf-stable food
**********************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Brands
======

.. list-table::
   :widths: 80 10 10
   :header-rows: 1

   * - Vendor
     - Origin
     - Shelf Life

   * - `Mountain House <https://www.mountainhouse.com>`__
     - US
     - 30y+

   * - `Fuel Your Preparation <https://www.fuelyourpreparation.com>`__
     - UK
     - 7y+

   * - `U.S. Army MREs <https://en.wikipedia.org/wiki/Meal,_Ready-to-Eat>`__
     - US
     - 3-5y

   * - `Travellunch <https://www.travellunch.de/en/>`__
     - DE
     - 5y+

   * - `Trek'n Eat <https://www.trekneat.com/en/de>`__
     - DE
     - 2y

   * - `Voyager Outdoor Meals <https://www.outdoor-food.co.uk>`__
     - FR
     - 1y
