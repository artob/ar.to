*************************************************************************************
Arto's Notes re: `OSVR <https://en.wikipedia.org/wiki/Open_Source_Virtual_Reality>`__
*************************************************************************************

`HDK 2 <http://www.osvr.org/hdk2.html>`__
=========================================

* 2160x1200 dual-display resolution (1080x1200 per eye) 441 PPI, running at 90 FPS
* Infrared camera operating at 100 Hz
* Sensor hub with integrated accelerometer, gyroscope, and compass
* External USB 3.0 connection for additional accessories
* Additional 2x USB 3.0 connectors for internal expansion

Unsorted
========

* https://www.razerzone.com/eu-en/hdk2-promo
* https://osvr.github.io/doc/installing/osx/
* https://osvr.github.io/doc/installing/linux/
* https://www.reddit.com/r/OSVR/
* https://www.reddit.com/user/OSVR
* https://github.com/OSVR/OSVR-Core
* http://resource.osvr.com/docs/OSVR-Core/md_TopicWritingClientApplication.html
* http://resource.osvr.com/docs/OSVR-Core/group__ClientKit.html
* https://github.com/OSVR/OSVR-Core/tree/master/examples/clients/opengl-sdl
