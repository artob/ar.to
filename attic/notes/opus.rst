*****************************************************************************
Arto's Notes re: `Opus <https://en.wikipedia.org/wiki/Opus_(audio_format)>`__
*****************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

See Also
========

- Arto's Notes re: `AV1 <av1>`__
