************************************************************************************
Arto's Notes re: `batteries <https://en.wikipedia.org/wiki/List_of_battery_sizes>`__
************************************************************************************

`Li-ion (lithium-ion) <https://en.wikipedia.org/wiki/Lithium-ion_battery>`__
============================================================================

* 18650 18x65mm 3.7V 1500-3600mAh (can be larger with protection circuit)
* 19670 19x67mm 3.7V 1500-3600mAh (correct designation of protected 18650)

References
----------

* https://batterybro.com/
* https://reactual.com/outdoor-equipment-2/power-sources/best-18650-batteries.html
* http://www.ebay.com/gds/18650-Battery-Buying-Guide-/10000000177628747/g.html

LiPo (lithium polymer)
======================

* 1S: 3.7V nominal (3.0V min, 4.2V max)
* 2S: 7.4V nominal (6.0V min, 8.4V max)
* 3S: 11.1V nominal (9.0V min, 12.6V max)
* 4S: 14.8V nominal (12.0V min, 16.8V max)

References
----------

* https://rogershobbycenter.com/lipoguide/
* http://www.instructables.com/id/Lithium-Polymer-Etiquette/
* https://learn.adafruit.com/li-ion-and-lipoly-batteries/overview

NiMH
====

* AA (aka 14500) 14x50mm
