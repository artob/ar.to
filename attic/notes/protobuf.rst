**************************************************************************************
Arto's Notes re: `Protocol Buffers <https://en.wikipedia.org/wiki/Protocol_Buffers>`__
**************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- https://opensource.google.com/projects/protobuf
- https://developers.google.com/protocol-buffers/
- https://developers.google.com/protocol-buffers/docs/proto3
- https://twitter.com/hashtag/protobuf?lang=en
- https://github.com/protocolbuffers/protobuf
- https://stackoverflow.com/tags/protocol-buffers

----

Languages
=========

- `C++ <https://github.com/protocolbuffers/protobuf/tree/master/src>`__

- `C# <https://github.com/protocolbuffers/protobuf/tree/master/csharp>`__

- `Dart <https://github.com/dart-lang/protobuf>`__

- `Elixir <https://github.com/tony612/protobuf-elixir>`__ (third-party)

- `Erlang <https://github.com/tomas-abrahamsson/gpb>`__ (third-party)

- `Go <https://github.com/golang/protobuf>`__

- `Java <https://github.com/protocolbuffers/protobuf/tree/master/java>`__

- `JS <https://github.com/protocolbuffers/protobuf/tree/master/js>`__

- `OCaml <https://github.com/mransan/ocaml-protoc>`__ (third-party)

- `Objective-C <https://github.com/protocolbuffers/protobuf/tree/master/objectivec>`__

- `PHP <https://github.com/protocolbuffers/protobuf/tree/master/php>`__

- `Python <https://github.com/protocolbuffers/protobuf/tree/master/python>`__

- `Ruby <https://github.com/protocolbuffers/protobuf/tree/master/ruby>`__

----

Installation
============

::

   $ mix --force escript.install hex protobuf         # Elixir
   $ go get github.com/golang/protobuf/protoc-gen-go  # Go
   $ npm install google-protobuf                      # Node.js
   $ pecl install protobuf                            # PHP
   $ pip3 install protobuf                            # Python 3
   $ gem install google-protobuf                      # Ruby

Installation on `macOS <mac>`__
-------------------------------

https://github.com/Homebrew/homebrew-core/blob/master/Formula/protobuf.rb

::

   $ brew install protobuf

Installation on `Alpine <alpine>`__
-----------------------------------

https://pkgs.alpinelinux.org/package/edge/main/x86_64/protobuf

::

   $ apk add protobuf

Installation on `Ubuntu <ubuntu>`__
-----------------------------------

https://packages.ubuntu.com/libprotobuf-dev

::

   $ sudo apt install libprotobuf10 libprotobuf-dev

----

Tutorials
=========

Basics
------

- `C++ <https://developers.google.com/protocol-buffers/docs/cpptutorial>`__

- `C# <https://developers.google.com/protocol-buffers/docs/csharptutorial>`__

- `Go <https://developers.google.com/protocol-buffers/docs/gotutorial>`__

- `Java <https://developers.google.com/protocol-buffers/docs/javatutorial>`__

- `Python <https://developers.google.com/protocol-buffers/docs/pythontutorial>`__

----

See Also
========

- Arto's Notes re: `gRPC <grpc>`__, `polyglotism <polyglot>`__
