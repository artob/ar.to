*******************************************************************************
Arto's Notes re: `D <https://en.wikipedia.org/wiki/D_(programming_language)>`__
*******************************************************************************

Installation
============

macOS
-----

::

   $ brew install dmd

   $ dmd --version

Features
========

Better C
--------

* https://dlang.org/dmd-windows.html#switch-betterC
* https://dlang.org/spec/betterc.html
* https://dlang.org/changelog/2.076.0.html

Comments
========

* `D as a Better C
  <https://dlang.org/blog/2017/08/23/d-as-a-better-c/>`__
  (2017) by Walter Bright
