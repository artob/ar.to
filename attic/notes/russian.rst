*****************************************************************************
Arto's Notes re: `Russian <https://en.wikipedia.org/wiki/Russian_language>`__
*****************************************************************************

TODO

Alphabet
========

.. table::

   ==== == == == ===============================================================
   #    B  s     Name
   ==== == == == ===============================================================
    1.  А  а     `a <https://en.wikipedia.org/wiki/A_(Cyrillic)>`__
    2.  Б  б     `be <https://en.wikipedia.org/wiki/Be_(Cyrillic)>`__
    3.  В  в     `ve <https://en.wikipedia.org/wiki/Ve_(Cyrillic)>`__
    4.  Г  г     `ge <https://en.wikipedia.org/wiki/Ge_(Cyrillic)>`__
    5.  Д  д     `de <https://en.wikipedia.org/wiki/De_(Cyrillic)>`__
    6.  Е  е     `ye <https://en.wikipedia.org/wiki/Ye_(Cyrillic)>`__
    7.  Ё  ё     `yo <https://en.wikipedia.org/wiki/Yo_(Cyrillic)>`__
    8.  Ж  ж     `zhe <https://en.wikipedia.org/wiki/Zhe_(Cyrillic)>`__
    9.  З  з     `ze <https://en.wikipedia.org/wiki/Ze_(Cyrillic)>`__
   10.  И  и     `i <https://en.wikipedia.org/wiki/I_(Cyrillic)>`__
   11.  Й  й     `i (short) <https://en.wikipedia.org/wiki/Short_I>`__
   12.  К  к     `ka <https://en.wikipedia.org/wiki/Ka_(Cyrillic)>`__
   13.  Л  л     `el <https://en.wikipedia.org/wiki/El_(Cyrillic)>`__
   14.  М  м     `em <https://en.wikipedia.org/wiki/Em_(Cyrillic)>`__
   15.  Н  н     `en <https://en.wikipedia.org/wiki/En_(Cyrillic)>`__
   16.  О  о     `o <https://en.wikipedia.org/wiki/O_(Cyrillic)>`__
   17.  П  п     `pe <https://en.wikipedia.org/wiki/Pe_(Cyrillic)>`__
   18.  Р  р     `er <https://en.wikipedia.org/wiki/Er_(Cyrillic)>`__
   19.  С  с     `es <https://en.wikipedia.org/wiki/Es_(Cyrillic)>`__
   20.  Т  т     `te <https://en.wikipedia.org/wiki/Te_(Cyrillic)>`__
   21.  У  у     `u <https://en.wikipedia.org/wiki/U_(Cyrillic)>`__
   22.  Ф  ф     `ef <https://en.wikipedia.org/wiki/Ef_(Cyrillic)>`__
   23.  Х  х     `kha <https://en.wikipedia.org/wiki/Kha_(Cyrillic)>`__
   24.  Ц  ц     `tse <https://en.wikipedia.org/wiki/Tse_(Cyrillic)>`__
   25.  Ч  ч     `che <https://en.wikipedia.org/wiki/Che_(Cyrillic)>`__
   26.  Ш  ш     `sha <https://en.wikipedia.org/wiki/Sha_(Cyrillic)>`__
   27.  Щ  щ     `shcha <https://en.wikipedia.org/wiki/Shcha>`__
   28.  Ъ  ъ     `yer (hard sign) <https://en.wikipedia.org/wiki/Yer>`__
   29.  Ы  ы     `yery <https://en.wikipedia.org/wiki/Yery>`__
   30.  Ь  ь  '  `yeri (soft sign) <https://en.wikipedia.org/wiki/Soft_sign>`__
   31.  Э  э     `e <https://en.wikipedia.org/wiki/E_(Cyrillic)>`__
   32.  Ю  ю     `yu <https://en.wikipedia.org/wiki/Yu_(Cyrillic)>`__
   33.  Я  я     `ya <https://en.wikipedia.org/wiki/Ya_(Cyrillic)>`__
   ==== == == == ===============================================================

Numbers
=======

Cardinal
--------

один, два, три, четыре, пять, шесть, семь, восемь, девять, и десять.

(odin, dva, tri, chetyre, pyat', shest', sem', vosem', devyat', i desyat'.)

1. один (odin)
2. два (dva)
3. три (tri)
4. четыре (chetyre)
5. пять (pyat')
6. шесть (shest')
7. семь (sem')
8. восемь (vosem')
9. девять (devyat')
10. десять (desyat')

Pronouns
========

.. table::

   ======= ============
   Russian English
   ======= ============
   он      he
   она     she
   оно     it
   это     this/that/it
   ======= ============

Possessive
----------

Correlatives
============

.. table::

   ============ ======
   what         что
   that         тот
   why          Зачем
   when         когда
   then         затем
   where        где
   here         вот
   there        там
   how          как
   who          кто
   whose        чья
   this/that/it это
   ============ ======

Gender
======

Heuristics
----------

* Most male beings are masculine, and so are days, towns, and languages.
* You can nearly always tell the gender of a noun from its ending.
* Masculine nouns normally end with a consonant or an **-й** diphthong.
* Feminine nouns normally end with **-а** or **-я**.
* Neuter nouns almost always end with **-о** or **-е**.
* Most nouns ending with **-ь** (soft sign) are feminine, but there are many
  masculine ones too; it is necessary to memorize the gender of soft-sign
  nouns.
* Nouns ending **-а** or **-я** which denote males are masculine.
* If a noun ends with **-и** or **-у** or **-ю**, it is likely of foreign
  origin and neuter.
* Foreign words denoting females are feminine, regardless of their endings.

Verbs
=====

* Russian infinitive forms normally end with **-ть** (e.g. курить, to smoke).

Words
=====

* Арто (Arto)
* `Европа <https://ru.wikipedia.org/wiki/%D0%95%D0%B2%D1%80%D0%BE%D0%BF%D0%B0>`__ (Europe)
* `Финляндия <https://ru.wikipedia.org/wiki/%D0%A4%D0%B8%D0%BD%D0%BB%D1%8F%D0%BD%D0%B4%D0%B8%D1%8F>`__ (Finland)
* `Хельсинки <https://ru.wikipedia.org/wiki/%D0%A5%D0%B5%D0%BB%D1%8C%D1%81%D0%B8%D0%BD%D0%BA%D0%B8>`__ (Helsinki)

Toasts
------

* За нас! (To us!)
* За здоровье! (Cheers! lit. To health!)

Courses
=======

* `Babbel <https://www.babbel.com/learn-russian-online>`__

See Also
========

* `Ukrainian <ukrainian>`__
