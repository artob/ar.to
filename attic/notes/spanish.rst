*****************************************************************************
Arto's Notes re: `Spanish <https://en.wikipedia.org/wiki/Spanish_language>`__
*****************************************************************************

Grammar
=======

Adjectives
----------

* Most adjectives end with the suffixes **-o** (masculine) or **-a**
  (feminine). The suffix is to agree with the gender of the pronoun or
  the noun.
* Adjectives that end with the suffix **-e** or with a consonant only have
  one form. They don't change regardless of the gender of the noun they
  refer to.
* To make an adjective plural, you often just add the suffix **-s**.
  When an adjective ends in a consonant, add an **-es** to the end.
  (Note that the singular **-z** ending becomes **-ces** in the plural.)

Unsorted
========

* https://www.goodreads.com/review/list/22170557-arto-bendiken?shelf=spanish-language
