********************************************************************
Arto's Notes re: `algebra <https://en.wikipedia.org/wiki/Algebra>`__
********************************************************************

The `mathematical <math>`__ study of symbols and rules for manipulating those symbols.

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Divisions
=========

`Elementary Algebra <https://en.wikipedia.org/wiki/Elementary_algebra>`__
-------------------------------------------------------------------------

- `Precalculus algebra questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/algebra-precalculus>`__

----

`Linear Algebra <https://en.wikipedia.org/wiki/Linear_algebra>`__
-------------------------------------------------------------------------

Guides
^^^^^^

- `An Intuitive Guide To Linear Algebra
  <https://betterexplained.com/articles/linear-algebra-guide/>`__
  at Better Explained

- `Immersive Linear Algebra
  <http://immersivemath.com/ila/>`__

Books
^^^^^

- `The Manga Guide to Linear Algebra
  <https://nostarch.com/linearalgebra>`__
  (2012) by Shin Takahashi & Iroha Inoue

- `No Bullshit Guide to Linear Algebra
  <https://www.goodreads.com/book/show/20939012>`__
  (2017, V2) by Ivan Savov
  (`website <https://minireference.com/blog/no-bs-linear-algebra-book/>`__,
  `review <https://machinelearningmastery.com/no-bullshit-guide-to-linear-algebra-review/>`__)

- `Coding the Matrix: Linear Algebra through Computer Science Applications
  <https://www.goodreads.com/book/show/25321818>`__
  (2015) by Philip Klein
  (`@Amazon <https://www.amazon.com/dp/B00VSN9NHY>`__)

Forums
^^^^^^

- `Linear algebra questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/linear-algebra>`__

----

`Abstract Algebra <https://en.wikipedia.org/wiki/Abstract_algebra>`__
-------------------------------------------------------------------------

- `Abstract algebra questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/abstract-algebra>`__

----

See Also
========

- Arto's Notes re: `mathematics <math>`__
