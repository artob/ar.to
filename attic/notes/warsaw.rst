******************************************************************
Arto's Notes re: `Warsaw <https://en.wikipedia.org/wiki/Warsaw>`__
******************************************************************

Services
========

Gyms
----

* `AKS Herkules
  <https://www.facebook.com/AKS-Herkules-Warszawa-128034037278492/>`__
* `Legia Sztanga
  <http://legiasztanga.pl/>`__
* `McFIT Warszawa Śródmieście
  <https://www.mcfit.com/pl/silownie/silownie/studiodetails/studio/warszawa-srodmiescie/>`__
  (24h)
* `McFIT Warszawa Ochota
  <https://www.mcfit.com/pl/silownie/silownie/studiodetails/studio/warszawa-ochota/>`__
  (24h)
* `FitnessClub S4 Warszawa Mokotów, ul. Racjonalizacji 5
  <http://fitnessclubs4.pl/mokotow-racjonalizacji/>`__
* `Gold's Gym Warsaw
  <#>`__
