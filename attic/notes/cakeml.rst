***************************************************************************************
Arto's Notes re: `CakeML <https://en.wikipedia.org/wiki/Standard_ML#Implementations>`__
***************************************************************************************

CakeML is a verified implementation of a significant subset of `Standard ML <sml>`__.

* https://cakeml.org/
* https://github.com/CakeML/cakeml

See Also
========

* `ATS <ats>`__
* `F* <fstar>`__
* `Idris <idris>`__
