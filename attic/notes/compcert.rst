**********************************************************************
Arto's Notes re: `CompCert <https://en.wikipedia.org/wiki/CompCert>`__
**********************************************************************

CompCert is a formally-verified `C99 <c>`__ compiler.

* http://compcert.inria.fr/
* https://github.com/AbsInt/CompCert
