**************************************************
Arto's Notes re: `fast.ai <http://www.fast.ai/>`__
**************************************************

* http://www.fast.ai/
* https://twitter.com/fastdotai
* https://github.com/fastai
* http://wiki.fast.ai/
* http://forums.fast.ai/

Courses
=======

Deep Learning Part 1: Practical Deep Learning for Coders
--------------------------------------------------------

http://course.fast.ai/

"There are around 20 hours of lessons, and you should plan to spend around
10 hours a week for 7 weeks to complete the material."

Deep Learning Part 2: Cutting Edge Deep Learning for Coders
-----------------------------------------------------------

http://course.fast.ai/part2.html

See Also
========

`artificial intelligence (AI) <ai>`__,
`machine learning (ML) <ml>`__
