************************************************************
Arto's Notes re: `PHP <https://en.wikipedia.org/wiki/PHP>`__
************************************************************

Installation
============

macOS
-----

HHVM
^^^^

https://docs.hhvm.com/hhvm/installation/mac

::

   $ brew tap hhvm/hhvm && brew install hhvm  # requires macOS Sierra
