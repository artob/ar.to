**********************************************************************
Arto's Notes re: `geometry <https://en.wikipedia.org/wiki/Geometry>`__
**********************************************************************

The `mathematical <math>`__ study of shape and the properties of space.

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

TODO

----

See Also
========

- Arto's Notes re: `mathematics <math>`__

- `Geometry questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/geometry>`__
