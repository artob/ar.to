*********************************************************************************
Arto's Notes re: `Adafruit <https://en.wikipedia.org/wiki/Adafruit_Industries>`__
*********************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Products
========

`Feather <https://www.adafruit.com/feather>`__
----------------------------------------------

- `Adafruit Feather nRF52 Pro
  <https://www.adafruit.com/product/3574>`__
  (`guide <https://learn.adafruit.com/adafruit-nrf52-pro-feather>`__)

- `Adafruit Feather nRF52 Bluefruit LE
  <https://www.adafruit.com/product/3406>`__
  (`guide <https://learn.adafruit.com/bluefruit-nrf52-feather-learning-guide>`__)

----

Distributors
============

Distributors in North America
-----------------------------

- `Allied
  <https://www.alliedelec.com/adafruit-industries/?mpp=60&sort=metric_inventorydesc>`__
  (US)
  DHL, FedEx, UPS, USPS.

- `Arrow
  <https://www.arrow.com/en/manufacturers/adafruit-industries>`__
  (US)
  DHL, FedEx, UPS, USPS.

- `Digi-Key
  <https://www.digikey.com/en/supplier-centers/a/adafruit>`__
  (US)

- `Element14
  <https://www.newark.com/b/adafruit>`__
  (US)

- `Jameco
  <https://www.jameco.com/shop/StoreCatalogDrillDownView?langId=-1&storeId=10001&catalogId=10001&refineType=String&sub_attr_name=Manufacturer&refineValue=Adafruit%20Industries&from=mflisting>`__
  (US)

- `Mouser
  <https://www.mouser.com/adafruit/>`__
  (US)
  DHL, FedEx, UPS, USPS.

- `Pololu
  <https://www.pololu.com/brands/adafruit>`__
  (US)

Distributors in Europe
----------------------

- `Pimoroni
  <https://shop.pimoroni.com>`__
  (UK)

- `The Pi Hut
  <https://thepihut.com>`__
  (UK)

- `EXP-Tech
  <https://www.exp-tech.de/en/search?sSearch=adafruit&p=1&n=48>`__
  (DE)

- `Generation Robots
  <https://www.generationrobots.com/en/95_adafruit-industries>`__
  (FR)

- `Mouser
  <https://eu.mouser.com/adafruit/>`__

----

See Also
========

- Arto's Notes re: `electronics <electronics>`__
