****************************************************************************
Arto's Notes re: `mathematics <https://en.wikipedia.org/wiki/Mathematics>`__
****************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Branches
========

- Arithmetic

  - Number theory

- `Algebra <algebra>`__

- `Calculus <calculus>`__ (study of continuous change)

  - `Analysis <analysis>`__ (study of change & quantity)

- `Geometry & Topology <geometry>`__ (study of space)

  - `Trigonometry <trig>`__

- `Logic <logic>`__

  - `Type theory <types>`__

- `Probability <prob>`__ & `Statistics <stats>`__

----

Divisions
=========

- `Pure mathematics
  <https://en.wikipedia.org/wiki/Pure_mathematics>`__

- `Applied mathematics
  <https://en.wikipedia.org/wiki/Applied_mathematics>`__

- `Discrete mathematics
  <https://en.wikipedia.org/wiki/Discrete_mathematics>`__

- `Computational mathematics
  <https://en.wikipedia.org/wiki/Computational_mathematics>`__

----

`Notation <https://en.wikipedia.org/wiki/Mathematical_notation>`__
==================================================================

Symbols
-------

- `List of mathematical abbreviations
  <https://en.wikipedia.org/wiki/List_of_mathematical_abbreviations>`__

- `List of mathematical symbols
  <https://en.wikipedia.org/wiki/List_of_mathematical_symbols>`__

- `List of mathematical symbols by subject
  <https://en.wikipedia.org/wiki/List_of_mathematical_symbols_by_subject>`__

- `List of logic symbols
  <https://en.wikipedia.org/wiki/List_of_logic_symbols>`__

Letters
-------

- `Greek letters used in mathematics, science, and engineering
  <https://en.wikipedia.org/wiki/Greek_letters_used_in_mathematics,_science,_and_engineering>`__

- `Latin letters used in mathematics
  <https://en.wikipedia.org/wiki/Latin_letters_used_in_mathematics>`__

----

Books
=====

- `Good Math: A Geek's Guide to the Beauty of Numbers, Logic, and Computation
  <https://www.goodreads.com/book/show/20757972>`__
  (2013) by Mark C. Chu-Carroll

- `Concepts of Modern Mathematics
  <https://www.goodreads.com/book/show/17315363>`__
  (2012) by Ian Stewart
  (`@Amazon <https://www.amazon.com/dp/B00CWR4MIK>`__)

- `Mathematics: Its Content, Methods and Meaning
  <https://www.goodreads.com/book/show/18994501>`__
  (2012) by A.D. Aleksandrov, A.N. Kolmogorov, M.A. Lavrent'ev
  (`@Amazon <https://www.amazon.com/dp/B00GUP46MC>`__)

Remedial/Review Books
---------------------

- `No Bullshit Guide to Mathematics
  <http://www.lulu.com/shop/ivan-savov/no-bullshit-guide-to-mathematics/paperback/product-23697411.html>`__
  (2018, V5) by Ivan Savov
  (`@HN <https://news.ycombinator.com/item?id=16562353>`__)

- `No Bullshit Guide to Math and Physics
  <https://www.goodreads.com/book/show/22876442>`__
  (2013, V4) by Ivan Savov
  (`website <https://minireference.com/blog/no-bs-math-and-physics-book/>`__,
  `@HN <https://news.ycombinator.com/item?id=4994367>`__)

- `Mathematics for the Nonmathematician
  <https://www.goodreads.com/book/show/18994249>`__
  (2013) by Morris Kline
  (`@Amazon <https://www.amazon.com/dp/B00BX1DN9K>`__)

- `Mathematics Rebooted: A Fresh Approach to Understanding
  <https://www.goodreads.com/book/show/36386740>`__
  (2017) by Lara Alcock
  (`@Amazon <https://www.amazon.com/dp/B07661R8N4>`__)

Miscellaneous Books
-------------------

- `Mathematical Notation: A Guide for Engineers and Scientists
  <https://www.goodreads.com/book/show/14407567>`__
  (2011) by Edward R. Scheinerman

----

Study
=====

- `How to Study Mathematics <https://www.math.uh.edu/~dblecher/pf2.html>`__

- `Ask HN: How to self-learn math? <https://news.ycombinator.com/item?id=16562173>`__

- `Notes on Discrete Mathematics
  <http://www.cs.yale.edu/homes/aspnes/classes/202/notes.pdf>`__
  (`@HN <https://news.ycombinator.com/item?id=17391580>`__)

----

Topics
======

- `Outline of mathematics
  <https://en.wikipedia.org/wiki/Outline_of_mathematics>`__

- `Areas of mathematics
  <https://en.wikipedia.org/wiki/Areas_of_mathematics>`__

- `Foundations of mathematics
  <https://en.wikipedia.org/wiki/Foundations_of_mathematics>`__

- `History of mathematics
  <https://en.wikipedia.org/wiki/History_of_mathematics>`__

----

See Also
========

- Arto's Notes re: `MATLAB <matlab>`__

- `Mathematics Stack Exchange <https://math.stackexchange.com/>`__

- `MathOverflow <https://mathoverflow.net/>`__
