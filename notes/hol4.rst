*******************************************************************************
Arto's Notes re: `HOL4 <https://en.wikipedia.org/wiki/HOL_(proof_assistant)>`__
*******************************************************************************

**HOL4** is a proof assistant for `higher-order logic <hol>`__.

* https://hol-theorem-prover.org/
* https://github.com/HOL-Theorem-Prover/HOL

Related Software
================

* `HOL Light <https://en.wikipedia.org/wiki/HOL_Light>`__
* `HOL Zero <http://proof-technologies.com/holzero/>`__

See Also
========

* `Coq <coq>`__
* `Isabelle <isabelle>`__
