***********************
Arto's Notes re: to-dos
***********************

Current
=======

Learn
-----

- `Flutter <flutter>`__

- `Mynewt <mynewt>`__

- `BLE <bluetooth>`__

- Zig

Daily
=====

Learn
-----

- Review Coursera situation.

Weekly
======

FLOSS
-----

- Review and merge any pending GitHub/Bitbucket pull requests.

Monthly
=======

Future
======

- Learn `MATLAB <matlab>`__.
