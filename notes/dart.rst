*************************************************************************************
Arto's Notes re: `Dart <https://en.wikipedia.org/wiki/Dart_(programming_language)>`__
*************************************************************************************

* https://www.dartlang.org
* https://github.com/dart-lang/sdk

Installation
============

https://www.dartlang.org/install

macOS with Homebrew
-------------------

https://www.dartlang.org/install/mac

::

   $ brew tap dart-lang/dart

   $ brew install dart --devel

Linux
-----

https://www.dartlang.org/install/linux

Tutorials
=========

* https://www.dartlang.org/guides/get-started

Libraries
=========

* https://pub.dartlang.org/

News
====

Official
--------

* https://www.dartlang.org/dart-2

* https://news.dartlang.org

* https://medium.com/dartlang

* https://twitter.com/dart_lang

Community
---------

* https://www.reddit.com/r/dartlang/

Forums
======

* https://gitter.im/dart-lang/home

* https://stackoverflow.com/questions/tagged/dart

See Also
========

`Flutter <flutter>`__
