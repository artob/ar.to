****************************************************************
Arto's Notes re: `Wi-Fi <https://en.wikipedia.org/wiki/Wi-Fi>`__
****************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

`Protocols <https://en.wikipedia.org/wiki/IEEE_802.11>`__
=========================================================

- `IEEE 802.11ax <https://en.wikipedia.org/wiki/IEEE_802.11ax>`__
  (2019)
  2.4/5.0 GHz, 6 Gbit/s

- `IEEE 802.11ac <https://en.wikipedia.org/wiki/IEEE_802.11ac>`__
  Wave2 (2016)
  5.0 GHz, 2.34 Gbit/s

- `IEEE 802.11ac <https://en.wikipedia.org/wiki/IEEE_802.11ac>`__
  Wave1 (2013)
  5.0 GHz, 1.3 Gbit/s

- `IEEE 802.11n <https://en.wikipedia.org/wiki/IEEE_802.11n-2009>`__
  (2009)
  2.4/5.0 GHz, 600 Mbit/s

- `IEEE 802.11g <https://en.wikipedia.org/wiki/IEEE_802.11g-2003>`__
  (2003)
  2.4 GHz, 54 Mbit/s

- `IEEE 802.11b <https://en.wikipedia.org/wiki/IEEE_802.11b-1999>`__
  (1999)
  2.4 GHz,
  11 Mbit/s

- `IEEE 802.11 <https://en.wikipedia.org/wiki/IEEE_802.11_(legacy_mode)>`__
  (1997)
  2.4 GHz, 2 Mbit/s

----

`Security <https://en.wikipedia.org/wiki/IEEE_802.11#Security>`__
=================================================================

- `WPA3 <https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access#WPA3>`__
  (2018)

- `WPA2 <https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access#WPA2>`__
  (2004)

- `WPA <https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access>`__
  (2003)

- `WEP <https://en.wikipedia.org/wiki/Wired_Equivalent_Privacy>`__
  (1997)

----

See Also
========

- Arto's Notes re: `technology <tech>`__

- `List of 802.11ax Hardware
  <https://wikidevi.com/wiki/List_of_802.11ax_Hardware>`__
