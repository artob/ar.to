**************************************************************************
Arto's Notes re: `JavaScript <https://en.wikipedia.org/wiki/JavaScript>`__
**************************************************************************

* https://kangax.github.io/compat-table/es2016plus/

Books
=====

* `Exploring ES6
  <http://exploringjs.com/es6/>`__

* `Exploring ES2016 and ES2017
  <http://exploringjs.com/es2016-es2017/>`__

* `Exploring ES2018 and ES2019
  <http://exploringjs.com/es2018-es2019/>`__

Frameworks and Libraries
========================

* `jQuery
  <https://en.wikipedia.org/wiki/JQuery>`__
* `AngularJS
  <https://en.wikipedia.org/wiki/AngularJS>`__
* `Ember.js
  <https://en.wikipedia.org/wiki/Ember.js>`__
* `React.js
  <https://en.wikipedia.org/wiki/React_(JavaScript_library)>`__

Development Environment
=======================

::

   $ sudo pip install jsbeautifier

Development Environment on macOS
--------------------------------

::

   $ sudo port install phantomjs
   $ sudo port install nodejs
   $ sudo port install npm
   $ sudo npm install -g grunt-cli
   $ sudo npm install -g qunit
   $ sudo npm install -g jasmine-node@2.0.0-beta4
