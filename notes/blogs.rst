*****************************
Arto's Notes re: blogs I read
*****************************

Technology
==========

- `Benedict Evans <https://www.ben-evans.com/>`__
  (`@BenedictEvans <https://twitter.com/BenedictEvans>`__)


Programming
===========

- `James Hague <http://prog21.dadgum.com/>`__
  (`@dadgumjames <https://twitter.com/dadgumjames>`__)

- `John D. Cook <https://www.johndcook.com/blog/most-popular/>`__
  (`@JohnDCook <https://twitter.com/JohnDCook>`__)

Computer Science
================

- `Adrian Colyer <https://blog.acolyer.org/>`__
  (`@adriancolyer <https://twitter.com/adriancolyer>`__)

Miscellaneous
=============

- `Global Guerrillas <http://globalguerrillas.typepad.com/>`__
  (`@johnrobb <https://twitter.com/johnrobb>`__)

Philosophy
==========

- `Edward Feser <http://edwardfeser.blogspot.com/>`__
