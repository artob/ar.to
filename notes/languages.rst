**************************
Arto's Notes re: languages
**************************

Top Three
=========

* `English <english>`__
* `Mandarin <chinese>`__
* `Spanish <spanish>`__

Of Major Interest
=================

* `Chinese <chinese>`__       (#)
* `English <english>`__       (.)
* `Finnish <finnish>`__       (.)
* `Russian <russian>`__       (!)
* `Spanish <spanish>`__       (!)
* `Swedish <swedish>`__       (.)
* `Ukrainian <ukrainian>`__   (!)

Of Minor Interest
=================

* `Arabic <arabic>`__
* `German <german>`__
* `Greek <greek>`__
* `Japanese <japanese>`__
* `Latin <latin>`__
* `Portuguese <portuguese>`__

Courses
=======

* `Babbel
  <https://home.babbel.com/en/registration/new?invitation_code=565535220990>`__
* `busuu
  <https://www.busuu.com/>`__
* `Lingvist
  <https://lingvist.com/>`__
* `Rocket Languages
  <https://www.rocketlanguages.com/>`__

Tutors
======

* `italki
  <https://www.italki.com/i/CbaffA>`__
* `Preply
  <https://preply.com/#_prefMjQzMzc2>`__

References
==========

* `List of languages by number of native speakers
  <https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers>`__
  in Wikipedia
* `The 10 most spoken languages in the world
  <https://www.babbel.com/en/magazine/the-10-most-spoken-languages-in-the-world>`__
  by Babbel

----

See Also
========

- Arto's Notes re: `localization <l10n>`__
