****************************************************************************
Arto's Notes re: `type theory <https://en.wikipedia.org/wiki/Type_theory>`__
****************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

`Systems <https://en.wikipedia.org/wiki/Type_system>`__
=======================================================

`Dependent Types <https://en.wikipedia.org/wiki/Dependent_type>`__
------------------------------------------------------------------

   "Dependent types are useful not only because they help you express
   correctness properties in types. Dependent types also often let you write
   certified programs without writing anything that looks like a proof. Even
   with subset types, which for many contexts can be used to express any
   relevant property with enough acrobatics, the human driving the proof
   assistant usually has to build some proofs explicitly. Writing formal
   proofs is hard, so we want to avoid it as far as possible. Dependent
   types are invaluable for this purpose."

   -- Adam Chlipala, `Certified Programming with Dependent Types
      <http://adam.chlipala.net/cpdt/html/Intro.html>`__

----

Books
=====

- `Types and Programming Languages
  <https://www.cis.upenn.edu/~bcpierce/tapl/>`__
  aka TaPL
  (2002) by Benjamin C. Pierce
  (`Goodreads
  <https://www.goodreads.com/book/show/20363346-types-and-programming-languages>`__)

- `Advanced Topics in Types and Programming Languages
  <https://www.cis.upenn.edu/~bcpierce/attapl/>`__
  aka ATTaPL
  (2004) by Benjamin C. Pierce et al.
  (`Goodreads
  <https://www.goodreads.com/book/show/788751.Advanced_Topics_in_Types_and_Programming_Languages>`__)

- `Software Foundations
  <https://softwarefoundations.cis.upenn.edu/current/index.html>`__
  (2011+) by Benjamin C. Pierce et al.
  (`Goodreads
  <https://www.goodreads.com/book/show/13413455-software-foundations>`__)

- `Practical Foundations for Programming Languages
  <https://www.cs.cmu.edu/~rwh/pfpl.html>`__
  aka PFPL
  (2nd Ed; 2016) by Robert Harper
  (`Goodreads
  <https://www.goodreads.com/book/show/29907895-practical-foundations-for-programming-languages>`__)

- `Programming Languages: Application and Interpretation
  <http://cs.brown.edu/~sk/Publications/Books/ProgLangs/>`__
  aka PLAI
  (2nd Ed) by Shriram Krishnamurthi
  (`Goodreads
  <https://www.goodreads.com/book/show/10580126-programming-languages>`__,
  `Wikipedia
  <https://en.wikipedia.org/wiki/Programming_Languages:_Application_and_Interpretation>`__)

Readings
========

- Basic Simple Type Theory
  (1997) by Hindley

----

Reference
=========

- `lambda (λ) cube
  <https://en.wikipedia.org/wiki/Lambda_cube>`__

----

See Also
========

- `logic <logic>`__: `first-order logic (FOL) <fol>`__, `higher-order logic (HOL) <hol>`__
