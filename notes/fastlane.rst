*******************************************************
Arto's Notes re: `fastlane <https://fastlane.tools/>`__
*******************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- https://fastlane.tools

- https://docs.fastlane.tools

- https://flutter.io/fastlane-cd/

----

`Installation <https://docs.fastlane.tools/getting-started/android/setup/>`__
=============================================================================

Installation on macOS
---------------------

::

   $ brew cask install fastlane

----

`Actions <https://docs.fastlane.tools/actions/>`__
==================================================

- `supply <https://docs.fastlane.tools/actions/supply/>`__
  aka `upload_to_play_store`

----

`Plugins <https://docs.fastlane.tools/plugins/available-plugins/>`__
====================================================================

TODO

----

See Also
========

- Arto's Notes re: `Android <android>`__, `iOS <ios>`__,
  `Flutter <flutter>`__
