******************************************************************************
Arto's Notes re: `Windows <https://en.wikipedia.org/wiki/Microsoft_Windows>`__
******************************************************************************

Downloads
=========

Developer
---------

* `Windows 10 Enterprise
  <https://developer.microsoft.com/en-us/windows/downloads/virtual-machines>`__
  for VMWare, Hyper-V, VirtualBox, and Parallels

Windows 10
----------

* `Windows 10 ISOs
  <https://www.microsoft.com/en-us/software-download/windows10ISO>`__
  (`elsewhere <http://windowsiso.net/windows-10-iso/>`__)

Windows 8
---------

* `Windows 8 ISOs
  <https://www.microsoft.com/en-us/software-download/windows8ISO>`__
  (`elsewhere <http://windowsiso.net/windows-8-1-iso/windows-8-1-download/>`__)

Windows 7
---------

* `Windows 7 ISOs
  <https://www.microsoft.com/en-us/software-download/windows7>`__
  (`elsewhere <http://windowsiso.net/windows-7-iso/windows-7-download/>`__)

Purchases
=========

Windows 10
----------

* `Windows 10 Home
  <https://www.amazon.com/Microsoft-Windows-10-Home-Download/dp/B01019BM7O>`__
  ($109.99)

* `Windows 10 Home OEM
  <https://www.newegg.com/Product/NewProduct.aspx?Item=N82E16832397686>`__

* `Windows 10 Pro
  <https://www.amazon.com/Microsoft-Windows-10-Pro-Download/dp/B01019BOEA>`__
  ($189.00)

* `Windows 10 Pro OEM
  <https://www.newegg.com/Product/NewProduct.aspx?item=N82E16832397687>`__

License Keys
------------

* https://www.kinguin.net/software-deals/

Reference
=========

* `Activate Windows 7 or Windows 8.1
  <https://support.microsoft.com/en-us/help/15083/windows-how-to-activate>`__

* `Get help with Windows activation errors
  <https://support.microsoft.com/en-us/help/10738/windows-10-get-help-with-activation-errors>`__

* `Is it OK to use OEM Windows on your own PC? Don't ask Microsoft
  <http://www.zdnet.com/article/is-it-ok-to-use-oem-windows-on-your-own-pc-dont-ask-microsoft/>`__

Miscellaneous
=============

* `KMS Client Setup Keys
  <https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/jj612867(v=ws.11)>`__
