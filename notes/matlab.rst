******************************************************************
Arto's Notes re: `MATLAB <https://en.wikipedia.org/wiki/MATLAB>`__
******************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Links
=====

- `MATLAB <https://www.mathworks.com/products/matlab/>`__

- `MATLAB Academy <https://matlabacademy.mathworks.com/>`__

- `MATLAB Online <https://matlab.mathworks.com/>`__

----

Usage
=====

Symbolic Math
-------------

::

   >> syms x y z
   >> factor(x^2 - 4*x + 3)
   ans =
   [ x - 1, x - 3]

----

See Also
========

- Arto's Notes re: `mathematics <math>`__

- `MATLAB questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/tags/matlab>`__
