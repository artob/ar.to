**********************************************************************************************
Arto's Notes re: `first-order logic (FOL) <https://en.wikipedia.org/wiki/First-order_logic>`__
**********************************************************************************************

See Also
========

* `higher-order logic (HOL) <hol>`__
* `Zermelo–Fraenkel set theory (ZFC) <zfc>`__
