******************************************************************************
Arto's Notes re: `Alpine Linux <https://en.wikipedia.org/wiki/Alpine_Linux>`__
******************************************************************************

Administration
==============

::

   $ cat /etc/alpine-release

Upgrading
---------

::

   $ apk update && apk upgrade

`Tutorials <https://wiki.alpinelinux.org/wiki/Tutorials_and_Howtos>`__
======================================================================

Reference
=========

* `Overview of Alpine Linux
  <https://wiki.alpinelinux.org/wiki/Alpine_Linux:Overview>`__
* `Comparison with other distros
  <https://wiki.alpinelinux.org/wiki/Comparison_with_other_distros>`__
* `Alpine Linux package management
  <https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management>`__
* `Creating an Alpine package
  <https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package>`__

Unsorted
========

* https://github.com/scaleway/image-alpine
