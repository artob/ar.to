******************************************************************************************
Arto's Notes re: `probability theory <https://en.wikipedia.org/wiki/Probability_theory>`__
******************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Guides
======

- `An Intuitive (and Short) Explanation of Bayes' Theorem
  <https://betterexplained.com/articles/an-intuitive-and-short-explanation-of-bayes-theorem/>`__
  at Better Explained

----

Notation
========

- `Notation in probability and statistics
  <https://en.wikipedia.org/wiki/Notation_in_probability_and_statistics>`__

----

See Also
========

- Arto's Notes re: `mathematics <math>`__ (`statistics <stats>`__)

- `Probability questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/probability>`__

- `Probability questions at MathOverflow
  <https://mathoverflow.net/questions/tagged/pr.probability>`__

- `Cross Validated <https://stats.stackexchange.com/>`__
