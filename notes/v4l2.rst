*********************************************************************
Arto's Notes re: `V4L2 <https://en.wikipedia.org/wiki/Video4Linux>`__
*********************************************************************

Listing Devices
===============

::

   $ lsmod | fgrep uvcvideo

   $ ls -l /dev/video*
   crw-rw---- 1 root video 81, 0 Jan  1 00:00 /dev/video0

   $ lsusb
   $ lsusb -d 041e:4097 -t
   $ sudo lsusb -d 041e:4097 -v

   $ sudo v4l2-ctl --list-devices
   $ sudo v4l2-ctl --info

   $ uvcdynctrl --list

Introspecting Devices
=====================

::

   $ sudo aptitude install v4l-utils
   $ dpkg -L v4l-utils

   $ sudo v4l2-ctl --list-formats
   ioctl: VIDIOC_ENUM_FMT
           Index       : 0
           Type        : Video Capture
           Pixel Format: 'YUYV'
           Name        : YUV 4:2:2 (YUYV)

           Index       : 1
           Type        : Video Capture
           Pixel Format: 'MJPG' (compressed)
           Name        : MJPEG

Recording Video
===============

::

   # http://packages.ubuntu.com/trusty/streamer
   $ sudo aptitude install streamer

   # http://packages.ubuntu.com/trusty/libav-tools
   $ sudo aptitude install ffmpeg libav-tools

   # http://packages.ubuntu.com/trusty/mencoder
   $ sudo aptitude install mencoder

Recording with Streamer
-----------------------

::

   $ streamer -c /dev/video0 -f jpeg -t 0:10 -o video.avi

Capturing Frames
================

Capturing with Streamer
-----------------------

::

   $ streamer -o frame.jpeg
   $ streamer -c /dev/video0 -f jpeg -o frame.jpeg

Specification
=============

* http://linuxtv.org/downloads/v4l-dvb-apis/v4l2spec.html

Loopback Device
===============

* https://github.com/umlaeute/v4l2loopback
* https://github.com/umlaeute/v4l2loopback/wiki
* https://github.com/umlaeute/v4l2loopback/wiki/Gstreamer
* https://github.com/umlaeute/v4l2loopback/wiki/Ffmpeg

::

   $ sudo aptitude install v4l2loopback-utils

   $ man 1 v4l2loopback-ctl

   $ sudo modprobe v4l2loopback
   $ sudo modprobe -r v4l2loopback
   $ ls -l /dev/video0
   $ ls -l /sys/devices/virtual/video4linux/video0/

   $ sudo ffmpeg -re -i input.avi -f v4l2 /dev/video0
   $ sudo gst-launch-1.0 videotestsrc ! v4l2sink device=/dev/video0
   $ sudo gst-launch-0.10 -v videotestsrc pattern=snow ! "video/x-raw-yuv,width=640,height=480,framerate=15/1,format=(fourcc)YUY2" ! v4l2sink device=/dev/video0

   $ sudo aptitude install libv4l-dev # for <libv4l2.h>
   $ sudo pip2 install v4l2capture

See Also
========

* https://linuxtv.org/wiki/index.php/Main_Page
* https://linuxtv.org/docs.php
* http://www.ideasonboard.org/uvc/
* https://help.ubuntu.com/community/Webcam
