********************************************************************************
Arto's Notes re: `Unreal Engine <https://en.wikipedia.org/wiki/Unreal_Engine>`__
********************************************************************************

https://www.unrealengine.com/

Reference
=========

* https://docs.unrealengine.com/latest/INT/

Tutorials
=========

* `Get Started with UE4
  <https://docs.unrealengine.com/latest/INT/GettingStarted/>`__

* `Introduction to C++ Programming in UE4
  <https://docs.unrealengine.com/latest/INT/Programming/Introduction/>`__

* `The Shooter Tutorial
  <http://shootertutorial.com/>`__

Books
=====

* `Unreal Engine 4.x By Example
  <https://www.safaribooksonline.com/library/view/unreal-engine-4x/9781785885532/>`__
  (2016)
  by Benjamin Carnall

Courses
=======

* `The Unreal Engine Developer Course
  <https://www.udemy.com/unrealcourse/>`__
  ($10) at Udemy
