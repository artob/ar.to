***********************************************************************************
Arto's Notes re: `analysis <https://en.wikipedia.org/wiki/Mathematical_analysis>`__
***********************************************************************************

The `mathematical <math>`__ study of limits and related theories.

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

TODO

----

See Also
========

- Arto's Notes re: `mathematics <math>`__ (`calculus <calculus>`__)

- `Analysis questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/analysis>`__
