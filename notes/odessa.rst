******************************************************************
Arto's Notes re: `Odessa <https://en.wikipedia.org/wiki/Odessa>`__
******************************************************************

* `Goods <#goods>`__
* `Services <#services>`__
* `Languages <#languages>`__
* `See Also <#see-also>`__

----

Goods
=====

TODO

----

Services
========

Gyms
----

TODO

Taxi
----

* `Uklon <http://www.uklon.com.ua/>`__

Opticians
---------

TODO

----

Languages
=========

Russian
-------

Tutors
^^^^^^

* `Russian language tutors Odessa
  <https://preply.com/en/odessa/russian-tutors>`__

Courses
^^^^^^^

* `AMBergh Education
  <http://www.ambergh.com/learn-russian/odessa>`__

* `ECHO Eastern Europe
  <https://echoee.com/odessa/>`__

* `NovaMova
  <http://novamova.net/russian-schools/odessa>`__

Ukrainian
---------

Tutors
^^^^^^

* `Ukrainian language tutors Odessa
  <https://preply.com/en/kiev/ukrainian-tutors>`__

Courses
^^^^^^^

* `ECHO Eastern Europe
  <https://echoee.com/odessa/>`__

----

See Also
========

`Ukraine <ukraine>`__: `Kharkiv <kharkiv>`__, `Kiev <kiev>`__, and `Lviv <lviv>`__
