************************
Arto's Notes re: virtues
************************

`Stoic <stoicism>`__
====================

Cardinal
--------

The four cardinal Stoic virtues are:

* Courage (*andreia*)
* Temperance (*sophrosyne*)
* Justice (*dikaiosyne*)
* Wisdom (*sophia*)

Masculine
=========

Tactical
--------

The four universal tactical masculine virtues are:

* Strength
* Courage
* Mastery
* Honor

Explanation
-----------

https://www.youtube.com/watch?v=fYOORimbpUs

   "Strength, Courage, Mastery, and Honor are the alpha virtues of men all
   over the world. They are the fundamental virtues of men because without
   them, no 'higher' virtues can be entertained. You need to be alive to
   philosophize. You can add to these virtues and you can create rules and
   moral codes to govern them, but if you remove them from the equation
   altogether you aren't just leaving behind the virtues that are specific
   to men, you are abandoning the virtues that make civilization possible."

   -- Jack Donovan, The Way of Men

   "When men evaluate each other as men, they still look for the same
   virtues that they'd need to keep the perimeter. Men respond to and admire
   the qualities that would make men useful and dependable in an emergency. Men
   have always had a role apart, and they still judge one another according to
   the demands of that role as a guardian in a gang struggling for survival
   against encroaching doom. Everything that is specifically about being a
   man--not merely a person--has to do with that role."

   -- Jack Donovan, The Way of Men

   "Men cannot be men--much less good or heroic men--unless their actions
   have meaningful consequences to people they truly care about. Strength
   requires an opposing force, courage requires risk, mastery requires hard
   work, honor requires accountability to other men."

   -- Jack Donovan, The Way of Men
