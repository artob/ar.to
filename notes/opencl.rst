******************************************************************
Arto's Notes re: `OpenCL <https://en.wikipedia.org/wiki/OpenCL>`__
******************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

See Also
========

- `GPUs <gpu>`__, `CUDA <cuda>`__
