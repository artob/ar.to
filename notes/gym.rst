************************
Arto's Notes re: the gym
************************

Checklist
=========

* on person
  * gym card & cash
  * tablet with the `5x5 app <apps>`__
  * glasses
  * watch
  * health insurance certificate
* in backpack
  * `BCAA pills <supplements>`__
  * water bottle
  * gym shoes
  * gym shorts
  * small towel
  * change of clothes
  * USB power pack

See Also
========

* `strength training <strength>`__
