**************************
Arto's Notes re: logistics
**************************

* `Parcl <https://www.parcl.com/>`__
* `Viabox <https://www.viabox.com/>`__

Ukraine
=======

* https://novaposhta.ua/en/international_delivery/about
* http://ukraine-express.com/en/
* https://meest-express.com.ua/ua/ & http://www.meest.us/
* https://modnakasta.ua/
