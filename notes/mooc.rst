************************************************************************************
Arto's Notes re: `MOOCs <http://en.wikipedia.org/wiki/Massive_open_online_course>`__
************************************************************************************

Providers
=========

* `Coursera <https://www.coursera.org/>`__
* `edX <https://www.edx.org/>`__
* `Udacity <https://www.udacity.com/>`__
* `Udemy <https://www.udemy.com/>`__

Courses
=======

CSE167x: Computer Graphics (UC San Diego)
-----------------------------------------

https://www.edx.org/course/computer-graphics-uc-san-diegox-cse167x

Cryptography I (Stanford University)
------------------------------------

https://www.coursera.org/course/crypto

Compilers (Stanford University)
-------------------------------

https://www.coursera.org/course/compilers

Computer Networks (University of Washington)
--------------------------------------------

https://www.coursera.org/course/comnetworks

Heterogeneous Parallel Programming (University of Illinois at Urbana-Champaign)
-------------------------------------------------------------------------------

https://www.coursera.org/course/hetero

Greek and Roman Mythology (University of Pennsylvania)
------------------------------------------------------

https://www.coursera.org/course/mythology

* The Odyssey
* Hymn to Demeter
* Hymn to Apollo
* The Complete Aschylus

Human Evolution: Past and Future (University of Wisconsin–Madison)
------------------------------------------------------------------

https://www.coursera.org/course/humanevolution
