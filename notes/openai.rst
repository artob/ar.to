******************************************************************
Arto's Notes re: `OpenAI <https://en.wikipedia.org/wiki/OpenAI>`__
******************************************************************

* https://openai.com/
* https://twitter.com/OpenAI
* https://github.com/openai

See Also
========

`artificial intelligence (AI) <ai>`__,
`machine learning (ML) <ml>`__
