********************************************************************************
Arto's Notes re: `Unity3D <https://en.wikipedia.org/wiki/Unity_(game_engine)>`__
********************************************************************************

https://unity3d.com/

Downloads
=========

* https://unity3d.com/unity/beta

Reference
=========

* https://docs.unity3d.com/2017.1/Documentation/ScriptReference/
* https://docs.unity3d.com/2017.1/Documentation/Manual/

Tutorials
=========

* https://unity3d.com/learn/tutorials
* https://unity3d.com/learn/tutorials/topics/interface-essentials
* https://unity3d.com/learn/tutorials/topics/physics
* https://unity3d.com/learn/tutorials/topics/virtual-reality
