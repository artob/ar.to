************************
Arto's Notes re: Arduino
************************

Boards
======

* `Arduino/Genuino Zero <https://www.arduino.cc/en/Main/ArduinoBoardZero>`__
* `Arduino/Genuino Uno <https://www.arduino.cc/en/Main/ArduinoBoardUno>`__

Best Practices
==============

* Store constant data in program memory (PROGMEM). Wrap strings with ``F()``.

Tutorials
=========

* https://www.arduino.cc/en/Guide/Environment
* https://www.arduino.cc/en/Tutorial/HomePage
  * https://www.arduino.cc/en/Tutorial/BuiltInExamples

* https://www.arduino.cc/en/hacking/libraries
* https://www.arduino.cc/en/Hacking/LibraryTutorial
* http://playground.arduino.cc/Code/Library

* https://www.arduino.cc/en/Tutorial/Memory
* http://playground.arduino.cc/Learning/Memory

Reference
=========

* https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5:-Library-specification
* https://www.arduino.cc/en/Reference/HomePage
* https://www.arduino.cc/en/Reference/StyleGuide
* https://www.arduino.cc/en/Reference/APIStyleGuide
* https://www.arduino.cc/en/Reference/Constants

PROGMEM
-------

* https://www.arduino.cc/en/Reference/PROGMEM
* http://www.gammon.com.au/progmem
* http://www.nongnu.org/avr-libc/user-manual/pgmspace.html
  * `avr/pgmspace.h <http://www.nongnu.org/avr-libc/user-manual/group__avr__pgmspace.html>`__
* https://learn.adafruit.com/memories-of-an-arduino/optimizing-program-memory

SRAM
----

* https://learn.adafruit.com/memories-of-an-arduino/optimizing-sram

EEPROM
------

* https://www.arduino.cc/en/Reference/EEPROM
* https://learn.adafruit.com/memories-of-an-arduino/eeprom

Software
========

* https://www.arduino.cc/en/Reference/Libraries
* http://playground.arduino.cc/Main/LibraryList
* https://github.com/firmata/arduino
* http://www.airspayce.com/mikem/arduino/RadioHead/

Cryptography
------------

* https://github.com/rweather/arduinolibs
  * https://rweather.github.io/arduinolibs/crypto.html
* https://github.com/Cathedrow/Cryptosuite

Support
=======

* http://arduino.stackexchange.com/
