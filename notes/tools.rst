***************************************************************
Arto's Notes re: `tools <https://en.wikipedia.org/wiki/Tool>`__
***************************************************************

* `Purposes <#purposes>`__
* `Taxonomy <#taxonomy>`__
* `Books <#books>`__
* `See Also <#see-also>`__

----

Purposes
========

* `cutting <https://en.wikipedia.org/wiki/Saw>`__
* `drilling <https://en.wikipedia.org/wiki/Drill>`__
* `driving <#>`__ (fasteners)
* `grinding <#>`__
* `heating <#>`__
* `painting <#>`__
* `polishing <#>`__
* `routing <https://en.wikipedia.org/wiki/Wood_router>`__
* `sanding <https://en.wikipedia.org/wiki/Sander>`__
* `shaping <#>`__

Taxonomy
========

`Hand Tools <https://en.wikipedia.org/wiki/Hand_tool>`__
--------------------------------------------------------

`Power Tools <https://en.wikipedia.org/wiki/Power_tool>`__
----------------------------------------------------------

Books
=====

See Also
========

* `computer numerical control (CNC) <cnc>`__
* `mechanical advantage
  <https://en.wikipedia.org/wiki/Mechanical_advantage>`__
