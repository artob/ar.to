*************************
Arto's Notes re: projects
*************************

I work on `augmented-reality wargames <conreality>`__.

In my copious free time, I study `military theory & history
<warfare>`__ and prognosticate on the impact of emerging
technologies of mass superempowerment.

See my `GitHub <https://github.com/bendiken>`__ and
`Open Hub <https://www.openhub.net/accounts/bendiken>`__ profiles for
the full list of open-source projects that I currently participate in.

Current Projects
================

My current major projects are the following:

* `Crypto Economics Consulting Group (CECG) <cecg>`__
* `Conreality <conreality>`__
* `DRY <dry>`__: `DRYlang <drylang>`__, `DRYlib <drylib>`__
* `Unlicense <unlicense>`__

Previous Projects
=================

My previous major open-source projects include:

* `Drush <https://github.com/drush-ops>`__
  and other `Drupal <drupal>`__ `modules <https://www.drupal.org/user/26089>`__
  (2005-2009)
* `OpenSEF <https://www.drupal.org/forum/general/general-discussion/2005-08-27/why-xaneon-switched-from-mambo-to-drupal>`__ (2004-2005)
* `RDF.rb <https://github.com/ruby-rdf>`__ (2009-2016)

See Also
========

`programming <programming>`__
