***********************************************************************************
Arto's Notes re: `ATS <https://en.wikipedia.org/wiki/ATS_(programming_language)>`__
***********************************************************************************

* http://www.ats-lang.org/
* https://github.com/ats-lang/ATS-Postiats-release

See Also
========

* `CakeML <cakeml>`__
* `F* <fstar>`__
* `Idris <idris>`__
