*********************************************************
Arto's Notes re: `OpenOCD <https://elinux.org/OpenOCD>`__
*********************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- http://openocd.org

- https://github.com/ntfreak/openocd

----

Installation
============

https://github.com/ntfreak/openocd/blob/master/README#L134

----

`Installation on macOS <https://mynewt.apache.org/latest/get_started/native_install/cross_tools.html#installing-openocd-on-mac-os>`__
-------------------------------------------------------------------------------------------------------------------------------------

::

   $ brew install open-ocd

----

`Installation on Linux <https://mynewt.apache.org/latest/get_started/native_install/cross_tools.html#installing-openocd-on-linux>`__
------------------------------------------------------------------------------------------------------------------------------------

TODO

----

See Also
========

- Arto's Notes re: `RTOSes <rtos>`__, `Mynewt <mynewt>`__
