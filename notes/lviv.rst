**************************************************************
Arto's Notes re: `Lviv <https://en.wikipedia.org/wiki/Lviv>`__
**************************************************************

The largest city in western `Ukraine <ukraine>`__.

(Львів in `Ukrainian <ukrainian>`__, Lwów [lvuf] in Polish,
Lemberg in German, Leopolis in Latin.)

* `Travel <#travel>`__
* `Goods <#goods>`__
* `Services <#services>`__
* `Events <#events>`__
* `Activities <#activities>`__
* `Languages <#languages>`__
* `See Also <#see-also>`__

----

Travel
======

Visa
----

* US and EU citizens don't need to apply for a visa to enter Ukraine.
  **Just make sure to pack your passport.**

* Don't forget to **reserve enough time at the airport for passport security
  control** on entering and exiting Ukraine.
  If you are flying direct to Lviv, passport control will take place at the
  Lviv airport (when departing, it will be after the general security
  control). If you are transiting through Kiev, passport control will happen
  at the Kiev airport and the flight to/from Lviv is a domestic flight
  thereafter.
  In the worst case (in case of long queues), passport security control
  could amount to 30 minutes of lost time; more typically, you'll be done
  within 10 minutes, particularly at the Lviv airport.

Airport
-------

* **There is a single airport in Lviv**, the `Lviv Danylo Halytskyi
  International Airport <https://en.wikipedia.org/wiki/Lviv_Danylo_Halytskyi_International_Airport>`__.
  It is a pleasantly simple small airport.

* You can get a taxi from the airport, or order an Uber to pick you up from there.

* **A taxi from the airport to the center of town, or vice versa, normally
  should cost less than ₴100** (less than €4) and takes about 15-20 minutes.
  However, unless you explicitly agree the fare beforehand, you will
  probably be charged the tourist special (2-3× the ordinary fare, but it
  will still be no more than about €10).

* If you like to live dangerously, have no cargo luggage to drop off, and
  have done your online check-in for your departure, it is typically
  possible to arrive at the Lviv airport 10-15 minutes prior to your gate
  closing and still make your flight--especially for domestic flights in the
  early morning.

Comms
-----

* **There is free Wi-Fi connectivity at the Lviv and Kiev airports.**

* Check your data roaming plan and phone settings. This isn't the EU, and
  your domestic operator may charge you ridiculous amounts in data roaming
  fees for Ukraine. **Roaming rates may be as high as €1/MB.** You have been
  warned.

* Alternatively, your domestic operator may offer no roaming service at all
  in Ukraine; prepare accordingly.

* Prepaid SIM cards (with 5 GB included data) can be bought and activated at
  Vodafone shops (for example, on Shevchenka Ave) for less than €5.
  No passport or personal details are needed for these SIM cards.

* Supermarkets also sell prepaid SIMs, but you would probably need to be
  able to read Ukrainian or Russian to activate them. Activation usually
  just means calling a service number once.

Language
--------

* **The native language is Ukrainian.**
  Most people also understand or speak Russian and Polish.

* **English is not commonplace overall**, but young people and most service
  staff around the center of town usually understand or speak English.

Money
-----

* The local currency is called `hryvnia <https://en.wikipedia.org/wiki/Ukrainian_hryvnia>`__
  (sign ₴, code UAH). Exchange rate rules-of-thumb as of September 2017 are
  **30:1 UAH/EUR and 25:1 UAH/USD.**

* UAH currency is generally not accepted by money exchangers outside Ukraine.
  Prepare to use ATMs or exchangers to get cash upon arrival (or at the Kiev
  airport), and to exchange any surplus UAH cash back to your own currency
  prior to departing Ukraine.

* The airport has several ATMs, and it's not difficult to locate ATMs around
  town.

* **Getting cash from random ATMs can be problematic**. Withdrawal limits of
  ₴500 to ₴2,000 are typical. Withdrawals are also frequently rejected for
  no discernible reason; prepare accordingly.

* **The only reliable ATMs are those of PrivatBank**, where you can withdraw
  up to ₴8,000 in cash at a time, though the 4% commission (up to ₴320 at a
  time) is a joykill.

* If the ATMs at ProCredit Bank (Shevchenka Ave 28) work with your card, you
  may be able to withdraw up to ₴8,000 at a time without a commission.

* To ensure enough cash on hand, a prudent option is to bring some notes in
  foreign currencies of interest (USD, EUR, PLN) and then exchange the notes
  at exchangers or banks at the airport or throughout the center of town.
  Exchange rates are clearly posted and the spread is usually reasonable.

* **Plastic payment methods (Visa, MasterCard, etc) are accepted at most
  points of sale, including hotels, supermarkets, cafes, and restaurants.**

* People may accept foreign currencies of interest (USD, EUR, PLN) in
  payment for private services or for large cash transactions. Many
  Ukrainians save money in (relatively) stabler foreign currencies such as
  USD.

----

Goods
=====

Supermarkets
------------

* The two major supermarket chains are Silpo (Сільпо) and Arsen (Арсен).

* The best supermarkets can be found at major shopping malls such as King
  Cross Leopolis and Victoria Gardens.

* **In the center of town, there is a 24/7 Arsen supermarket on the bottom
  floor of the Roksolana shopping mall on Soborna Square.** It's relatively
  small, congested, and claustrophobic, but the best option in walking
  distance. There are also 24/7 kiosks and convenience stores.

Shopping
--------

The major shopping malls are:

* `Forum Lviv <http://lviv.multi.eu/>`__ (ТРЦ Форум Львів)
  near the center

* `ТРЦ Victoria Gardens <http://www.promo.victoriagardens.com.ua/>`__
  near the airport

* `ТРЦ King Cross Leopolis <http://www.kingcross.com.ua/en/>`__
  near the ring highway

Second-hand
-----------

* `OLX <https://www.olx.ua/uk/lvov/>`__

----

Services
========

Taxi
----

* **Hopping into a random taxi without being able to speak Ukrainian or
  Russian almost invariably means getting the tourist special, which entails
  2-3× the ordinary fare.**

* **Few taxi drivers speak English**. Recommend agreeing the fare and
  destination beforehand using Google Translate.

* **Taxis generally only accept cash**; foreign currencies of interest (USD,
  EUR, PLN) are usually negotiable, though the exchange rate won't be in
  your favor.

* **Taxis often don't have seatbelts**. (And most people don't generally use
  seatbelts in cars.) If you need seatbelts, check for them before stepping
  into a taxi, or just use Uber. Conversely, if you don't want to look like
  a tourist, don't attempt to put on your seatbelt.

* **Use Uber if possible for a better experience** than taxis and a cheaper
  cost than the taxi tourist special (though not cheaper than the ordinary
  fare of taxis). There are up to 1,000 Uber drivers in town and most trips
  cost about ₴50-200 (€2-6). You can use Uber with cash payments. Given that
  you enter your destination into the app, you don't need to speak with the
  driver beyond a basic polite greeting and farewell.

* Other taxi apps include `Uklon <http://www.uklon.com.ua/>`__.
  You can also call `ELC Taxi <http://www.elc.com.ua/>`__ or +380-952600486.

Coworking
---------

There are many coworking spaces, for example:

* `iHUB Lviv <http://ihub.world/en/lviv-en/>`__
  (`@Facebook <https://www.facebook.com/ihublviv/>`__),
  Zamknena Street 9.

Gyms
----

Recommended gyms for `strength training <strength>`__:

* `Sport Life <https://www.sportlife.ua/uk>`__
  Multiple locations in Lviv.
  Nationwide megagym chain.
  Good weightroom, though plates are a mix of kg and lbs.

* `Fitness Forever <http://fitness.lviv.ua/>`__

* `Olimp <http://olimp-strong.com.ua/>`__
  ₴80/visit.

Barbers
-------

Recommended barbers:

* `Solidol
  <https://solidolbarbers.com/>`__
  Kniazia Romana St 28, near the Swiss Hotel.
  Excellent service in English.
  ₴250 for a cut and shave (1h).
  Reserve a day in advance via the website.
  Note that they will call to confirm/remind a few hours prior to appointment, you will need to pick up.

Dentists
--------

Recommended dentists:

* `Kohut & Kohut
  <http://kohutdental.virtual.ua/en/>`__
  Hertsena Street 6/1a.
  Good service, some of the staff speak English.

Doctors
-------

Recommended clinics:

* `American Medical Centers (AMC)
  <http://amcenters.com/lviv/>`__
  Akademika Bohomol'tsya St 3.
  Good service in English. 24/7.

Opticians
---------

* `Luxoptica <https://luxoptica.ua/ua/>`__
  Shevchenka Ave 3.
  No English, use Google Translate.

Delivery of Water
-----------------

* `Ecobutel <https://www.ecobutel.com.ua/uk/>`__

Legal
-----

* `Bachynskyy & Kolomiets <https://bkpartners.legal/en>`__

Schooling
---------

* `Ukrainian Catholic University (UCU) <https://ucu.edu.ua/en/>`__
  ($3,500/semester)

* `Lviv Business School (LvBS) <https://lvbs.com.ua/en/>`__
  of the UCU

* `Lemberg Tech Business School (LemBS) <http://lembs.com/>`__

Post
----

* http://uacargo.com.ua/gorod/lvov
* http://uacargo.com.ua/novaya-pochta/lvov
* http://uacargo.com.ua/mist-ekspress/lvov

* https://novaposhta.ua/office/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2
* https://novaposhta.ua/office/view/id/7/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2
* https://novaposhta.ua/office/view/id/10/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2
* https://novaposhta.ua/office/view/id/16/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2
* https://novaposhta.ua/office/view/id/35/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2
* https://novaposhta.ua/office/view/id/36/city/%D0%9B%D1%8C%D0%B2%D1%96%D0%B2

----

Events
======

English
-------

* `English Speaking Club Lviv
  <https://www.facebook.com/groups/205752549586799/>`__
  every Tuesday from 19:00 to 23:00
  in Щось цікаве (Something Interesting), Rynok Square 13

* `(English) Language Exchange Club Lviv
  <https://www.facebook.com/groups/LEC.Lviv/>`__
  every Wednesday from 19:30 to 22:30
  in Пузата Хата (Puzata Khata), Shevchenka Ave 10

* `English Language Exchange Club Lviv
  <https://www.facebook.com/groups/687960597891262/>`__,
  every Saturday from 15:00 to 23:00
  in Пузата Хата (Puzata Khata), Shevchenka Ave 10

Networking
----------

* `InterNations events & activities
  <https://www.internations.org/calendar/>`__, monthly

Technology
----------

* `Lviv Ruby User Group (#pivorak)
  <https://pivorak.com/>`__, monthly
  (`@Facebook <https://www.facebook.com/pivorak/>`__,
  `@Meetup.com <https://www.meetup.com/ruby-lviv/>`__)

* `R0boCamp <http://robocamp.com.ua/>`__

Miscellaneous
-------------

* `Lviv TIC's events of the day
  <http://www.touristinfo.lviv.ua/en/events/dayevents/>`__,
  daily

* `Lviv cultural events <http://ot-ot.lviv.ua/en/>`__

* `Meetups in Lviv
  <https://www.meetup.com/cities/ua/l%27viv/>`__

----

Activities
==========

Hiking
------

* `Karpaty Travel hiking tours
  <http://www.karpaty.travel/en/tours/hiking/>`__

Bouldering
----------

* `The Wall <http://the-wall.com.ua/en/>`__

* http://lviv.travel/en/index/what_to_do/sport/climbingwall

Airsoft
-------

* http://www.lvivadventure.com/en/lviv-activities/airsoft-gun-shooting.html

Paintball
---------

* http://lviv.travel/en/index/what_to_do/sport/paintball

* http://www.lvivadventure.com/en/lviv-activities/paintball-lviv.html

Laser Tag
---------

* http://www.lvivadventure.com/en/lviv-activities/laser-tag.html
* `Lazer Park <http://www.pivdennij.com/mahazyn/index/index/shop/1820/>`__

----

Languages
=========

`Ukrainian <ukrainian>`__
-------------------------

Courses
^^^^^^^

* `Ukrainian Language and Culture School in Lviv
  <http://learn-ukrainian.org.ua/>`__

* `ECHO Eastern Europe
  <https://echoee.com/lviv/>`__

* `Ukrainian Catholic University Summer Ukrainian Language & Culture School
  <http://studyukrainian.org.ua/en/programs/Ukrainian_language_summer_school>`__

Tutors
^^^^^^

* `Ukrainian language tutors in Lviv via Preply
  <https://preply.com/en/lviv/ukrainian-tutors>`__

`Russian <russian>`__
---------------------

Courses
^^^^^^^

* `ECHO Eastern Europe
  <https://echoee.com/lviv/>`__

Tutors
^^^^^^

* `Russian language tutors in Lviv via Preply
  <https://preply.com/en/lviv/russian-tutors>`__

English
-------

Tutors
^^^^^^

* https://preply.com/en/lviv/english-tutors
* https://www.luke.lv/ (B1 and up)

----

Unsorted
========

* https://www.facebook.com/devfest.ukraine
* https://devfest.gdg.org.ua/
* http://lwo.aero/en/schedule
* https://www.skyscanner.net/flights-to/lwo/airlines-that-fly-to-lviv-airport.html
* https://flights.expedia.com/flights-from-lwo-airport/
* https://en.wikipedia.org/wiki/Ukraine_International_Airlines
* https://www.rome2rio.com/s/Rzesz%C3%B3w/Lviv
* http://www.goeuro.com/buses/rzeszow/lviv
* https://www.busradar.com/coach/lviv/rzeszow/
* https://sendmoney.privatbank.ua/ua/

----

See Also
========

`Ukraine <ukraine>`__: `Kharkiv <kharkiv>`__, `Kiev <kiev>`__, and `Odessa <odessa>`__
