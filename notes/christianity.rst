*****************************
Arto's Notes re: Christianity
*****************************

Politics
========

* `Christian anarchism
  <https://en.wikipedia.org/wiki/Christian_anarchism>`__

Literature
==========

* `The Kingdom of God Is Within You
  <https://en.wikipedia.org/wiki/The_Kingdom_of_God_Is_Within_You>`__
  (Царство Божие внутри вас)
  by Leo Tolstoy

* `Anarchy and Christianity
  <https://www.goodreads.com/book/show/12069548-anarchy-and-christianity>`__
  by Jacques Ellul

See Also
========

`Catholicism <catholicism>`__
