********************************
Arto's Notes re: shooting sports
********************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Safety
======

`Jeff Cooper's four basic rules of gun safety`__:

1. **All guns are always loaded.**
   Even if they are not, treat them as if they are.
2. **Never let the muzzle cover anything you are not willing to destroy.**
   (For those who insist that this particular gun is unloaded, see *Rule #1*.)
3. **Keep your finger off the trigger till your sights are on the target.**
   This is the *Golden Rule*. Its violation is directly responsible for
   about 60 percent of inadvertent discharges.
4. **Identify your target, and what is behind it.**
   Never shoot at anything that you have not positively identified.

__ http://myweb.cebridge.net/mkeithr/Jeff/jeff11_4.html
.. http://myweb.cebridge.net/mkeithr/Jeff/jeff6_2.html
.. http://www.donath.org/Rants/TheFourRules/

----

See Also
========

- Arto's Notes re: `rifles <rifles>`__, `pistols <pistols>`__,
  `gunsmithing <gunsmithing>`__
