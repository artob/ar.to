********************************************************************
Arto's Notes re: `PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__
********************************************************************

* http://postgis.net/install/
* http://postgis.net/docs/

Installation
============

::

   $ brew install postgis --without-sfcgal

See Also
========

`KML <kml>`__
