*****************************************************************************************
Arto's Notes re: `virtual reality (VR) <https://en.wikipedia.org/wiki/Virtual_reality>`__
*****************************************************************************************

* `Software <#software>`__
* `Hardware <#hardware>`__
* `See Also <#see-also>`__

----

Software
========

Platforms
---------

* `Unity <unity>`__ (C#)

* `Unreal Engine <unreal>`__ (C++)

SDKs
----

* `OpenHMD <http://www.openhmd.net/>`__

* `OSVR <https://osvr.github.io/>`__

* `OpenVR/SteamVR <https://github.com/ValveSoftware/openvr/wiki/API-Documentation>`__

* `Viveport SDK <https://developer.viveport.com/documents/sdk/en/viveportsdk.html>`__

----

Hardware
========

`Headsets <https://en.wikipedia.org/wiki/Virtual_reality_headset>`__
--------------------------------------------------------------------

Shipping
^^^^^^^^

* `OSVR HDK 2 <osvr>`__ (MSRP $399)
* `Oculus Rift <https://en.wikipedia.org/wiki/Oculus_Rift>`__ (MSRP $399)
* `Oculus Go <https://en.wikipedia.org/wiki/Oculus_VR#Oculus_Go>`__ (MSRP $199)
* `HTC Vive <https://en.wikipedia.org/wiki/HTC_Vive>`__ (MSRP $599)
* `Samsung Gear VR <https://en.wikipedia.org/wiki/Samsung_Gear_VR>`__ (MSRP $129)

Upcoming
^^^^^^^^

* `FOVE 0 <https://en.wikipedia.org/wiki/Fove>`__ (MSRP $599) with eye-tracking

Requirements
^^^^^^^^^^^^

* NVIDIA GTX `970 <https://en.wikipedia.org/wiki/GeForce_900_series>`__/`1060 <https://en.wikipedia.org/wiki/GeForce_10_series>`__ GPU,
  Intel Core `i5-4590 <https://ark.intel.com/products/80815>`__/`i3-8350K <https://ark.intel.com/products/126689>`__ CPU,
  8 GiB RAM.

* 1× HDMI 1.4 port, 3× USB 3.0 ports, 1× USB 2.0 port.

Sources:

* https://www.vrheads.com/heres-what-you-need-your-pc-run-vr
* http://www.osvr.org/getting-started.html
* https://support.oculus.com/170128916778795/
* https://www.vive.com/us/ready/

----

See Also
========

`augmented reality (AR) <ar>`__, `3D graphics <3d>`__, `GPUs <gpu>`__
