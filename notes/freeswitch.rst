**************************************************************************
Arto's Notes re: `FreeSWITCH <https://en.wikipedia.org/wiki/FreeSWITCH>`__
**************************************************************************

* https://freeswitch.org
* https://freeswitch.org/confluence/display/FREESWITCH/Introduction
* https://freeswitch.org/how-does-freeswitch-compare-to-asterisk/

Installation
============

Installation on macOS
---------------------

::

   $ brew install freeswitch --without-moh

   $ freeswitch -nc --nonat

News
====

* https://freeswitch.org/blog/
* https://twitter.com/freeswitch
* https://twitter.com/anthmfs
* https://twitter.com/briankwest

Forums
======

* https://www.reddit.com/r/freeswitch/
* https://www.reddit.com/r/VOIP/

Events
======

* https://twitter.com/cluecon
* https://www.cluecon.com

See Also
========

* `Asterisk <asterisk>`__
* `Yate <https://en.wikipedia.org/wiki/Yate_(telephony_engine)>`__
