**********************************************************************
Arto's Notes re: `calculus <https://en.wikipedia.org/wiki/Calculus>`__
**********************************************************************

The `mathematical <math>`__ study of continuous change.

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Guides
======

- `Calculus Learning Guide
  <https://betterexplained.com/guides/calculus/>`__
  at Better Explained

----

See Also
========

- Arto's Notes re: `mathematics <math>`__

- `Calculus questions on Mathematics Stack Exchange
  <https://math.stackexchange.com/questions/tagged/calculus>`__
