*****************************************************************************************************
Arto's Notes re: `Markov logic networks (MLN) <https://en.wikipedia.org/wiki/Markov_logic_network>`__
*****************************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Courses
=======

- `Markov Logic Networks (Carnegie Mellon 10-803)
  <https://homes.cs.washington.edu/~pedrod/803/>`__ (2008)

----

People
======

- `Pedro Domingos <https://en.wikipedia.org/wiki/Pedro_Domingos>`__
  (`@pmddomingos <https://twitter.com/pmddomingos>`__,
  `home page <https://homes.cs.washington.edu/~pedrod/>`__)

----

See Also
========

- Arto's Notes re: `artificial intelligence (AI) <ai>`__,
  `machine learning (ML) <ml>`__

- https://www.quora.com/What-is-the-state-of-the-art-in-Markov-Logic-Networks
