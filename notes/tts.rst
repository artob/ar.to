******************************************************************************************
Arto's Notes re: `text-to-speech (TTS) <https://en.wikipedia.org/wiki/Speech_synthesis>`__
******************************************************************************************

Software
========

eSpeak NG
---------

* `eSpeak NG <https://en.wikipedia.org/wiki/ESpeakNG>`__
  in Wikipedia
* https://github.com/espeak-ng/espeak-ng
* https://launchpad.net/ubuntu/+source/espeak-ng
* https://eeejay.github.io/espeak/emscripten/espeak.html
* http://odo.lv/Espeak?language=en

Festival/FestVox/Flite
----------------------

* `Festival
  <https://en.wikipedia.org/wiki/Festival_Speech_Synthesis_System>`__
  in Wikipedia
* `Festival <http://www.cstr.ed.ac.uk/projects/festival/>`__
* `CMU Festvox <http://www.festvox.org/>`__
* `CMU Flite <http://www.festvox.org/flite/>`__

FreeTTS
-------

* `FreeTTS <https://en.wikipedia.org/wiki/FreeTTS>`__
  in Wikipedia

`MaryTTS <http://mary.dfki.de/>`__
----------------------------------

* https://github.com/marytts/marytts

Miscellaneous
-------------

* `Gnuspeech <https://en.wikipedia.org/wiki/Gnuspeech>`__

Standards
=========

* `Speech Synthesis Markup Language (SSML)
  <https://en.wikipedia.org/wiki/Speech_Synthesis_Markup_Language>`__
  from the W3C

Unsorted
========

* https://en.wikipedia.org/wiki/CMU_Pronouncing_Dictionary
* https://wiki.gnome.org/Projects/Orca/SpeechSynthesisEngines
