********************************************************************************
Arto's Notes re: `KML <https://en.wikipedia.org/wiki/Keyhole_Markup_Language>`__
********************************************************************************

* https://developers.google.com/maps/support/kmlmaps
* https://developers.google.com/maps/documentation/javascript/kml
* https://developers.google.com/maps/documentation/javascript/examples/layer-kml

* https://developers.google.com/kml/documentation/kml_tut
* https://developers.google.com/kml/documentation/kmlreference

See Also
========

`PostGIS <postgis>`__
