*******************************************************************************************
Arto's Notes re: `machine learning (ML) <https://en.wikipedia.org/wiki/Machine_learning>`__
*******************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Tribes
======

The five machine-learning paradigms described by Pedro Domingos in
`The Master Algorithm <https://en.wikipedia.org/wiki/The_Master_Algorithm>`__:

.. list-table::
   :widths: 25 25 25 25
   :header-rows: 1

   * - Tribe
     - Origins
     - Problem
     - Solution

   * - Symbolists
     - Logic, philosophy
     - Knowledge composition
     - Induction

   * - Connectionists
     - Neuroscience
     - Credit assignment
     - Backpropagation

   * - Evolutionaries
     - Evolutionary biology
     - Structure discovery
     - Genetic programming

   * - Bayesians
     - Statistics
     - Uncertainty
     - Probabilistic inference

   * - Analogizers
     - Psychology
     - Similarity
     - Kernel machines

Symbolists
----------

TODO

Connectionists
--------------

TODO

Evolutionaries
--------------

TODO

Bayesians
---------

TODO

Analogizers
-----------

TODO

----

Essays
======

- `Ways to think about machine learning
  <https://www.ben-evans.com/benedictevans/2018/06/22/ways-to-think-about-machine-learning-8nefy>`__
  (2018) by Ben Evans

----

Books
=====

- `Mathematics for Machine Learning <https://mml-book.github.io/>`__
  (`@HN <https://news.ycombinator.com/item?id=16750789>`__)

----

Courses
=======

- `Mathematics for Machine Learning
  <https://www.coursera.org/specializations/mathematics-machine-learning>`__
  at Coursera

----

Glossary
========

- parameter: a value that the algorithm itself mutates over time.

- hyperparameter: a value specified prior to learning, e.g., the learning rate.

----

News
====

- https://www.reddit.com/r/MachineLearning/
- https://www.reddit.com/r/learnmachinelearning/
- http://www.wildml.com/newsletter/
- https://twitter.com/jeremyphoward/likes

----

See Also
========

- Arto's Notes re: `artificial intelligence (AI) <ai>`__,
  `deep learning (DL) <dl>`__,
  `neural networks (NNs) <nn>`__,
  `Markov logic network (MLN) <mln>`__,
  `fast.ai <fastai>`__,
  `NuPIC <nupic>`__,
  `OpenAI <openai>`__,
  `TensorFlow <tensorflow>`__
