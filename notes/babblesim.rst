*************************************************************
Arto's Notes re: `BabbleSim <https://babblesim.github.io/>`__
*************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- https://babblesim.github.io

- https://github.com/BabbleSim

----

Tutorials
=========

- https://docs.zephyrproject.org/latest/boards/posix/nrf52_bsim/doc/board.html

----

See Also
========

- Arto's Notes re: `RTOSes <rtos>`__, `Mynewt <mynewt>`__
