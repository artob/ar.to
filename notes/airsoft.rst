********************************************************************
Arto's Notes re: `airsoft <https://en.wikipedia.org/wiki/Airsoft>`__
********************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Retailers
=========

International
-------------

- `Airsoft GI <https://www.airsoftgi.com>`__ (USA)
  also at:
  `Twitter <https://twitter.com/AirsoftGI>`__,
  `YouTube <https://www.youtube.com/user/AIRSOFTGIdotcom>`__

- `Airsoft Megastore <http://www.airsoftmegastore.com>`__ (USA)
  also at:
  `Twitter <https://twitter.com/airsoftms>`__,
  `YouTube <https://www.youtube.com/user/airsoftmegastoreCOM>`__

- `Evike <https://www.evike.com>`__ (USA)
  also at:
  `Twitter <https://twitter.com/EvikeSuperStore>`__,
  `YouTube <https://www.youtube.com/user/evikecom>`__

- `Redwolf Airsoft <https://www.redwolfairsoft.com>`__ (UK, Hong Kong)
  also at:
  `Twitter <https://twitter.com/redwolfairsoft>`__,
  `YouTube <https://www.youtube.com/user/redwolfairsoft>`__

Ukraine
-------

- `Sturm (ШТУРМ) <https://sturm.com.ua>`__ (Lviv)
  also at:
  `Facebook <https://www.facebook.com/sturmmag/>`__

- `Junker <https://junker.kiev.ua>`__ (Kiev)
  also at:
  `Twitter <https://twitter.com/Junkerkievua>`__,
  `Instagram <https://www.instagram.com/junker_shop/>`__

----

Manufacturers
=============

AEG Rifles
----------

- `APS <http://www.aps-concept.com>`__ (Hong Kong)

- `SRC <http://www.starrainbow.com.tw/?lang=en>`__ (Taiwan)

- `Tokyo Marui <https://www.tokyo-marui.co.jp>`__ (Japan)

Control Systems
---------------

- `Airsoft Systems <http://www.airsoftsystems.com>`__ (Bulgaria)

- `GATE Electronics <https://www.gatee.eu>`__ (Poland)

- `Systema <http://www.systema-engineering.com>`__ (Japan)

- `Wolverine Airsoft <https://wolverineairsoft.com>`__ (USA)

- `Xcortech <http://www.xcortech.com.tw>`__ (Taiwan)

----

Technology
==========

Batteries
---------

TODO

Gearbox
-------

- Version 2
- Version 3
- Version 7

See also many more details in answers to `this question on Quora
<https://www.quora.com/How-many-versions-of-airsoft-gearboxes-are-there-What-guns-are-they-used-for>`__.

MOSFET
------

TODO

Fire Control Unit (FCU)
-----------------------

TODO

----

Tutorials
=========

- `Airsoft Beginner's Guide
  <https://www.youtube.com/watch?v=S3IBULpytmo>`__ (7m58s)

- `Starting Out with Airsoft: The Basics & What You Need to Know
  <https://www.youtube.com/watch?v=LjG2ggzHDXA>`__ (21m42s)

- `Complete Guide for Beginner Airsoft Players
  <https://www.youtube.com/watch?v=fYJAYI6EXYU>`__ (19m54s)

- `Complete Guide to Purchasing Your First Airsoft Gun
  <https://www.youtube.com/watch?v=e5epVEvzMM4>`__ (21m44s)

----

Forums
======

- https://www.reddit.com/r/airsoft/

----

Glossary
========

BB
   The ammunition for airsoft guns

AEG
   Automatic Electric Gun

AEP
   Automatic Electric Pistol

ASCU
   Airsoft Systems Smart Control Unit

BDU
   Battle Dress Uniform

blowback
   Simulated recoil

clip
   Magazine

CQB
   Close Quarters Battle

DMR
   `Designated Marksman Rifle
   <https://en.wikipedia.org/wiki/Designated_marksman_rifle>`__

ECU
   Electrical Control Unit

FCU
   Fire Control Unit

FPS
   Feet Per Second

Hi-Cap Mag
   High-capacity magazine, holding 300+ BBs

hop-up
   `Hop-up <https://en.wikipedia.org/wiki/Hop-up_(airsoft)>`__ devices apply
   backspin to the projectile, extending the effective range of the weapon
   without increasing projectile velocity.

HPA
   High Pressure Air

loadout
   TODO

Low-Cap Mag
   Low-capacity magazine, reflecting the capacity of their firearm
   counterparts

Mid-Cap Mag
   Middle-capacity magazine, holding ~100 BBs

MTW
   Modular Training Weapon

PTW
   Professional Training Weapons from Systema.
   The premium electric airsoft guns available today.

ROF
   Rate of Fire

RPM
   Rounds Per Minute

RPS
   Rounds Per Second

----

Unsorted
========

- https://airsoftc3.com
- https://www.airsoftmap.fr
- https://www.airsoftmap.net

----

See Also
========

- Arto's Notes re: `MILSIM <milsim>`__
