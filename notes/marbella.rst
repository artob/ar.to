**********************************************************************
Arto's Notes re: `Marbella <https://en.wikipedia.org/wiki/Marbella>`__
**********************************************************************

Transportation
==============

* Uber does not work in Marbella.

Logistics
=========

* Amazon.es delivers using SEUR.
* Amazon.de delivers using DHL Express Worldwide EU for one-day delivery.

Supermarkets
============

* Mercadona (Mon-Sat 09:00-21:30)
* Supercor Express (Mon-Sun 08:00-02:00)

Services
========

Barbers
-------

* London Ali's, Av Ramón y Cajal, 29601 Marbella.
  Mon-Sat ?:?-?:?.

Gyms
----

* `Gimnasio Atenas Marbella <http://www.gimnasioatenas.com/contacto/>`__,
  `Calle Pintor Pacheco, 29603 Marbella <https://goo.gl/maps/CrZitzQSjNp>`__.
  €10/visit, €33/month. Mon-Fri 08:00-22:30, Sat 10:00-21:30.
  Mornings are quiet. Hefty single squat rack off in the corner, though with
  treacherous footing due to somewhat uneven floor at that spot. Multiple
  bench racks. Difficult to find enough floor space for deadlifts.

Laundry
-------

* La Fuente.
  Avda. Aries de Velesco 1, 29600 Marbella.
  Ficha lavadora 8kg €4.75. Ficha secadora €3.25 .

Opticians
---------

* `Specsavers <https://en.specsavers.es/stores/marbella>`__,
  Avda. Ricardo Soriano 12 Local 5A, 29601 Marbella.

Unsorted
========

* http://classifieds.surinenglish.com/
