******************************************************************
Arto's Notes re: `Gradle <https://en.wikipedia.org/wiki/Gradle>`__
******************************************************************

https://gradle.org

Installation
============

macOS
-----

::

   $ brew install gradle

Tutorials
=========

* `Java Quickstart
  <https://docs.gradle.org/current/userguide/tutorial_java_projects.html>`__
