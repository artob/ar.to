**************************************************************
Arto's Notes re: `gRPC <https://en.wikipedia.org/wiki/GRPC>`__
**************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- https://grpc.io
- https://twitter.com/grpcio
- https://github.com/grpc
- https://stackoverflow.com/tags/grpc

----

Languages
=========

- `C++ <https://github.com/grpc/grpc/tree/master/src/cpp>`__

- `C# <https://github.com/grpc/grpc/tree/master/src/csharp>`__

- `Dart <https://github.com/grpc/grpc-dart>`__

- `Elixir <https://github.com/tony612/grpc-elixir>`__ (third-party)

- `Go <https://github.com/grpc/grpc-go>`__

- `Java <https://github.com/grpc/grpc-java>`__

- `Node.js <https://github.com/grpc/grpc-node>`__

- `Objective-C <https://github.com/grpc/grpc/tree/master/src/objective-c>`__

- `PHP <https://github.com/grpc/grpc/tree/master/src/php>`__

- `Python <https://github.com/grpc/grpc/tree/master/src/python/grpcio>`__

- `Ruby <https://github.com/grpc/grpc/tree/master/src/ruby>`__

----

`Installation <https://packages.grpc.io/>`__
============================================

::

   $ go get google.golang.org/grpc  # Go
   $ npm install grpc               # Node.js
   $ pecl install grpc              # PHP
   $ pip3 install grpcio            # Python 3
   $ gem install grpc               # Ruby

   $ go get -u github.com/golang/protobuf/protoc-gen-go
   $ pip3 install grpcio-tools googleapis-common-protos            # Python 3
   $ gem install grpc-tools

Installation on `macOS <mac>`__
-------------------------------

https://github.com/Homebrew/homebrew-core/blob/master/Formula/grpc.rb

::

   $ brew install grpc

Installation on `Alpine <alpine>`__
-----------------------------------

https://pkgs.alpinelinux.org/package/edge/testing/x86_64/grpc

::

   $ apk add grpc

Installation on `Ubuntu <ubuntu>`__
-----------------------------------

https://packages.ubuntu.com/libgrpc-dev

::

   $ sudo apt install libgrpc0 libgrpc-dev

----

Tutorials
=========

Quick Start
-----------

- `C++ <https://grpc.io/docs/quickstart/cpp.html>`__

- `Dart <https://grpc.io/docs/quickstart/dart.html>`__

- `Go <https://grpc.io/docs/quickstart/go.html>`__

- `Java <https://grpc.io/docs/quickstart/java.html>`__

- `PHP <https://grpc.io/docs/quickstart/php.html>`__

- `Python <https://grpc.io/docs/quickstart/python.html>`__

- `Ruby <https://grpc.io/docs/quickstart/ruby.html>`__

----

See Also
========

- Arto's Notes re: `Protocol Buffers <protobuf>`__,
  `polyglotism <polyglot>`__
