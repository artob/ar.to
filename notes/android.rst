***************************************************************************************
Arto's Notes re: `Android <https://en.wikipedia.org/wiki/Android_(operating_system)>`__
***************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Releases
========

- `Android 9.0 Pie
  <https://developer.android.com/about/versions/pie/>`__
  (API Level 28)

- `Android 8.0 Oreo
  <https://developer.android.com/about/versions/oreo/>`__
  (API Level 26)

- `Android 7.0 Nougat
  <https://developer.android.com/about/versions/nougat/>`__
  (API Level 24)

- `Android 6.0 Marshmallow
  <https://developer.android.com/about/versions/marshmallow/>`__
  (API Level 23)

See Also
--------

- https://source.android.com/setup/build-numbers

- https://en.wikipedia.org/wiki/Android_version_history

----

Debugging
=========

::

   $ export PATH=$HOME/Library/Android/sdk/platform-tools:$PATH

   $ adb devices

----

Languages
=========

- `Java <java>`__ and `Kotlin <kotlin>`__
- `Groovy <groovy>`__
- `Lua <lua>`__ via `LuaJ <http://www.luaj.org/luaj/README.html>`__?
- `Erlang <erlang>`__/`Elixir <elixir>`__ via Erjang?
- `Ruby <ruby>`__ via `RubyMotion <http://www.rubymotion.com/>`__
- https://en.wikipedia.org/wiki/Android_software_development

----

Reference
=========

- `Android Debug Bridge (adb)
  <https://developer.android.com/tools/help/adb.html>`__

- `API differences between API levels 27 and 28
  <https://developer.android.com/sdk/api_diff/28/changes.html>`__

- `API differences between API levels 26 and 27
  <https://developer.android.com/sdk/api_diff/27/changes.html>`__

- `API differences between API levels 25 and 26
  <https://developer.android.com/sdk/api_diff/26/changes.html>`__

- `API differences between API levels 24 and 25
  <https://developer.android.com/sdk/api_diff/25/changes.html>`__

- `API differences between API levels 23 and 24
  <https://developer.android.com/sdk/api_diff/24/changes.html>`__

----

Books
=====

- `The Busy Coder's Guide to Android Development
  <https://commonsware.com/Android/>`__
  (`Kindle <https://www.amazon.com/dp/B06Y4TCV7F>`__,
  `code <https://github.com/commonsguy/cw-omnibus>`__)

----

Courses
=======

- `Developing Android Apps
  <https://www.udacity.com/course/new-android-fundamentals--ud851>`__
  via Udacity

----

Unsorted
========

- https://developer.android.com/studio/index.html
- https://developer.android.com/studio/install.html
- https://developer.android.com/guide/index.html
- https://developer.android.com/tools/projects/projects-cmdline.html
- https://developer.android.com/tools/building/building-cmdline.html
- https://stackoverflow.com/questions/tagged/android
- https://stackoverflow.com/questions/17625622/how-to-completely-uninstall-android-studio

----

- Arto's Notes re: `Java <java>`__, `Kotlin <kotlin>`__,
  `Flutter <flutter>`__
