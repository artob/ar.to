***********************
Arto's Notes re: travel
***********************

Shortlist
=========

* passport
* wallet
* phone
* laptop

Checklist
=========

* on person
  * wallet & cash
  * passport & copy of passport
  * boarding pass (after online check-in)
  * health insurance certificate
  * phone
  * glasses
  * watch
  * armband
  * rings
* in backpack
  * luggage tag
  * pen
  * sleeping mask
  * earbuds
  * contact lenses
  * spare glasses
  * laptop & laptop charger
  * USB charger
  * USB power pack
  * headphones
* in suitcase
  * luggage tag
  * toiletries: toothbrush, etc
  * `supplements <supplements>`__
  * condoms
  * clothing
  * external SSD
* miscellaneous
  * HDMI display adapter
  * offline maps
  * offline dictionaries

Shopping list
=============

* toothpaste, mouthwash, dental floss
* shower gel, shampoo
* sunblock, aftersun lotion
* bandaids
* mosquito repellent

Events
======

* `InterNations <https://www.internations.org/>`__
* `Meetup.com <https://www.meetup.com/>`__

Airlines
========

* `SkyScanner
  <https://www.skyscanner.net/>`__

Americas
--------

* `Norwegian Air
  <http://www.norwegian.com/en/destinations/>`__ (from CPH, LGW)

Europe
------

* `airBaltic
  <https://www.airbaltic.com/en-DE/index>`__
* `Belavia
  <https://en.belavia.by/home/>`__
* `Norwegian Air
  <http://www.norwegian.com/en/destinations/>`__ (from BER)
* `Pegasus Airlines
  <https://www.flypgs.com/en/>`__ (from SXF, LWO)
* `Ryanair
  <https://www.ryanair.com/gb/en/>`__ (from SXF, LWO)
* `Wizz Air
  <https://wizzair.com/>`__ (from SXF, LWO)

Coaches
=======

Europe
------

* `Flixbus
  <https://www.flixbus.com/>`__
* `PolskiBus
  <http://www.polskibus.com/en/index.htm>`__

See Also
========

* `Backup <backup>`__
* `Cities <cities>`__
* `Conferences <conferences>`__
* `Events <events>`__
* `Inventory <inventory>`__
* `Languages <languages>`__
* `Modafinil <modafinil>`__
