**************************************************************************
Arto's Notes re: `PCs <https://en.wikipedia.org/wiki/Personal_computer>`__
**************************************************************************

* `Components <#components>`__
* `Peripherals <#peripherals>`__

----

Components
==========

Essential
---------

* `power supply (PSU)
  <https://en.wikipedia.org/wiki/Power_supply_unit_(computer)>`__

* `motherboard (mobo) <https://en.wikipedia.org/wiki/Motherboard>`__
  with an integrated
  `NIC <https://en.wikipedia.org/wiki/Network_interface_controller>`__
  and optional
  `audio <https://en.wikipedia.org/wiki/Sound_card>`__/`video <https://en.wikipedia.org/wiki/Video_card>`__
  support

* `processor (CPU) <https://en.wikipedia.org/wiki/Central_processing_unit>`__
  (with heatsink or cooler)
  compatible with the mobo's
  `socket <https://en.wikipedia.org/wiki/CPU_socket>`__

* `RAM memory <https://en.wikipedia.org/wiki/Random-access_memory>`__
  compatible with the mobo

* internal persistent storage, such as
  an `SSD <https://en.wikipedia.org/wiki/Solid-state_drive>`__
  or a `HDD <https://en.wikipedia.org/wiki/Hard_disk_drive>`__

Optional
--------

* `chassis (case) <https://en.wikipedia.org/wiki/Computer_case>`__

* `graphics processing unit (GPU) <gpu>`__

* `optical (DVD or Blu-ray) drive <https://en.wikipedia.org/wiki/Optical_disc_drive>`__

----

Peripherals
===========

Essential
---------

* `monitor <https://en.wikipedia.org/wiki/Computer_monitor>`__

* `keyboard <https://en.wikipedia.org/wiki/Computer_keyboard>`__

* `mouse <https://en.wikipedia.org/wiki/Computer_mouse>`__

Optional
--------

* `speakers <https://en.wikipedia.org/wiki/Computer_speakers>`__

* `webcam <https://en.wikipedia.org/wiki/Webcam>`__

* `printer <https://en.wikipedia.org/wiki/Printer_(computing)>`__

* `scanner <https://en.wikipedia.org/wiki/Image_scanner>`__
