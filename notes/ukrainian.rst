*********************************************************************************
Arto's Notes re: `Ukrainian <https://en.wikipedia.org/wiki/Ukrainian_language>`__
*********************************************************************************

The national language of `Ukraine <ukraine>`__.

* `Alphabet <#alphabet>`__
* `Numbers <#numbers>`__
* `Vocabulary <#vocabulary>`__
* `Grammar <#grammar>`__
* `Learning <#learning>`__
* `Links <#links>`__
* `See Also <#see-also>`__

----

`Alphabet <https://en.wikipedia.org/wiki/Ukrainian_alphabet>`__
===============================================================

*(To be expanded.)*

----

Numbers
=======

Cardinals
---------

один, два, три, чотири, п'ять, шість, сім, вісім, дев'ять

1. один
2. два
3. три
4. чотири
5. п'ять
6. шість
7. сім
8. вісім
9. дев'ять

Ordinals
--------

перший, другий, третій.

перше, друге, третє.

1. перший, перше
2. другий, друге
3. третій, третє

----

Vocabulary
==========

* кого: who, whom
* що: what, that
* для: for
* тому що: because

----

Miscellaneous
-------------

* без нічого: without anything
* без алкоголю: without alcohol
* тільки …: only … (+nom)
* усе: everything, all-in-all

----

Connectives
-----------

* and: і
* or: або, чи
* but: але
* although: хоч

----

Pronouns
--------

Personal pronouns (Особові займенники)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* I: Я
* we: ми
* you (sg.): ти
* you (pl.): ви
* he: він
* she: вона
* it: воно
* they: вони

----

Days
----

1. Понеділок
2. вівторок
3. середа
4. четвер
5. п'ятниця
6. субота
7. неділя

----

`Colors <https://ukrainianlessons.com/vocabulary-colors/>`__
------------------------------------------------------------

* white: білий
* black: чорний
* red: червоний

----

Dishes
------

Сніданок
^^^^^^^^

бутерброд, каша, яєчня

Обід
^^^^

0. закуска: овочевий салат (огірок, помідор, перець, салат, капуста, …)
1. перше: суп (борщ, …)
2. друге: м'ясо (…) і гарнір (картопля, гречка, рис, макарони, …)
3. третє: напої (кава, чай, сік) і десерти ()

----

Verbs
-----

* їхати (to go by transport at this moment): Я іду
* Йти (to go by foot now): Я йду
* Ходити (to go by foot usual): 
* Гуляти (to walk): Я гуляю
* Подорожувати (to travel): Я подорожую

* Писати (to write): Я пишу
* Малювати (to draw): Я малюю
* Розмовляти (to speak): Я розмовляю
* Говорити (to speak): Я говорю
* Працювати (to work): Я працюю

* Спробувати (to try): Я спробую
* Готувати (to cook): Я готую
* Хотіти (to want): я хочу
* Купити (to buy): я купляю
* Наказувати (to order): Я замовляю
* Мати (to have): я маю


----

`Grammar <https://en.wikipedia.org/wiki/Ukrainian_grammar>`__
=============================================================

* `Tenses <#tenses>`__

* `Cases <#cases>`__ (Відмінки)

  * `Nominative case <#nominative-case>`__

  * `Genitive case <#genitive-case>`__

  * `Dative case <#dative-case>`__

  * `Accusative case <#accusative-case>`__

  * `Instrumental case <#instrumental-case>`__

  * `Locative case <#locative-case>`__

  * `Vocative case <#vocative-case>`__

* `Genders <#genders>`__

Ukrainian contains 7 cases and 2 numbers for its nominal declension; 2 aspects, 3 tenses, 3 moods, and 2 voices for its verbal conjugation. Adjectives must agree in number, gender, and case with their nouns.

----

`Tenses <https://en.wikipedia.org/wiki/Grammatical_tense>`__
------------------------------------------------------------

Ukrainian has 3 verb tenses:

1. Past tense (Минулий час)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

2. Present tense (Теперішній час)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3. Future tense (Майбутній час)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The future tense is the easiest.

----

`Cases <https://en.wikipedia.org/wiki/Grammatical_case>`__ (Відмінки)
---------------------------------------------------------------------

Ukrainian has 7 grammatical cases:

----

1. `Nominative case <https://en.wikipedia.org/wiki/Nominative_case>`__ (Називний відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks the subject of a verb.

----

2. `Genitive case <https://en.wikipedia.org/wiki/Genitive_case>`__ (Родовий відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks a noun as modifying another noun; marks a noun as being the possessor of another noun.

----

3. `Dative case <https://en.wikipedia.org/wiki/Dative_case#Slavic_languages>`__ (Давальний відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks the indirect object of a verb.

----

4. `Accusative case <https://en.wikipedia.org/wiki/Accusative_case>`__ (Знахідний відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks the direct object of a transitive verb.

Masculine (Чоловічий рід)
"""""""""""""""""""""""""

If the noun is inanimate, nothing changes. Otherwise:

* –□, vowels → –a
* –ь → –я

Feminine (Жіночий рід)
""""""""""""""""""""""

* –а → –у
* –я → –ю
* –□ → –□ (ніч, піч, річ, любов)

Neuter (Середній рід)
"""""""""""""""""""""

Nothing changes for inanimate nouns (almost all neuter nouns).

Links
"""""

* https://ukrainianlessons.com/accusativecase/
* http://www.ukrainianlanguage.org.uk/read/unit07/page7-4.htm

----

5. `Instrumental case <https://en.wikipedia.org/wiki/Instrumental_case>`__ (Орудний відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks the instrument or means by or with which the subject achieves or accomplishes an action; may denote either a physical object or an abstract concept.

----

6. `Locative case <https://en.wikipedia.org/wiki/Locative_case#Slavic_languages>`__ (Місцевий відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Marks a location, used only with prepositions. Corresponds vaguely to the English prepositions "in", "on", "at", and "by".

----

7. `Vocative case <https://en.wikipedia.org/wiki/Vocative_case#Ukrainian>`__ (Кличний відмінок)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Identifies a person (animal, object, etc) being addressed or occasionally the determiners of that noun.

----

`Genders <https://en.wikipedia.org/wiki/Grammatical_gender>`__
--------------------------------------------------------------

Masculine (Чоловічий рід)
^^^^^^^^^^^^^^^^^^^^^^^^^

Nouns (Роди іменника)
"""""""""""""""""""""

* –□
* –ь
* –о (animate)

Feminine (Жіночий рід)
^^^^^^^^^^^^^^^^^^^^^^

Nouns (Роди іменника)
"""""""""""""""""""""

* –а
* –я
* *Exceptions:* ніч, піч, річ, любов

Neuter (Середній рід)
^^^^^^^^^^^^^^^^^^^^^

Nouns (Роди іменника)
"""""""""""""""""""""

* –e
* –о (inanimate)
* –XXя, e.g., –ння, –ття
* *Exceptions:* ім'я

----

Adjectives
----------

Adjectives agree with the nouns they modify in gender, number, and case.

Hard declension
^^^^^^^^^^^^^^^

–ий (m), –а (f), –е (n), –і (pl)

Soft declension
^^^^^^^^^^^^^^^

–ій (m), –я (f), –є (n), –і (pl)

----

Adverbs
-------

*(To be expanded.)*

–о, –е

----

Learning
========

Courses
-------

* `Ukrainian Language and Culture School in Lviv
  <http://learn-ukrainian.org.ua/>`__

* `ECHO Eastern Europe
  <https://echoee.com/lviv/>`__

* `Ukrainian Catholic University Summer Ukrainian Language & Culture School
  <http://studyukrainian.org.ua/en/programs/Ukrainian_language_summer_school>`__

Tutors
------

* `Ukrainian language tutors via Preply
  <https://preply.com/en/lviv/ukrainian-tutors>`__
  (`Lviv <lviv>`__, etc.)

----

Links
=====

* `Ukrainian on Stack Exchange
  <https://ukrainian.stackexchange.com/>`__

* `Ukrainian on Reddit
  <https://www.reddit.com/r/Ukrainian/>`__

----

See Also
========

`languages <languages>`__, `Russian <russian>`__, `Ukraine <ukraine>`__
