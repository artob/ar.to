********************************************************************************************
Arto's Notes re: `Amazon Web Services <https://en.wikipedia.org/wiki/Amazon_Web_Services>`__
********************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Services
========

Application Integration
-----------------------

- `Amazon Simple Notification Service (SNS)
  <https://aws.amazon.com/sns/>`__
  [SMS]

- `Amazon Simple Queue Service (SQS)
  <https://aws.amazon.com/sqs/>`__
  [MQ]

AR & VR
-------

- `Amazon Sumerian
  <https://aws.amazon.com/sumerian/>`__
  [WebVR]

Compute
-------

- `AWS Elastic Compute Cloud (EC2)
  <https://aws.amazon.com/ec2/on-demand/>`__
  [hosting]

- `AWS Batch
  <https://aws.amazon.com/batch/>`__
  [batch jobs]

- `AWS Elastic Beanstalk
  <https://aws.amazon.com/elasticbeanstalk/>`__
  [PaaS]

- `AWS Lambda
  <https://aws.amazon.com/lambda/>`__

- `AWS Elastic Load Balancing (ELB)
  <https://aws.amazon.com/elasticloadbalancing/>`__
  [proxy]

Customer Engagement
-------------------

- `Amazon Simple Email Service (SES)
  <https://aws.amazon.com/ses/>`__
  [SMTP]

Database
--------

- `Amazon Aurora
  <https://aws.amazon.com/rds/aurora/>`__
  [relational DBaaS]

- `Amazon Neptune
  <https://aws.amazon.com/neptune/>`__
  [graph DBaaS]

- `Amazon Relational Database Service (RDS) for PostgreSQL
  <https://aws.amazon.com/rds/postgresql/>`__
  [hosted PostgreSQL]

Game Development
----------------

- `Amazon GameLift
  <https://aws.amazon.com/gamelift/>`__
  [hosted game servers]

- `Amazon Lumberyard
  <https://aws.amazon.com/lumberyard/>`__
  [3D engine]

Machine Learning
----------------

- `Amazon SageMaker
  <https://aws.amazon.com/sagemaker/>`__
  [deep learning]

- `Amazon Rekognition
  <https://aws.amazon.com/rekognition/>`__
  [computer vision]

Management Tools
----------------

- `AWS CloudFormation
  <https://aws.amazon.com/cloudformation/>`__
  [SCM]

Network & Content Delivery
--------------------------

- `Amazon Virtual Private Cloud (VPC)
  <https://aws.amazon.com/vpc/>`__
  [intranet]

- `Amazon API Gateway
  <https://aws.amazon.com/api-gateway/>`__
  [proxy]

- `Amazon CloudFront
  <https://aws.amazon.com/cloudfront/>`__
  [CDN]

- `Amazon Route 53
  <https://aws.amazon.com/route53/>`__
  [DNS]

Storage
-------

- `Amazon Simple Storage Service (S3)
  <https://aws.amazon.com/s3/>`__
  [object store]

- `Amazon Elastic Block Store (EBS)
  <https://aws.amazon.com/ebs/>`__
  [disk volume]

- `Amazon Elastic File System (EFS)
  <https://aws.amazon.com/efs/>`__
  [file system]

----

Tooling
=======

- `AWS Management Console
  <https://console.aws.amazon.com/>`__

- `AWS Command Line Interface (CLI)
  <https://aws.amazon.com/cli/>`__
  (`@GitHub <https://github.com/aws/aws-cli>`__)

- `AWS Developer Tools (SDKs)
  <https://aws.amazon.com/getting-started/tools-sdks/>`__

----

See Also
========

- `The Open Guide to Amazon Web Services
  <https://github.com/open-guides/og-aws>`__

- Arto's Notes re: `Ansible <ansible>`__
