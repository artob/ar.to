********************************************************************
Arto's Notes re: `Kharkiv <https://en.wikipedia.org/wiki/Kharkiv>`__
********************************************************************

* `Reference <#reference>`__
* `See Also <#see-also>`__

----

Reference
=========

* `Kharkiv at Wikitravel
  <http://wikitravel.org/en/Kharkiv>`__

* `Kharkiv at Lonely Planet
  <https://www.lonelyplanet.com/ukraine/kharkiv>`__

----

See Also
========

`Ukraine <ukraine>`__: `Kiev <kiev>`__, `Lviv <lviv>`__, and `Odessa <odessa>`__
