************************************************************************
Arto's Notes re: `Gibraltar <https://en.wikipedia.org/wiki/Gibraltar>`__
************************************************************************

Airlines
========

* `British Airways
  <https://www.britishairways.com/en-gb/destinations/gibraltar/flights-to-gibraltar>`__
* `EasyJet
  <http://www.easyjet.com/en/cheap-flights/gibraltar/london-gatwick>`__
* `Monarch
  <http://www.monarch.co.uk/gibraltar/flights>`__
