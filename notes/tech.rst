***************************
Arto's Notes re: technology
***************************

My shortlist of essential platform technologies that are worth learning,
investing into, and contributing to (when open source).

Markup Languages
================

- `reStructuredText <rest>`__

Essential Tooling
=================

- `Bash <bash>`__
- `Git <git>`__
- `SSH <ssh>`__
- `Make <makefile>`__
- `Vim <vim>`__

Programming Languages
=====================

- `C <c>`__/`C++ <cxx>`__ (for POSIX and embedded)
- `Go <go>`__ (for basic tooling)
- `OCaml <ocaml>`__ (for advanced tooling)
- `Lua <lua>`__ (for embeddable scripting)
- `Kotlin <kotlin>`__ (for the JVM)

Numerical Computing
===================

- `MATLAB <matlab>`__
- `Julia <julia>`__
- `Python <python>`__ with NumPy

Automated Reasoning
===================

- `Coq <coq>`__

GUI Frameworks
==============

- `Flutter <flutter>`__
- `Qt <qt>`__ (legacy)

3D Frameworks
=============

- `Vulkan <vulkan>`__
- `OpenGL <opengl>`__ (legacy)

Database Systems
================

- `PostgreSQL <postgres>`__
- `SQLite <sqlite>`__/SQLCipher
- `LMDB <lmdb>`__

Runtime Platforms
=================

- `WebAssembly <wasm>`__
- `Erlang (ERTS) <erlang>`__
- `JVM <jvm>`__
- `POSIX <posix>`__

Embedded Platforms
==================

- `Adafruit Feather nRF52 <adafruit>`__
- `BeagleBone Blue <beaglebone>`__
- `Arduino <arduino>`__
- `Raspberry Pi <rpi>`__

`Operating Systems <os>`__
==========================

Unix-like
---------

- `Linux <linux>`__
- `FreeBSD <freebsd>`__
- `OpenBSD <openbsd>`__
- `Minix <minix>`__

`RTOSes <rtos>`__
-----------------

- `Mynewt <mynewt>`__

Miscellaneous
-------------

- `seL4 <sel4>`__
- `Android <android>`__

CPU Architectures
=================

- `RISC-V <riscv>`__
- `x86 <x86>`__ (legacy)
- `ARM <arm>`__ (legacy)

Media Codecs
============

- `AV1 <av1>`__ (video)
- `Opus <opus>`__ (audio)

Miscellaneous & Unsorted
========================

- `Bluetooth <bluetooth>`__
- `gRPC <grpc>`__
- `TensorFlow <tensorflow>`__
- `OpenCV <opencv>`__
- `ROS <ros>`__
- `Gazebo <gazebo>`__
