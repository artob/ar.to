*********************************************************************************************************
Arto's Notes re: `artificial intelligence (AI) <https://en.wikipedia.org/wiki/Artificial_intelligence>`__
*********************************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Quotes
======

   "AI is the new electricity. Just as 100 years ago electricity transformed
   industry after industry, AI will now do the same."

   -- Andrew Ng,
   `The AI Revolution: Why Deep Learning Is Suddenly Changing Your Life
   <http://fortune.com/ai-artificial-intelligence-deep-machine-learning/>`__

   "Amid all this activity, a picture of our AI future is coming into view,
   and it is not the HAL 9000--a discrete machine animated by a charismatic
   (yet potentially homicidal) humanlike consciousness--or a Singularitan
   rapture of superintelligence. The AI on the horizon looks more like
   Amazon Web Services--cheap, reliable, industrial-grade digital smartness
   running behind everything, and almost invisible except when it blinks
   off. This common utility will serve you as much IQ as you want but no
   more than you need. Like all utilities, AI will be supremely boring, even
   as it transforms the Internet, the global economy, and civilization. It
   will enliven inert objects, much as electricity did more than a century
   ago. Everything that we formerly electrified we will now cognitize. This
   new utilitarian AI will also augment us individually as people (deepening
   our memory, speeding our recognition) and collectively as a species.
   There is almost nothing we can think of that cannot be made new,
   different, or interesting by infusing it with some extra IQ. In fact, the
   business plans of the next 10,000 startups are easy to forecast: *Take X
   and add AI*. This is a big deal, and now it’s here."

   -- Kevin Kelly.
   `The Three Breakthroughs That Have Finally Unleashed AI on the World
   <https://www.wired.com/2014/10/future-of-artificial-intelligence/>`__

----

Videos
======

- `The Master Algorithm
  <https://www.youtube.com/watch?v=B8J4uefCQMc>`__
  (2015) by Pedro Domingos (Authors at Google)

- `AI: What's Working, What's Not
  <https://www.youtube.com/watch?v=od7quAx9nMw>`__
  (2017) by Frank Chen, Andreessen Horowitz

- `The State of Artificial Intelligence
  <https://www.youtube.com/watch?v=NKpuX_yzdYs>`__
  (2017) by Andrew Ng, Stanford University

- `Building the Software 2.0 Stack
  <https://www.youtube.com/watch?v=zywIvINSlaI>`__
  (2018) by Andrej Karpathy, Tesla

----

Books
=====

Essential Books
---------------

- `The Master Algorithm: How the Quest for the Ultimate Learning Machine Will Remake Our World
  <https://en.wikipedia.org/wiki/The_Master_Algorithm>`__
  (2015) by Pedro Domingos
  (`@Amazon <https://www.amazon.com/dp/B012271YB2>`__)

- `Prediction Machines: The Simple Economics of Artificial Intelligence
  <https://www.goodreads.com/book/show/36484703>`__
  (2018) by Ajay Agrawal, Joshua Gans, and Avi Goldfarb
  (`@Amazon <https://www.amazon.com/dp/B075GXJPFS>`__)

----

Essays
======

- `Physiognomy's New Clothes
  <https://medium.com/@blaisea/physiognomys-new-clothes-f2d4b59fdd6a>`__
  by Blaise Aguera y Arcas

----

Blogs
=====

- `Rodney Brooks on the Future of Robotics and Artificial Intelligence
  <http://rodneybrooks.com/forai-future-of-robotics-and-artificial-intelligence/>`__

----

See Also
========

- Arto's Notes re: `machine learning (ML) <ml>`__ (`deep learning (DL) <dl>`__)

- Arto's Notes re: `predictive processing (PP) <pp>`__
