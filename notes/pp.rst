*************************************************************************************************
Arto's Notes re: `predictive processing (PP) <https://en.wikipedia.org/wiki/Predictive_coding>`__
*************************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Quotes
======

   "In his [2004] book On Intelligence, Jeff Hawkins was among the first to
   argue that prediction is the basis for human intelligence."

   -- Ajay Agrawal et al,
   `Prediction Machines: The Simple Economics of Artificial Intelligence
   <https://www.goodreads.com/book/show/36484703>`__

----

Books
=====

- `Exploring Robotic Minds: Actions, Symbols, and Consciousness as Self-Organizing Dynamic Phenomena
  <https://www.goodreads.com/book/show/32592535>`__
  (2016) by Jun Tani
  (`@Amazon <https://www.amazon.com/dp/B01LZZ5W1D>`__,
  `@OUP <https://global.oup.com/academic/product/exploring-robotic-minds-9780190281069>`__)

- `Surfing Uncertainty: Prediction, Action, and the Embodied Mind
  <https://www.goodreads.com/book/show/26796709>`__
  (2015) by Andy Clark
  (`@Amazon <https://www.amazon.com/dp/B0146Y9T34>`__)

- `The Predictive Mind
  <https://www.goodreads.com/book/show/19341362>`__
  (2013) by Jakob Hohwy
  (`@Amazon <https://www.amazon.com/dp/B00GV74Q7E>`__)

- `On Intelligence
  <https://en.wikipedia.org/wiki/On_Intelligence>`__
  (2004) by Jeff Hawkins
  (`@Amazon <https://www.amazon.com/dp/B003J4VE5Y>`__)

----

Papers
======

- `Philosophy and Predictive Processing (PPP) <https://predictive-mind.net/>`__

----

Blogs
=====

- `The Brains Blog <http://philosophyofbrains.com/>`__

----

Commentary
==========

By Andy Clark
-------------

- `Surfing Uncertainty: Prediction, Action, and the Embodied Mind
  <http://philosophyofbrains.com/2015/12/14/surfing-uncertainty-prediction-action-and-the-embodied-mind.aspx>`__

- `Conservative versus Radical Predictive Processing
  <http://philosophyofbrains.com/2015/12/15/conservative-versus-radical-predictive-processing.aspx>`__

- `Expecting Ourselves
  <http://philosophyofbrains.com/2015/12/16/expecting-ourselves.aspx>`__

- `The Dark Side of the Predictive Mind
  <http://philosophyofbrains.com/2015/12/17/the-dark-side-of-the-predictive-mind.aspx>`__

By Scott Alexander
------------------

- `Book Review: Surfing Uncertainty
  <http://slatestarcodex.com/2017/09/05/book-review-surfing-uncertainty/>`__
  (`@Reddit <https://www.reddit.com/r/slatestarcodex/comments/6ycnob/book_review_surfing_uncertainty/>`__)

- `Predictive Processing and Perceptual Control
  <http://slatestarcodex.com/2017/09/06/predictive-processing-and-perceptual-control/>`__
  (`@Reddit <https://www.reddit.com/r/slatestarcodex/comments/6yke9a/predictive_processing_and_perceptual_control/>`__)

- `How Do We Get Breasts Out of Bayes Theorem?
  <http://slatestarcodex.com/2017/09/07/how-do-we-get-breasts-out-of-bayes-theorem/>`__
  (`@Reddit <https://www.reddit.com/r/slatestarcodex/comments/6ysns0/how_do_we_get_breasts_out_of_bayes_theorem/>`__)

- `Toward a Predictive Theory of Depression
  <http://slatestarcodex.com/2017/09/12/toward-a-predictive-theory-of-depression/>`__
  (`@Reddit <https://www.reddit.com/r/slatestarcodex/comments/6zsf6d/toward_a_predictive_theory_of_depression/>`__)

Miscellaneous
-------------

- `The brain is a Peirce engine
  <http://esr.ibiblio.org/?p=7651>`__ by ESR

- `Edge.org 2016: What do you consider the most interesting recent
  scientific news? What makes it important?
  <https://www.edge.org/response-detail/26707>`__

----

Talks
=====

- `TED2017: Your brain hallucinates your conscious reality
  <https://www.ted.com/talks/anil_seth_how_your_brain_hallucinates_your_conscious_reality>`__

----

Previously
==========

`Perceptual control theory (PCT) <https://en.wikipedia.org/wiki/Perceptual_control_theory>`__
---------------------------------------------------------------------------------------------

- `Book Review: Behavior - The Control of Perception
  <http://slatestarcodex.com/2017/03/06/book-review-behavior-the-control-of-perception/>`__

----

Thoughts
========

- How might PP account for `virtual reality sickness
  <https://en.wikipedia.org/wiki/Virtual_reality_sickness>`__?

----

See Also
========

- Arto's Notes re: `artificial intelligence (AI) <ai>`__,
  `machine learning (ML) <ml>`__
