****************************************************************************
Arto's Notes re: `Nix <https://en.wikipedia.org/wiki/Nix_package_manager>`__
****************************************************************************

* https://nixos.org/nix/
* https://github.com/NixOS/nix

See Also
========

`NixOS <https://en.wikipedia.org/wiki/NixOS>`__
