***************************
Arto's Notes re: influences
***************************

Listed somewhat in the order I encountered and learned from them.

Edgar Rice Burroughs
--------------------

Robert Heinlein
---------------

Carl Sagan
----------

Richard Dawkins
---------------

Charles Darwin
--------------

Ayn Rand
--------

Aristotle
---------

Stefan Molyneux
---------------

Jonathan Haidt
--------------
