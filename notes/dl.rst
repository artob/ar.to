*************************************************************************************
Arto's Notes re: `deep learning (DL) <https://en.wikipedia.org/wiki/Deep_learning>`__
*************************************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Prerequisites
=============

- `Calculus <calculus>`__

- `Linear algebra <algebra#linear-algebra>`__

- `Probability <prob>`__ & `statistics <stats>`__

----

Books
=====

- `Deep Learning, Vol. 1: From Basics to Practice
  <https://www.goodreads.com/book/show/38657926>`__
  (2018) by Andrew Glassner
  (`website <https://dlbasics.com/>`__,
  `@Twitter <https://twitter.com/AndrewGlassner>`__)

- `Deep Learning, Vol. 2: From Basics to Practice
  <https://www.goodreads.com/book/show/38714149>`__
  (2018) by Andrew Glassner
  (`website <https://dlbasics.com/>`__,
  `@Twitter <https://twitter.com/AndrewGlassner>`__)

- `Deep Learning
  <https://www.goodreads.com/book/show/34105574>`__
  (2016) by Ian Goodfellow, Yoshua Bengio, Aaron Courville
  (`website <https://www.deeplearningbook.org/>`__)

See Also
--------

- `Arto's Deep Learning bookshelf
  <https://www.goodreads.com/review/list/22170557?shelf=deep-learning>`__

- `Artificial Neural Networks and Deep Learning
  <https://www.goodreads.com/list/show/89481>`__ community bookshelf.

----

Courses
=======

- `UvA Deep Learning Course
  <http://uvadlc.github.io/>`__
  at the University of Amsterdam

- https://www.coursera.org/specializations/deep-learning

- https://www.udacity.com/course/deep-learning--ud730

- https://www.udemy.com/practical-deep-learning-with-pytorch/

----

People
======

- `Geoffrey Hinton <https://en.wikipedia.org/wiki/Geoffrey_Hinton>`__,
  University of Toronto
  (`home page <http://www.cs.toronto.edu/~hinton/>`__)

- `Yann LeCun <https://en.wikipedia.org/wiki/Yann_LeCun>`__,
  Director of AI Research at Facebook
  (`home page <http://yann.lecun.com/>`__)

- `Yoshua Bengio <https://en.wikipedia.org/wiki/Yoshua_Bengio>`__,
  Université de Montréal
  (`home page <http://www.iro.umontreal.ca/~bengioy/yoshua_en/>`__)

----

Software
========

Frameworks
----------

- `TensorFlow <tensorflow>`__ (C++, Python) and Keras

- `PyTorch <https://github.com/pytorch/pytorch>`__ (Python)

- `Torch7 <https://en.wikipedia.org/wiki/Torch_(machine_learning)>`__ (Lua)

----

Hardware
========

`GPUs <gpu>`__
--------------

- **Recommended**: NVIDIA GTX 1080 Ti, GTX 1080, GTX 1070

- **On a small budget**: NVIDIA GTX 1060 (6 GB)

- **On a tiny budget**: NVIDIA GTX 1050 Ti (4 GB)

Sources:

- http://timdettmers.com/2017/04/09/which-gpu-for-deep-learning/
- https://www.oreilly.com/learning/build-a-super-fast-deep-learning-machine-for-under-1000
- https://jeffxtang.github.io/deep/learning,/hardware,/gpu,/performance/2017/02/14/deep-learning-machine.html
- https://medium.com/towards-data-science/build-a-deep-learning-rig-for-800-4434e21a424f
- https://www.servethehome.com/nvidia-deep-learning-ai-gpu-value-comparison-q2-2017/
- https://www.servethehome.com/deeplearning02-sub-800-ai-machine-learning-cuda-starter-build/

----

Unsorted
========

- https://keras.io
- http://deeplearning.net/software/theano/
- https://github.com/vahidk/EffectiveTensorflow
- https://github.com/torch/nn/tree/master/lib/THNN
- https://discuss.pytorch.org/t/torch-autograd-vs-pytorch-autograd/1671/5
- https://discuss.pytorch.org/t/roadmap-for-torch-and-pytorch/38/2
- https://github.com/tensorflow/models
- https://github.com/pytorch/examples
- https://github.com/pytorch/tutorials
- http://pytorch.org/tutorials/
- http://pytorch.org/docs/master/torchvision/models.html
- https://github.com/opencv/opencv/wiki/Deep-Learning-in-OpenCV

----

Resources
=========

Graphics
--------

- https://twitter.com/AndrewGlassner/status/976212991627575296
- https://twitter.com/AndrewGlassner/status/966822004153176064
- https://github.com/blueberrymusic/DeepLearningBook-Resources
- https://github.com/blueberrymusic/DeepLearningBookFigures-Thumbnails
- https://github.com/blueberrymusic/DeepLearningBookFigures-Volume1
- https://github.com/blueberrymusic/DeepLearningBookFigures-Volume2

----

See Also
========

- Arto's Notes re:
  `artificial intelligence (AI) <ai>`__,
  `machine learning (ML) <ml>`__,
  `neural networks <nn>`__
  `Markov logic network (MLN) <mln>`__,
  `fast.ai <fastai>`__,
  `OpenAI <openai>`__,
  `TensorFlow <tensorflow>`__
