*************************************************************************************
Arto's Notes re: `Lisp <https://en.wikipedia.org/wiki/Lisp_(programming_language)>`__
*************************************************************************************

See Also
========

`Common Lisp <common-lisp>`__, `Scheme <scheme>`__
