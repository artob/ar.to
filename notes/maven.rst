*****************************************************************************
Arto's Notes re: `Apache Maven <http://en.wikipedia.org/wiki/Apache_Maven>`__
*****************************************************************************

::

   $ mvn compile  # creates target/classes/

   $ mvn test

   $ mvn package  # creates target/PROJECT.war

   $ mvn clean
