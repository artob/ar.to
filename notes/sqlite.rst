******************************************************************
Arto's Notes re: `SQLite <https://en.wikipedia.org/wiki/SQLite>`__
******************************************************************

Reference
=========

* https://sqlite.org/docs.html

Extensions
==========

* `DBSTAT <https://sqlite.org/dbstat.html>`__
* `FTS3/4 <https://sqlite.org/fts3.html>`__
* `FTS5 <https://sqlite.org/fts5.html>`__
* `ICU <https://www.sqlite.org/src/artifact?ci=trunk&filename=ext/icu/README.txt>`__
* `JSON1 <https://sqlite.org/json1.html>`__
* `R*Tree <https://sqlite.org/rtree.html>`__
* `Session <https://sqlite.org/sessionintro.html>`__

Encryption
==========

SQLCipher
---------

* https://www.zetetic.net/sqlcipher/
* https://www.zetetic.net/sqlcipher/sqlcipher-for-android/
* https://www.zetetic.net/sqlcipher/introduction/
* https://www.zetetic.net/sqlcipher/design/
* https://www.zetetic.net/sqlcipher/sqlcipher-api/
* https://www.zetetic.net/sqlcipher/open-source/
* https://github.com/sqlcipher/sqlcipher
