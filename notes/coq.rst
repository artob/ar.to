************************************************************
Arto's Notes re: `Coq <https://en.wikipedia.org/wiki/Coq>`__
************************************************************

* https://coq.inria.fr/
* https://github.com/coq/coq

Frontends
=========

* `CoqIDE <https://coq.inria.fr/refman/Reference-Manual018.html>`__
* `PeaCoq <http://goto.ucsd.edu/peacoq/>`__
  (`GitHub <https://github.com/Ptival/PeaCoq>`__)
* `Proof General <https://proofgeneral.github.io/>`__
  (`GitHub <https://github.com/ProofGeneral/PG>`__)

Books
=====

* `Interactive Theorem Proving and Program Development
  <http://www.labri.fr/perso/casteran/CoqArt/>`__
  aka Coq'Art
  (2004) by Yves Bertot and Pierre Castéran
  (`Goodreads
  <https://www.goodreads.com/book/show/11279476-interactive-theorem-proving-and-program-development>`__)

* `Software Foundations
  <https://softwarefoundations.cis.upenn.edu/current/index.html>`__
  (2011+) by Benjamin C. Pierce et al.
  (`Goodreads
  <https://www.goodreads.com/book/show/13413455-software-foundations>`__)

* `Certified Programming with Dependent Types
  <http://adam.chlipala.net/cpdt/>`__
  (2013) by Adam Chlipala
  (`PDF <http://adam.chlipala.net/cpdt/cpdt.pdf>`__,
  `Goodreads
  <https://www.goodreads.com/book/show/22354770-certified-programming-with-dependent-types>`__)

* `Mathematical Components
  <https://math-comp.github.io/mcb/>`__
  (2015+) by Assia Mahboubi and Enrico Tassi
  (`PDF <https://math-comp.github.io/mcb/book.pdf>`__)

* `Software Foundations: Verified Functional Algorithms
  <https://www.cs.princeton.edu/~appel/vfa/>`__
  (2017+) by Andrew W. Appel

Tutorials
=========

Video Tutorials
---------------

* `Introduction to the Coq Proof Assistant
  <https://video.ias.edu/univalent/appel>`__
  by Andrew Appel
* `Video tutorials for the Coq proof assistant
  <http://math.andrej.com/2011/02/22/video-tutorials-for-the-coq-proof-assistant/>`__
  by Andrej Bauer

See Also
========

* `HOL4 <hol4>`__
* `Isabelle <isabelle>`__
