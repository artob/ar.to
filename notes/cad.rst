******************************************************************************
Arto's Notes re: `CAD <https://en.wikipedia.org/wiki/Computer-aided_design>`__
******************************************************************************

Software
========

* `Autodesk Fusion 360 <f360>`__ (commercial)
* `OpenSCAD <openscad>`__ (open source)

See Also
========

`CNC <cnc>`__
