*****************************************************
Arto's Notes re: `Kotlin <https://kotlinlang.org/>`__
*****************************************************

Installation
============

macOS
-----

::

   $ brew install kotlin

Notes
=====

* https://developer.android.com/kotlin/get-started.html
* https://developer.android.com/studio/preview/install-preview.html
* https://developer.android.com/studio/preview/kotlin-issues.html
* https://developer.android.com/kotlin/faq.html
* http://nilhcem.com/swift-is-like-kotlin/

Reference
=========

* `Calling Kotlin from Java
  <https://kotlinlang.org/docs/reference/java-to-kotlin-interop.html>`__
* `Calling Java code from Kotlin
  <https://kotlinlang.org/docs/reference/java-interop.html>`__
* `Using Gradle
  <https://kotlinlang.org/docs/reference/using-gradle.html>`__
* `Documenting Kotlin Code
  <https://kotlinlang.org/docs/reference/kotlin-doc.html>`__

Essays
======

* https://steve-yegge.blogspot.com/2017/05/why-kotlin-is-better-than-whatever-dumb.html

Libraries
=========

* https://github.com/KotlinBy/awesome-kotlin
* https://github.com/mcxiaoke/awesome-kotlin
