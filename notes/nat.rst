******************************************************************************************************************
Arto's Notes re: `network address translation (NAT) <https://en.wikipedia.org/wiki/Network_address_translation>`__
******************************************************************************************************************

* `STUN <https://en.wikipedia.org/wiki/STUN>`__

* `TURN <https://en.wikipedia.org/wiki/Traversal_Using_Relays_around_NAT>`__

* `ICE <https://en.wikipedia.org/wiki/Interactive_Connectivity_Establishment>`__
