*****************************************************************
Arto's Notes re: `metals <https://en.wikipedia.org/wiki/Metal>`__
*****************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

Elements
========

.. list-table::
   :widths: 20 40 20 20 20
   :header-rows: 1

   * - Name
     - Symbol
     - Density
     - `Melting ° <https://en.wikipedia.org/wiki/Melting_point>`__
     - `Price <http://www.infomine.com/investment/metal-prices/>`__

   * - `Tin <https://en.wikipedia.org/wiki/Tin>`__
     - Sn
     - 5.77 g/cm3
     - 505 K
     - `$21.0 <https://markets.businessinsider.com/commodities/tin-price>`__/kg

   * - `Lead <https://en.wikipedia.org/wiki/Lead>`__
     - Pb
     - 11.34 g/cm3
     - 601 K
     - `$2.6 <https://markets.businessinsider.com/commodities/lead-price>`__/kg

   * - `Zinc <https://en.wikipedia.org/wiki/Zinc>`__
     - Zn
     - 7.14 g/cm3
     - 693 K
     - `$3.4 <https://markets.businessinsider.com/commodities/zinc-price>`__/kg

   * - `Aluminum <https://en.wikipedia.org/wiki/Aluminium>`__
     - Al
     - 2.70 g/cm3
     - 933 K
     - `$2.2 <https://markets.businessinsider.com/commodities/aluminum-price>`__/kg

   * - `Silver <https://en.wikipedia.org/wiki/Silver>`__
     - Ag
     - 10.49 g/cm3
     - 1235 K
     - `$534.7 <https://markets.businessinsider.com/commodities/silver-price>`__/kg

   * - `Gold <https://en.wikipedia.org/wiki/Gold>`__
     - Au
     - 19.30 g/cm3
     - 1337 K
     - `$40,814.7 <https://markets.businessinsider.com/commodities/gold-price>`__/kg

   * - `Copper <https://en.wikipedia.org/wiki/Copper>`__
     - Cu
     - 8.96 g/cm3
     - 1358 K
     - `$6.7 <https://markets.businessinsider.com/commodities/copper-price>`__/kg

   * - `Nickel <https://en.wikipedia.org/wiki/Nickel>`__
     - Ni
     - 8.91 g/cm3
     - 1728 K
     - `$10.6 <https://markets.businessinsider.com/commodities/nickel-price>`__/kg

   * - `Iron <https://en.wikipedia.org/wiki/Iron>`__
     - Fe
     - 7.87 g/cm3
     - 1811 K
     - $0.2?/kg

   * - `Titanium <https://en.wikipedia.org/wiki/Titanium>`__
     - Ti
     - 4.51 g/cm3
     - 1941 K
     - `$11.6 <https://agmetalminer.com/metal-prices/titanium/>`__/kg

----

Alloys
======

.. list-table::
   :widths: 20 40 20 20 20
   :header-rows: 1

   * - Name
     - Symbol
     - Density
     - `Melting ° <https://en.wikipedia.org/wiki/Melting_point>`__
     - `Price <http://www.infomine.com/investment/metal-prices/>`__

   * - `Brass <https://en.wikipedia.org/wiki/Brass>`__
     - Cu-Zn
     - 8.4+ g/cm3
     - 1173+ K
     - $4.9?/kg

   * - `Bronze <https://en.wikipedia.org/wiki/Bronze>`__
     - Cu-Sn
     - ? g/cm3
     - 1223+ K
     - $5.1?/kg

----

See Also
========

- Arto's Notes re: `foundries <foundry>`__
