**************************************************************************
Arto's Notes re: `TensorFlow <https://en.wikipedia.org/wiki/TensorFlow>`__
**************************************************************************

* https://www.tensorflow.org
* https://twitter.com/tensorflow
* https://github.com/tensorflow
* https://github.com/astorfi/TensorFlow-World-Resources

Tutorials
=========

* **Low-Level APIs:**
  `Introduction <https://www.tensorflow.org/programmers_guide/low_level_intro>`__,
  `Tensors <https://www.tensorflow.org/programmers_guide/tensors>`__,
  `Variables <https://www.tensorflow.org/programmers_guide/variables>`__,
  `Graphs and Sessions <https://www.tensorflow.org/programmers_guide/graphs>`__

See Also
========

`artificial intelligence (AI) <ai>`__,
`machine learning (ML) <ml>`__
