**********************************************************************
Arto's Notes re: `Fritzing <https://en.wikipedia.org/wiki/Fritzing>`__
**********************************************************************

.. contents:: Table of Contents
   :local:
   :depth: 1
   :backlinks: none

----

- http://fritzing.org
- http://fritzing.org/download/

----

Libraries
=========

- `fritzing/fritzing-parts
  <https://github.com/fritzing/fritzing-parts>`__

- `adafruit/Fritzing-Library
  <https://github.com/adafruit/Fritzing-Library>`__

----

`Tutorials <http://fritzing.org/learning/>`__
=============================================

- `Creating Custom Parts
  <http://fritzing.org/learning/tutorials/creating-custom-parts/>`__

- `Make Beautiful Fritzing Parts with eagle2fritzing
  <https://learn.adafruit.com/make-beautiful-fritzing-parts-with-eagle2fritzing-brd2svg>`__

----

Bugs
====

- https://github.com/fritzing/fritzing-app/issues/3308

  Workaround: execute ``/Applications/Fritzing.app/Contents/MacOS/Fritzing``
  from CLI

----

See Also
========

- Arto's Notes re: `electronics <electronics>`__
