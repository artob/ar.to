*******************************************************************
Arto's Notes re: `CNC <https://en.wikipedia.org/wiki/CNC_router>`__
*******************************************************************

* `Books <#books>`__
* `News <#news>`__
* `Forums <#forums>`__
* `Machines <#machines>`__
* `Videos <#videos>`__
* `See Also <#see-also>`__

----

Books
=====

* `Getting Started with CNC
  <https://www.goodreads.com/book/show/32850595>`__
  (2016) by Edward Ford

* `Getting Started with 3D Carving
  <https://www.goodreads.com/book/show/35963007>`__
  (2017) by Zach Kaplan

* `Guerrilla Guide to CNC Machining, Mold Making, and Resin Casting
  <http://lcamtuf.coredump.cx/gcnc/>`__
  (2015) by Michal Zalewski

* `Design for CNC
  <https://www.goodreads.com/book/show/37201060>`__
  (2017) by by Gary Rohrbacher & Anne Filson

* `Fusion 360 for Makers: Design Your Own Digital Models for 3D Printing and CNC Fabrication
  <https://www.amazon.com/dp/1680453556>`__
  (2018) by Lydia Sloan Cline

----

News
====

* https://makezine.com/tag/digital-fabrication/

----

Forums
======

* https://www.reddit.com/r/hobbycnc/

* https://www.reddit.com/r/CNC/

* https://www.reddit.com/r/shapeoko/

* https://www.reddit.com/r/XCarve/

----

Machines
========

* https://www.reddit.com/r/hobbycnc/wiki/index

Shapeoko vs X-Carve
-------------------

* https://www.youtube.com/watch?v=71AQBgDkAz4
* https://discuss.inventables.com/t/x-carve-versus-shapeoko-3/1223
* http://carbide3d.com/vs/shapeoko-vs-xcarve/

Open Source
-----------

* `Inventables X-Carve <xcarve>`__

* `Carbide 3D Shapeoko
  <http://carbide3d.com/shapeoko/>`__

* `eShapeoko
  <https://amberspyglass.co.uk/store/eshapeoko-cnc-milling-machine-mechanical-kit.html>`__

* `OpenBuilds OX
  <https://openbuilds.com/builds/openbuilds-ox-cnc-machine.341/>`__

* `Sienci Mill One
  <https://sienci.com/product/sienci-mill-one-kit/>`__

* `The Mostly Printed CNC (MPCNC)
  <https://www.v1engineering.com/specifications/>`__

* `Maslow CNC
  <http://www.maslowcnc.com/>`__

Miscellaneous
-------------

* `CNC Router Parts
  <https://www.cncrouterparts.com/>`__

* `MillRight CNC Carve King
  <https://www.millrightcnc.com/product-page/millright-cnc-carve-king>`__

* `BobsCNC
  <https://www.bobscnc.com/>`__

----

Hardware
========

* `gShield <https://github.com/synthetos/grblShield/wiki>`__
  (`buy <https://synthetos.myshopify.com/products/gshield-v5>`__)

* TinyG
  (`buy <https://synthetos.myshopify.com/products/tinyg>`__)

----

Firmware
========

* `Grbl <https://github.com/grbl/grbl>`__

* `TinyG <https://github.com/synthetos/TinyG>`__

----

Videos
======

* `Winston Moy
  <https://www.youtube.com/channel/UCxdCeHBUOlcCWr6RM8acEog>`__

----

See Also
========

* http://www.allmir.in.ua/video/Sgx6_wSGUJA/CNC-Arduino-gshield-actobotics.html

* https://www.inventables.com/projects/shapeoko-2-mechanical-kit-to-full-kit#bill-of-materials

`CAD <cad>`__, `X-Carve <xcarve>`__
