*********************
Arto's Notes re: evil
*********************

   "All that is necessary for the triumph of evil is that good men do
   nothing."

   -- `Edmund Burke <https://en.wikiquote.org/wiki/Edmund_Burke>`__
      (`attributed <https://quoteinvestigator.com/2010/12/04/good-men-do/>`__)
